package com.quangpld.ordering.system.core.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.context.ApplicationContext;

import com.quangpld.core.product.management.entity.Category;
import com.quangpld.core.product.management.entity.Product;
import com.quangpld.core.product.management.service.CategoryService;
import com.quangpld.core.product.management.service.ProductService.Status;
import com.quangpld.ordering.system.core.entity.Order;
import com.quangpld.ordering.system.core.entity.OrderDetail;
import com.quangpld.ordering.system.core.entity.ProductDetail;
import com.quangpld.ordering.system.core.util.OrderUtils;

@RunWith(JUnit4.class)
public class ProductDetailServiceTest {

  /** ProductDetailService object. */
  private ProductDetailService productDetailService = null;

  /** List of ProductDetails to delete in tearDown method. */
  private List<ProductDetail> tearDownListProductDetails = null;

  /**
   * 
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
    ApplicationContext context = OrderUtils.getClassPathXmlApplicationContext();
    productDetailService = context.getBean("productDetailService", ProductDetailService.class);
    tearDownListProductDetails = new ArrayList<ProductDetail>();
  }

  /**
   * 
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {
    for (ProductDetail productDetail : tearDownListProductDetails) {
      productDetailService.deleteProductDetail(productDetail);
    }
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testSaveProductDetail() throws Exception {
    //
    createProductDetail(1, "http://www.amazon.com/gp/product/B0006AAS5G/ref=ox_sc_sfl_image_5?ie=UTF8&psc=1&smid=A2VLFBLXQC8E5H", "http://ecx.images-amazon.com/images/I/81KzL5Rqq3L._SL1500_.jpg");
    ProductDetail got = productDetailService.findProductDetailById(1);
    assertNotNull(got);
    assertEquals(1, got.getProductid());
    assertEquals("http://www.amazon.com/gp/product/B0006AAS5G/ref=ox_sc_sfl_image_5?ie=UTF8&psc=1&smid=A2VLFBLXQC8E5H", got.getProductlink());
    assertEquals("http://ecx.images-amazon.com/images/I/81KzL5Rqq3L._SL1500_.jpg", got.getProductimagelink());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testUpdateProductDetail() throws Exception {
    //
    createProductDetail(1, "http://www.amazon.com/gp/product/B0006AAS5G/ref=ox_sc_sfl_image_5?ie=UTF8&psc=1&smid=A2VLFBLXQC8E5H", "http://ecx.images-amazon.com/images/I/81KzL5Rqq3L._SL1500_.jpg");
    ProductDetail got = productDetailService.findProductDetailById(1);
    assertNotNull(got);
    assertEquals(1, got.getProductid());
    assertEquals("http://www.amazon.com/gp/product/B0006AAS5G/ref=ox_sc_sfl_image_5?ie=UTF8&psc=1&smid=A2VLFBLXQC8E5H", got.getProductlink());
    assertEquals("http://ecx.images-amazon.com/images/I/81KzL5Rqq3L._SL1500_.jpg", got.getProductimagelink());

    //
    got.setProductlink("http://www.amazon.com/Marc-Jacobs-Quartz-Womens-MBM3077/dp/B004JVVYSI/ref=sr_1_2?s=watches&ie=UTF8&qid=1366302341&sr=1-2&keywords=marc+jacobs");
    got.setProductimagelink("http://ecx.images-amazon.com/images/I/71PHdnoVn%2BL._SL1500_.jpg");
    productDetailService.updateProductDetail(got);
    got = productDetailService.findProductDetailById(1);
    assertNotNull(got);
    assertEquals(1, got.getProductid());
    assertEquals("http://www.amazon.com/Marc-Jacobs-Quartz-Womens-MBM3077/dp/B004JVVYSI/ref=sr_1_2?s=watches&ie=UTF8&qid=1366302341&sr=1-2&keywords=marc+jacobs", got.getProductlink());
    assertEquals("http://ecx.images-amazon.com/images/I/71PHdnoVn%2BL._SL1500_.jpg", got.getProductimagelink());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testFindProductDetailById() throws Exception {
    //
    createProductDetail(1, "http://www.amazon.com/gp/product/B0006AAS5G/ref=ox_sc_sfl_image_5?ie=UTF8&psc=1&smid=A2VLFBLXQC8E5H", "http://ecx.images-amazon.com/images/I/81KzL5Rqq3L._SL1500_.jpg");
    ProductDetail got = productDetailService.findProductDetailById(1);
    assertNotNull(got);
    assertEquals(1, got.getProductid());
    assertEquals("http://www.amazon.com/gp/product/B0006AAS5G/ref=ox_sc_sfl_image_5?ie=UTF8&psc=1&smid=A2VLFBLXQC8E5H", got.getProductlink());
    assertEquals("http://ecx.images-amazon.com/images/I/81KzL5Rqq3L._SL1500_.jpg", got.getProductimagelink());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testDeleteProductDetail() throws Exception {
    //
    createProductDetail(1, "http://www.amazon.com/gp/product/B0006AAS5G/ref=ox_sc_sfl_image_5?ie=UTF8&psc=1&smid=A2VLFBLXQC8E5H", "http://ecx.images-amazon.com/images/I/81KzL5Rqq3L._SL1500_.jpg");
    ProductDetail got = productDetailService.findProductDetailById(1);
    assertNotNull(got);
    assertEquals(1, got.getProductid());
    assertEquals("http://www.amazon.com/gp/product/B0006AAS5G/ref=ox_sc_sfl_image_5?ie=UTF8&psc=1&smid=A2VLFBLXQC8E5H", got.getProductlink());
    assertEquals("http://ecx.images-amazon.com/images/I/81KzL5Rqq3L._SL1500_.jpg", got.getProductimagelink());

    //
    productDetailService.deleteProductDetail(got);
    assertNull(productDetailService.findProductDetailById(1));
  }

  private void createProductDetail(int productId, String productLink, String productImageLink) throws Exception {
    ProductDetail productDetail = new ProductDetail(productId, productLink, productImageLink);
    productDetailService.saveProductDetail(productDetail);
    tearDownListProductDetails.add(productDetail);
  }

}
