/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2013-02-03
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-02-03    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.ordering.system.core.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.context.ApplicationContext;

import com.quangpld.ordering.system.core.entity.Order;
import com.quangpld.ordering.system.core.entity.OrderDetail;
import com.quangpld.ordering.system.core.entity.OrderDetailBill;
import com.quangpld.ordering.system.core.service.OrderService.Status;
import com.quangpld.ordering.system.core.util.OrderUtils;

/**
 * OrderServiceTest class.
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
@RunWith(JUnit4.class)
public class OrderServiceTest {

  /** OrderService object. */
  private OrderService orderService = null;

  /** List of orders to delete in tearDown method. */
  private List<Order> tearDownListOrders = null;

  /**
   * 
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
    ApplicationContext context = OrderUtils.getClassPathXmlApplicationContext();
    orderService = context.getBean("orderService", OrderService.class);
    tearDownListOrders = new ArrayList<Order>();
  }

  /**
   * 
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {
    for (Order order : tearDownListOrders) {
      //
      OrderDetail example = new OrderDetail();
      example.setOrderid(order.getOrderid());
      List<OrderDetail> orderDetails = orderService.getOrderDetailByExample(example);
      for (OrderDetail orderDetail : orderDetails) {
        orderService.removeOrderDetailBill(orderDetail.getOrderdetailid());
      }

      //
      orderService.removeAllOrderDetails(order.getOrderid());

      //
      orderService.deleteOrder(order);
    }
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testSaveOrder() throws Exception {
    //
    assertEquals(0, orderService.getOrdersCount());

    //
    Order order = new Order("Alex Ferguson", "01666666666", "userabc@yahoo.com", "Hà Nội", Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok")).getTime(), Status.NEW.toString());
    orderService.saveOrder(order);
    assertEquals(1, orderService.getOrdersCount());
    Order got = orderService.findOrderById(order.getOrderid());
    assertEquals("Alex Ferguson", got.getFullname());
    assertEquals("01666666666", got.getPhone());
    assertEquals("userabc@yahoo.com", got.getEmail());
    assertEquals("Hà Nội", got.getAddress());
    assertEquals("NEW", got.getOrderstatus());

    //
    tearDownListOrders.add(got);
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testUpdateOrder() throws Exception {
    //
    int orderId = createOrder("Alex Ferguson", "01666666666", "userabc@yahoo.com", "Hà Nội", Status.NEW.toString());
    assertEquals(1, orderService.getOrdersCount());
    Order got = orderService.findOrderById(orderId);
    assertEquals("Alex Ferguson", got.getFullname());
    assertEquals("01666666666", got.getPhone());
    assertEquals("userabc@yahoo.com", got.getEmail());
    assertEquals("Hà Nội", got.getAddress());
    assertEquals("NEW", got.getOrderstatus());

    //
    got.setOrderstatus(Status.CUSTOMER_CONFIRMED.toString());
    Order updated = orderService.updateOrder(got);
    assertEquals("Alex Ferguson", updated.getFullname());
    assertEquals("01666666666", updated.getPhone());
    assertEquals("userabc@yahoo.com", updated.getEmail());
    assertEquals("Hà Nội", updated.getAddress());
    assertEquals("CUSTOMER_CONFIRMED", updated.getOrderstatus());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testFindOrderById() throws Exception {
    //
    int orderId = createOrder("Alex Ferguson", "01666666666", "userabc@yahoo.com", "Hà Nội", Status.NEW.toString());
    assertEquals(1, orderService.getOrdersCount());
    Order got = orderService.findOrderById(orderId);
    assertEquals("Alex Ferguson", got.getFullname());
    assertEquals("01666666666", got.getPhone());
    assertEquals("userabc@yahoo.com", got.getEmail());
    assertEquals("Hà Nội", got.getAddress());
    assertEquals("NEW", got.getOrderstatus());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testFindOrderByExample() throws Exception {
    //
    createOrder("Alex Ferguson", "01666666666", "userabc@yahoo.com", "Hà Nội", Status.NEW.toString());
    createOrder("David Beckham", "0912345678", "beckham@gmail.com", "Manchester", Status.NEW.toString());
    assertEquals(2, orderService.getOrdersCount());

    //
    Order example = new Order();
    example.setOrderstatus(Status.NEW.toString());
    List<Order> orders = orderService.findOrderByExample(example);
    Collections.sort(orders, Order.ORDER_ID_ASC_COMPARATOR);
    assertEquals(2, orders.size());
    Order got1 = orders.get(0);
    assertEquals("Alex Ferguson", got1.getFullname());
    assertEquals("01666666666", got1.getPhone());
    assertEquals("userabc@yahoo.com", got1.getEmail());
    assertEquals("Hà Nội", got1.getAddress());
    assertEquals("NEW", got1.getOrderstatus());
    Order got2 = orders.get(1);
    assertEquals("David Beckham", got2.getFullname());
    assertEquals("0912345678", got2.getPhone());
    assertEquals("beckham@gmail.com", got2.getEmail());
    assertEquals("Manchester", got2.getAddress());
    assertEquals("NEW", got2.getOrderstatus());

    //
    createOrder("Wayne Rooney", "0988888888", "rooney@gmail.com", "Manchester", Status.NEW.toString());
    createOrder("Robin van Persie", "0968999999", "persie@gmail.com", "Manchester", Status.NEW.toString());
    createOrder("Chicharito", "0977777777", "chicharito@gmail.com", "Manchester", Status.NEW.toString());
    assertEquals(5, orderService.getOrdersCount());
    orders = orderService.findOrderByExample(example, 1, 3);
    Collections.sort(orders, Order.ORDER_ID_ASC_COMPARATOR);
    assertEquals(3, orders.size());
    got2 = orders.get(0);
    assertEquals("David Beckham", got2.getFullname());
    assertEquals("0912345678", got2.getPhone());
    assertEquals("beckham@gmail.com", got2.getEmail());
    assertEquals("Manchester", got2.getAddress());
    assertEquals("NEW", got2.getOrderstatus());
    Order got3 = orders.get(1);
    assertEquals("Wayne Rooney", got3.getFullname());
    assertEquals("0988888888", got3.getPhone());
    assertEquals("rooney@gmail.com", got3.getEmail());
    assertEquals("Manchester", got3.getAddress());
    assertEquals("NEW", got3.getOrderstatus());
    Order got4 = orders.get(2);
    assertEquals("Robin van Persie", got4.getFullname());
    assertEquals("0968999999", got4.getPhone());
    assertEquals("persie@gmail.com", got4.getEmail());
    assertEquals("Manchester", got4.getAddress());
    assertEquals("NEW", got4.getOrderstatus());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testDeleteOrder() throws Exception {
    //
    int orderId1 = createOrder("Alex Ferguson", "01666666666", "userabc@yahoo.com", "Hà Nội", Status.NEW.toString());
    int orderId2 = createOrder("David Beckham", "0912345678", "beckham@gmail.com", "Manchester", Status.NEW.toString());
    assertEquals(2, orderService.getOrdersCount());

    //
    Order order1 = orderService.findOrderById(orderId1);
    Order order2 = orderService.findOrderById(orderId2);
    assertNotNull(order1);
    assertNotNull(order2);
    orderService.deleteOrder(order1);
    assertEquals(1, orderService.getOrdersCount());
    order1 = orderService.findOrderById(orderId1);
    order2 = orderService.findOrderById(orderId2);
    assertNull(order1);
    assertNotNull(order2);

    //
    tearDownListOrders.remove(0);
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testGetOrdersCount() throws Exception {
    //
    assertEquals(0, orderService.getOrdersCount());
    for (int i = 0; i < 20; i++) {
      createOrder("user" + i, "01666666666", "userabc@yahoo.com", "Hà Nội", Status.NEW.toString());
    }
    assertEquals(20, orderService.getOrdersCount());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testAddOrderDetail() throws Exception {
    //
    int orderId = createOrder("Alex Ferguson", "01666666666", "userabc@yahoo.com", "Hà Nội", Status.NEW.toString());
    assertEquals(0, orderService.getOrderDetailsCount(orderId));
    orderService.addOrderDetail(new OrderDetail(orderId,
                                                "Amazon Kindle",
                                                "http://www.amazon.com/Kindle-eReader-eBook-Reader-e-Reader-Special-Offers/dp/B0051QVESA/ref=sr_tr_sr_5?ie=UTF8&qid=1359881414&sr=8-5&keywords=kindle",
                                                "10",
                                                "chiếc"));
    assertEquals(1, orderService.getOrderDetailsCount(orderId));
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testGetOrderDetailByExample() throws Exception {
    //
    int orderId1 = createOrder("Alex Ferguson", "01666666666", "userabc@yahoo.com", "Hà Nội", Status.NEW.toString());
    createOrderDetail(orderId1,
                      "Amazon Kindle",
                      "http://www.amazon.com/Kindle-eReader-eBook-Reader-e-Reader-Special-Offers/dp/B0051QVESA/ref=sr_tr_sr_5?ie=UTF8&qid=1359881414&sr=8-5&keywords=kindle",
                      "10",
                      "chiếc");
    createOrderDetail(orderId1,
                      "Amazon Kindle Paperwhite",
                      "http://www.amazon.com/gp/product/B008GEKXUO/ref=fs_cl",
                      "20",
                      "chiếc");
    createOrderDetail(orderId1,
                      "Invicta Men's 8932 Pro Diver Collection Silver-Tone Watch",
                      "http://www.amazon.com/Invicta-Diver-Collection-Silver-Tone-Watch/dp/B0006AAS7E/ref=sr_1_cc_1?s=aps&ie=UTF8&qid=1359882247&sr=1-1-catcorr&keywords=invicta+watches+men",
                      "4",
                      "cái");
    int orderId2 = createOrder("David Beckham", "0912345678", "beckham@gmail.com", "Manchester", Status.NEW.toString());

    //
    OrderDetail exampleOrderDetail = new OrderDetail();
    exampleOrderDetail.setOrderid(orderId1);
    List<OrderDetail> orderDetails = orderService.getOrderDetailByExample(exampleOrderDetail);
    assertEquals(3, orderDetails.size());

    //
    exampleOrderDetail.setOrderid(orderId2);
    orderDetails = orderService.getOrderDetailByExample(exampleOrderDetail);
    assertEquals(0, orderDetails.size());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testUpdateOrderDetail() throws Exception {
    //
    int orderId = createOrder("Alex Ferguson", "01666666666", "userabc@yahoo.com", "Hà Nội", Status.NEW.toString());
    createOrderDetail(orderId,
                      "Amazon Kindle",
                      "http://www.amazon.com/Kindle-eReader-eBook-Reader-e-Reader-Special-Offers/dp/B0051QVESA/ref=sr_tr_sr_5?ie=UTF8&qid=1359881414&sr=8-5&keywords=kindle",
                      "10",
                      "chiếc");
    createOrderDetail(orderId,
                      "Amazon Kindle Paperwhite",
                      "http://www.amazon.com/gp/product/B008GEKXUO/ref=fs_cl",
                      "20",
                      "chiếc");
    createOrderDetail(orderId,
                      "Invicta Men's 8932 Pro Diver Collection Silver-Tone Watch",
                      "http://www.amazon.com/Invicta-Diver-Collection-Silver-Tone-Watch/dp/B0006AAS7E/ref=sr_1_cc_1?s=aps&ie=UTF8&qid=1359882247&sr=1-1-catcorr&keywords=invicta+watches+men",
                      "4",
                      "cái");
    OrderDetail exampleOrderDetail = new OrderDetail();
    exampleOrderDetail.setOrderid(orderId);
    List<OrderDetail> orderDetails = orderService.getOrderDetailByExample(exampleOrderDetail);
    for (OrderDetail orderDetail : orderDetails) {
      assertNull(orderDetail.getComment());

      //
      orderDetail.setComment("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
      orderService.updateOrderDetail(orderDetail);
      assertEquals("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", orderDetail.getComment());
    }
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testRemoveOrderDetail() throws Exception {
    //
    int orderId = createOrder("Alex Ferguson", "01666666666", "userabc@yahoo.com", "Hà Nội", Status.NEW.toString());
    createOrderDetail(orderId,
                      "Amazon Kindle",
                      "http://www.amazon.com/Kindle-eReader-eBook-Reader-e-Reader-Special-Offers/dp/B0051QVESA/ref=sr_tr_sr_5?ie=UTF8&qid=1359881414&sr=8-5&keywords=kindle",
                      "10",
                      "chiếc");
    createOrderDetail(orderId,
                      "Amazon Kindle Paperwhite",
                      "http://www.amazon.com/gp/product/B008GEKXUO/ref=fs_cl",
                      "20",
                      "chiếc");
    createOrderDetail(orderId,
                      "Invicta Men's 8932 Pro Diver Collection Silver-Tone Watch",
                      "http://www.amazon.com/Invicta-Diver-Collection-Silver-Tone-Watch/dp/B0006AAS7E/ref=sr_1_cc_1?s=aps&ie=UTF8&qid=1359882247&sr=1-1-catcorr&keywords=invicta+watches+men",
                      "4",
                      "cái");
    OrderDetail exampleOrderDetail = new OrderDetail();
    exampleOrderDetail.setOrderid(orderId);
    List<OrderDetail> orderDetails = orderService.getOrderDetailByExample(exampleOrderDetail);
    assertEquals(3, orderDetails.size());

    //
    orderService.removeOrderDetail(orderDetails.get(0));
    orderService.removeOrderDetail(orderDetails.get(1));
    orderDetails = orderService.getOrderDetailByExample(exampleOrderDetail);
    assertEquals(1, orderDetails.size());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testRemoveAllOrderDetails() throws Exception {
    //
    int orderId = createOrder("Alex Ferguson", "01666666666", "userabc@yahoo.com", "Hà Nội", Status.NEW.toString());
    createOrderDetail(orderId,
                      "Amazon Kindle",
                      "http://www.amazon.com/Kindle-eReader-eBook-Reader-e-Reader-Special-Offers/dp/B0051QVESA/ref=sr_tr_sr_5?ie=UTF8&qid=1359881414&sr=8-5&keywords=kindle",
                      "10",
                      "chiếc");
    createOrderDetail(orderId,
                      "Amazon Kindle Paperwhite",
                      "http://www.amazon.com/gp/product/B008GEKXUO/ref=fs_cl",
                      "20",
                      "chiếc");
    createOrderDetail(orderId,
                      "Invicta Men's 8932 Pro Diver Collection Silver-Tone Watch",
                      "http://www.amazon.com/Invicta-Diver-Collection-Silver-Tone-Watch/dp/B0006AAS7E/ref=sr_1_cc_1?s=aps&ie=UTF8&qid=1359882247&sr=1-1-catcorr&keywords=invicta+watches+men",
                      "4",
                      "cái");
    OrderDetail exampleOrderDetail = new OrderDetail();
    exampleOrderDetail.setOrderid(orderId);
    List<OrderDetail> orderDetails = orderService.getOrderDetailByExample(exampleOrderDetail);
    assertEquals(3, orderDetails.size());

    //
    orderService.removeAllOrderDetails(orderId);
    orderDetails = orderService.getOrderDetailByExample(exampleOrderDetail);
    assertEquals(0, orderDetails.size());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testGetOrderDetailsCount() throws Exception {
    //
    int orderId = createOrder("Alex Ferguson", "01666666666", "userabc@yahoo.com", "Hà Nội", Status.NEW.toString());
    createOrderDetail(orderId,
                      "Amazon Kindle",
                      "http://www.amazon.com/Kindle-eReader-eBook-Reader-e-Reader-Special-Offers/dp/B0051QVESA/ref=sr_tr_sr_5?ie=UTF8&qid=1359881414&sr=8-5&keywords=kindle",
                      "10",
                      "chiếc");
    createOrderDetail(orderId,
                      "Amazon Kindle Paperwhite",
                      "http://www.amazon.com/gp/product/B008GEKXUO/ref=fs_cl",
                      "20",
                      "chiếc");
    createOrderDetail(orderId,
                      "Invicta Men's 8932 Pro Diver Collection Silver-Tone Watch",
                      "http://www.amazon.com/Invicta-Diver-Collection-Silver-Tone-Watch/dp/B0006AAS7E/ref=sr_1_cc_1?s=aps&ie=UTF8&qid=1359882247&sr=1-1-catcorr&keywords=invicta+watches+men",
                      "4",
                      "cái");
    assertEquals(3, orderService.getOrderDetailsCount(orderId));

    //
    orderService.removeAllOrderDetails(orderId);
    assertEquals(0, orderService.getOrderDetailsCount(orderId));
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testAddOrderDetailBill() throws Exception {
    //
    int orderId = createOrder("Alex Ferguson", "01666666666", "userabc@yahoo.com", "Hà Nội", Status.NEW.toString());
    int orderDetailId = createOrderDetail(orderId,
                                          "Amazon Kindle",
                                          "http://www.amazon.com/Kindle-eReader-eBook-Reader-e-Reader-Special-Offers/dp/B0051QVESA/ref=sr_tr_sr_5?ie=UTF8&qid=1359881414&sr=8-5&keywords=kindle",
                                          "10",
                                          "chiếc");
    createOrderDetailBill(orderDetailId, 99.99, 11.99, 0.00, 12.00, 0.00, 0.00, "USD", 21500.00);

    //
    OrderDetailBill got = orderService.getOrderDetailBillById(orderDetailId);
    assertNotNull(got);
    assertEquals(orderDetailId, got.getOrderdetailid());
    assertEquals(Double.doubleToLongBits(99.99), Double.doubleToLongBits(got.getOriginalcost()));
    assertEquals(Double.doubleToLongBits(11.99), Double.doubleToLongBits(got.getTax()));
    assertEquals(Double.doubleToLongBits(0.00), Double.doubleToLongBits(got.getShippinginternationalcost()));
    assertEquals(Double.doubleToLongBits(12.00), Double.doubleToLongBits(got.getShippingvietnamcost()));
    assertEquals(Double.doubleToLongBits(0.00), Double.doubleToLongBits(got.getShippinginternalcost()));
    assertEquals(Double.doubleToLongBits(0.00), Double.doubleToLongBits(got.getAdditionalcost()));
    assertEquals("USD", got.getCurrency());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testGetOrderDetailBillById() throws Exception {
    //
    int orderId = createOrder("Alex Ferguson", "01666666666", "userabc@yahoo.com", "Hà Nội", Status.NEW.toString());
    int orderDetailId = createOrderDetail(orderId,
                                          "Amazon Kindle",
                                          "http://www.amazon.com/Kindle-eReader-eBook-Reader-e-Reader-Special-Offers/dp/B0051QVESA/ref=sr_tr_sr_5?ie=UTF8&qid=1359881414&sr=8-5&keywords=kindle",
                                          "10",
                                          "chiếc");
    createOrderDetailBill(orderDetailId, 99.99, 11.99, 0.00, 12.00, 0.00, 0.00, "USD", 21500.00);

    //
    OrderDetailBill got = orderService.getOrderDetailBillById(orderDetailId);
    assertNotNull(got);
    assertEquals(orderDetailId, got.getOrderdetailid());
    assertEquals(Double.doubleToLongBits(99.99), Double.doubleToLongBits(got.getOriginalcost()));
    assertEquals(Double.doubleToLongBits(11.99), Double.doubleToLongBits(got.getTax()));
    assertEquals(Double.doubleToLongBits(0.00), Double.doubleToLongBits(got.getShippinginternationalcost()));
    assertEquals(Double.doubleToLongBits(12.00), Double.doubleToLongBits(got.getShippingvietnamcost()));
    assertEquals(Double.doubleToLongBits(0.00), Double.doubleToLongBits(got.getShippinginternalcost()));
    assertEquals(Double.doubleToLongBits(0.00), Double.doubleToLongBits(got.getAdditionalcost()));
    assertEquals("USD", got.getCurrency());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testUpdateOrderDetailBill() throws Exception {
    //
    int orderId = createOrder("Alex Ferguson", "01666666666", "userabc@yahoo.com", "Hà Nội", Status.NEW.toString());
    int orderDetailId = createOrderDetail(orderId,
                                          "Amazon Kindle",
                                          "http://www.amazon.com/Kindle-eReader-eBook-Reader-e-Reader-Special-Offers/dp/B0051QVESA/ref=sr_tr_sr_5?ie=UTF8&qid=1359881414&sr=8-5&keywords=kindle",
                                          "10",
                                          "chiếc");
    createOrderDetailBill(orderDetailId, 99.99, 11.99, 0.00, 12.00, 0.00, 0.00, "USD", 21500.00);

    //
    OrderDetailBill got = orderService.getOrderDetailBillById(orderDetailId);
    assertNotNull(got);
    assertEquals(orderDetailId, got.getOrderdetailid());
    assertEquals(Double.doubleToLongBits(99.99), Double.doubleToLongBits(got.getOriginalcost()));
    assertEquals(Double.doubleToLongBits(11.99), Double.doubleToLongBits(got.getTax()));
    assertEquals(Double.doubleToLongBits(0.00), Double.doubleToLongBits(got.getShippinginternationalcost()));
    assertEquals(Double.doubleToLongBits(12.00), Double.doubleToLongBits(got.getShippingvietnamcost()));
    assertEquals(Double.doubleToLongBits(0.00), Double.doubleToLongBits(got.getShippinginternalcost()));
    assertEquals(Double.doubleToLongBits(0.00), Double.doubleToLongBits(got.getAdditionalcost()));
    assertEquals("USD", got.getCurrency());

    //
    got.setTax(19.09);
    got.setCurrency("VNĐ");
    orderService.updateOrderDetailBill(got);
    got = orderService.getOrderDetailBillById(orderDetailId);
    assertNotNull(got);
    assertEquals(orderDetailId, got.getOrderdetailid());
    assertEquals(Double.doubleToLongBits(99.99), Double.doubleToLongBits(got.getOriginalcost()));
    assertEquals(Double.doubleToLongBits(19.09), Double.doubleToLongBits(got.getTax()));
    assertEquals(Double.doubleToLongBits(0.00), Double.doubleToLongBits(got.getShippinginternationalcost()));
    assertEquals(Double.doubleToLongBits(12.00), Double.doubleToLongBits(got.getShippingvietnamcost()));
    assertEquals(Double.doubleToLongBits(0.00), Double.doubleToLongBits(got.getShippinginternalcost()));
    assertEquals(Double.doubleToLongBits(0.00), Double.doubleToLongBits(got.getAdditionalcost()));
    assertEquals("VNĐ", got.getCurrency());
  }


  /**
   * 
   * @throws Exception
   */
  @Test
  public void testRemoveOrderDetailBill() throws Exception {
    //
    int orderId = createOrder("Alex Ferguson", "01666666666", "userabc@yahoo.com", "Hà Nội", Status.NEW.toString());
    int orderDetailId = createOrderDetail(orderId,
                                          "Amazon Kindle",
                                          "http://www.amazon.com/Kindle-eReader-eBook-Reader-e-Reader-Special-Offers/dp/B0051QVESA/ref=sr_tr_sr_5?ie=UTF8&qid=1359881414&sr=8-5&keywords=kindle",
                                          "10",
                                          "chiếc");
    createOrderDetailBill(orderDetailId, 99.99, 11.99, 0.00, 12.00, 0.00, 0.00, "USD", 21500.00);
    OrderDetailBill got = orderService.getOrderDetailBillById(orderDetailId);
    assertNotNull(got);

    //
    orderService.removeOrderDetailBill(orderDetailId);
    got = orderService.getOrderDetailBillById(orderDetailId);
    assertNull(got);
  }

  /**
   * 
   * @param fullname
   * @param phone
   * @param email
   * @param address
   * @param orderStatus
   * @return
   * @throws Exception
   */
  private int createOrder(String fullname, String phone, String email, String address, String orderStatus) throws Exception {
    Order order = new Order(fullname, phone, email, address, Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok")).getTime(), orderStatus);
    orderService.saveOrder(order);
    tearDownListOrders.add(order);
    return order.getOrderid();
  }

  /**
   * 
   * @param orderid
   * @param productname
   * @param productlink
   * @param productquantity
   * @param productunit
   * @throws Exception
   */
  private int createOrderDetail(int orderid, String productname, String productlink, String productquantity, String productunit) throws Exception {
    OrderDetail orderDetail = new OrderDetail(orderid, productname, productlink, productquantity, productunit);
    orderService.addOrderDetail(orderDetail);
    return orderDetail.getOrderdetailid();
  }

  private void createOrderDetailBill(int orderdetailid, double originalcost, double tax, double shippinginternationalcost,
                                     double shippingvietnamcost, double shippinginternalcost, double additionalcost, String currency, double exchangerate) throws Exception {
    OrderDetailBill orderDetailBill = new OrderDetailBill(orderdetailid, originalcost, tax, shippinginternationalcost,
                                                          shippingvietnamcost, shippinginternalcost, additionalcost, currency, exchangerate);
    orderService.addOrderDetailBill(orderDetailBill);
  }

}
