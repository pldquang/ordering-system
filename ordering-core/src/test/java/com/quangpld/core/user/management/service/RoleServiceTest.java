/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2013-02-02
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-02-02    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.core.user.management.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.context.ApplicationContext;

import com.quangpld.core.user.management.util.UserManagementUtils;

/**
 * RoleServiceTest class.
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
@RunWith(JUnit4.class)
public class RoleServiceTest {

  /** RoleService object. */
  private RoleService roleService = null;

  /** List of roles to delete in tearDown method. */
  private List<String> tearDownListRoles = null;

  /**
   * 
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
    ApplicationContext context = UserManagementUtils.getClassPathXmlApplicationContext();
    roleService = context.getBean("roleService", RoleService.class);
    tearDownListRoles = new ArrayList<String>();
  }

  /**
   * 
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {
    for (String role : tearDownListRoles) {
      roleService.revokeAllRolePermissions(role);
    }
  }

  @Test
  public void testGetRolePermission() throws Exception {
    //
    createRolePermission("admin", "*");
    List<String> rolePermissions = roleService.getRolePermission("admin");
    assertEquals(1, rolePermissions.size());
    assertEquals("*", rolePermissions.get(0));
  }

  @Test
  public void testGrantRolePermission() throws Exception {
    //
    List<String> rolePermissions = roleService.getRolePermission("admin");
    assertEquals(0, rolePermissions.size());

    //
    roleService.grantRolePermission("admin", "*");
    tearDownListRoles.add("admin");
    rolePermissions = roleService.getRolePermission("admin");
    assertEquals(1, rolePermissions.size());
    assertEquals("*", rolePermissions.get(0));
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testRevokeRolePermission() throws Exception {
    //
    List<String> rolePermissions = roleService.getRolePermission("admin");
    assertEquals(0, rolePermissions.size());

    //
    createRolePermission("admin", "create");
    createRolePermission("admin", "read");
    createRolePermission("admin", "update");
    createRolePermission("admin", "delete");
    rolePermissions = roleService.getRolePermission("admin");
    assertEquals(4, rolePermissions.size());
    assertEquals("create", rolePermissions.get(0));
    assertEquals("read", rolePermissions.get(1));
    assertEquals("update", rolePermissions.get(2));
    assertEquals("delete", rolePermissions.get(3));

    //
    roleService.revokeRolePermission("admin", "update");
    rolePermissions = roleService.getRolePermission("admin");
    assertEquals(3, rolePermissions.size());
    assertEquals("create", rolePermissions.get(0));
    assertEquals("read", rolePermissions.get(1));
    assertEquals("delete", rolePermissions.get(2));
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testRevokeAllRolePermission() throws Exception {
    //
    List<String> rolePermissions = roleService.getRolePermission("admin");
    assertEquals(0, rolePermissions.size());

    //
    createRolePermission("admin", "create");
    createRolePermission("admin", "read");
    createRolePermission("admin", "update");
    createRolePermission("admin", "delete");
    rolePermissions = roleService.getRolePermission("admin");
    assertEquals(4, rolePermissions.size());
    assertEquals("create", rolePermissions.get(0));
    assertEquals("read", rolePermissions.get(1));
    assertEquals("update", rolePermissions.get(2));
    assertEquals("delete", rolePermissions.get(3));

    //
    roleService.revokeAllRolePermissions("admin");
    tearDownListRoles.remove(0);
    rolePermissions = roleService.getRolePermission("admin");
    assertEquals(0, rolePermissions.size());
  }

  /**
   * 
   * @throws Exception
   */
  private void createRolePermission(String role, String permission) throws Exception {
    roleService.grantRolePermission(role, permission);
    if (!tearDownListRoles.contains(role)) {
      tearDownListRoles.add(role);
    }
  }

}
