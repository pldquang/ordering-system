package com.quangpld.ordering.system.core.crawling.impl;

import org.dom4j.Document;
import org.dom4j.Node;
import org.springframework.context.ApplicationContext;

import com.quangpld.core.common.util.StringUtil;
import com.quangpld.core.parser.HtmlParser;
import com.quangpld.core.setting.management.entity.Setting;
import com.quangpld.core.setting.management.service.SettingService;
import com.quangpld.ordering.system.core.common.OrderConstants;
import com.quangpld.ordering.system.core.crawling.Crawler;
import com.quangpld.ordering.system.core.crawling.CrawlerFactory;
import com.quangpld.ordering.system.core.util.OrderUtils;

public class EbayCrawler implements Crawler {

  /** SettingService object. */
  private SettingService settingService = null;

  private String productLink = null;

  private Document document = null;

  static {
    try {
      CrawlerFactory.registerProduct(OrderConstants.HOST_NAME_EBAY, new EbayCrawler());
    } catch (Exception e) {
      // TODO:
    }
  }

  public EbayCrawler() throws Exception {
    ApplicationContext context = OrderUtils.getClassPathXmlApplicationContext();
    settingService = context.getBean("settingService", SettingService.class);
  }

  @Override
  public Crawler createCrawler() throws Exception {
    return new EbayCrawler();
  }

  @Override
  public void setProductLink(String productLink) throws Exception {
    this.productLink = productLink;
    document = HtmlParser.parse(productLink);
  }

  @Override
  public String getProductTitle() throws Exception {
    //
    if (document != null) {
      Setting setting = settingService.findSettingById(OrderConstants.EBAY_PRODUCT_TITLE_NODE);
      Node itemTitle = document.selectSingleNode(setting != null ? setting.getSettingvalue()
                                                                 : OrderConstants.EBAY_PRODUCT_TITLE_NODE_DEFAULT_VALUE);
      if (itemTitle != null) {
        return OrderUtils.buildPrettyProductTitle(itemTitle.getText());
      }
    }
    
    //
    return null;
  }

  @Override
  public double getProductCost() throws Exception {
    //
    if (document != null) {
      Setting setting = settingService.findSettingById(OrderConstants.EBAY_PRODUCT_PRICE_NODE);
      Node prcIsum = document.selectSingleNode(setting != null ? setting.getSettingvalue()
                                                               : OrderConstants.EBAY_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
      if (prcIsum != null) {
        return OrderUtils.getDoubleValueFromText(prcIsum.getText());
      }
    }

    //
    return 0.00d;
  }

  @Override
  public double getProductTaxRate() throws Exception {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public double getProductImportTaxRate() throws Exception {
    // Get Product Category.
    String productCategory = null;
    Node categoryNode = document.selectSingleNode("//TD[@id='vi-VR-brumb-lnkLst']//LI[@class='bc-w'][2]/A");
    if (categoryNode != null) {
      productCategory = categoryNode.getText();
    }

    //
    if (StringUtil.isEmpty(productCategory)) {
      return 0.00d;
    }

    //
    Setting setting = settingService.findSettingById(OrderConstants.EBAY_PRODUCT_CATEGORY_SHOES_KEY);
    String shoes = setting != null ? setting.getSettingvalue()
                                   : OrderConstants.EBAY_PRODUCT_CATEGORY_SHOES_VALUE;
    setting = settingService.findSettingById(OrderConstants.EBAY_PRODUCT_CATEGORY_WATCHES_KEY);
    String watches = setting != null ? setting.getSettingvalue()
                                     : OrderConstants.EBAY_PRODUCT_CATEGORY_WATCHES_VALUE;

    //
    productCategory = productCategory.toUpperCase();
    if (productCategory.contains(shoes)) {
      setting = settingService.findSettingById(OrderConstants.EBAY_PRODUCT_CATEGORY_SHOES_IMPORT_TAX_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.EBAY_PRODUCT_CATEGORY_SHOES_IMPORT_TAX_VALUE);
    } else if (productCategory.contains(watches)) {
      setting = settingService.findSettingById(OrderConstants.EBAY_PRODUCT_CATEGORY_WATCHES_IMPORT_TAX_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.EBAY_PRODUCT_CATEGORY_WATCHES_IMPORT_TAX_VALUE);
    }

    //
    return 0.00d;
  }

  @Override
  public double getProductWeight() throws Exception {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public double getProductShippingCost() throws Exception {
    return 0.00d;
  }

  @Override
  public String getCurrency() throws Exception {
    return OrderConstants.CURRENCY_SYMBOL_USD;
  }

}
