/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2012-02-03
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-02-03    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.ordering.system.core.service;

import java.util.List;

import com.quangpld.ordering.system.core.entity.Order;
import com.quangpld.ordering.system.core.entity.OrderDetail;
import com.quangpld.ordering.system.core.entity.OrderDetailBill;

/**
 * OrderService interface.
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
public interface OrderService {

  public enum Status {
    NEW,
    CUSTOMER_CONFIRMED,
    CUSTOMER_DEPOSITED,
    CUSTOMER_FULL_PAID,
    SHIPPING_TIME,
    FINISHED;
  }

  /**
   * 
   * @param order
   * @throws Exception
   */
  void saveOrder(Order order) throws Exception;

  /**
   * 
   * @param order
   * @return
   * @throws Exception
   */
  Order updateOrder(Order order) throws Exception;

  /**
   * 
   * @param orderId
   * @return
   * @throws Exception
   */
  Order findOrderById(Integer orderId) throws Exception;

  /**
   * 
   * @param order
   * @return
   * @throws Exception
   */
  List<Order> findOrderByExample(Order order) throws Exception;

  /**
   * 
   * @param order
   * @param offset
   * @param limit
   * @return
   * @throws Exception
   */
  List<Order> findOrderByExample(Order order, int offset, int limit) throws Exception;

  /**
   * 
   * @param order
   * @throws Exception
   */
  void deleteOrder(Order order) throws Exception;

  /**
   * 
   * @param order
   * @throws Exception
   */
  void hardDeleteOrder(Order order) throws Exception;

  /**
   * 
   * @return
   * @throws Exception
   */
  int getOrdersCount() throws Exception;

  /**
   * 
   * @param orderDetail
   * @throws Exception
   */
  void addOrderDetail(OrderDetail orderDetail) throws Exception;

  /**
   * 
   * @param exampleOrderDetail
   * @return
   * @throws Exception
   */
  List<OrderDetail> getOrderDetailByExample(OrderDetail exampleOrderDetail) throws Exception;

  /**
   * 
   * @param exampleOrderDetail
   * @param offset
   * @param limit
   * @return
   * @throws Exception
   */
  List<OrderDetail> getOrderDetailByExample(OrderDetail exampleOrderDetail, int offset, int limit) throws Exception;

  /**
   * 
   * @param orderDetail
   * @return
   * @throws Exception
   */
  OrderDetail updateOrderDetail(OrderDetail orderDetail) throws Exception;

  /**
   * 
   * @param orderDetail
   * @throws Exception
   */
  void removeOrderDetail(OrderDetail orderDetail) throws Exception;

  /**
   * 
   * @param orderId
   * @throws Exception
   */
  void removeAllOrderDetails(int orderId) throws Exception;

  /**
   * 
   * @param orderId
   * @return
   * @throws Exception
   */
  int getOrderDetailsCount(int orderId) throws Exception;

  /**
   * 
   * @param orderDetailBill
   * @throws Exception
   */
  void addOrderDetailBill(OrderDetailBill orderDetailBill) throws Exception;

  /**
   * 
   * @param orderDetailId
   * @return
   * @throws Exception
   */
  OrderDetailBill getOrderDetailBillById(int orderDetailId) throws Exception;

  /**
   * 
   * @param orderDetailBill
   * @return
   * @throws Exception
   */
  OrderDetailBill updateOrderDetailBill(OrderDetailBill orderDetailBill) throws Exception;

  /**
   * 
   * @param orderDetailId
   * @throws Exception
   */
  void removeOrderDetailBill(int orderDetailId) throws Exception;

}
