package com.quangpld.ordering.system.core.crawling;

public interface Crawler {

  Crawler createCrawler() throws Exception;

  void setProductLink(String productLink) throws Exception;

  String getProductTitle() throws Exception;

  double getProductCost() throws Exception;

  double getProductTaxRate() throws Exception;

  double getProductImportTaxRate() throws Exception;

  double getProductWeight() throws Exception;

  double getProductShippingCost() throws Exception;

  String getCurrency() throws Exception;

}
