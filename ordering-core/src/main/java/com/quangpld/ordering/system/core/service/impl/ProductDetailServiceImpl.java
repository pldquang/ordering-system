package com.quangpld.ordering.system.core.service.impl;

import com.quangpld.core.dao.core.GenericDao;
import com.quangpld.ordering.system.core.entity.ProductDetail;
import com.quangpld.ordering.system.core.service.ProductDetailService;

public class ProductDetailServiceImpl implements ProductDetailService {

  /** Generic DAO object for table PRODUCTDETAIL. */
  private GenericDao<ProductDetail, Integer> productDetailDao = null;

  /**
   * Default constructor.
   */
  public ProductDetailServiceImpl() {
  }

  public void setProductDetailDao(GenericDao<ProductDetail, Integer> productDetailDao) {
    this.productDetailDao = productDetailDao;
  }

  @Override
  public void saveProductDetail(ProductDetail productDetail) throws Exception {
    productDetailDao.saveOrUpdate(productDetail);
  }

  @Override
  public ProductDetail updateProductDetail(ProductDetail productDetail) throws Exception {
    productDetailDao.saveOrUpdate(productDetail);
    return productDetail;
  }

  @Override
  public ProductDetail findProductDetailById(Integer productId) throws Exception {
    return productDetailDao.findById(productId);
  }

  @Override
  public void deleteProductDetail(ProductDetail productDetail) throws Exception {
    productDetailDao.delete(productDetail);
  }

}
