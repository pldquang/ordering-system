package com.quangpld.ordering.system.core.rss;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

public class AmazonRssReader implements RssReader {

  private SyndFeed feed;

  public AmazonRssReader(String link) throws Exception {
    URL url  = new URL(link);
    XmlReader reader = new XmlReader(url);
    feed = new SyndFeedInput().build(reader);
  }

  @Override
  public String getTitle() throws Exception {
    return feed.getTitle();
  }

  @Override
  public String getLink() throws Exception {
    return feed.getLink();
  }

  @Override
  public String getDescription() throws Exception {
    return feed.getDescription();
  }

  @Override
  public String getPublishedDate() throws Exception {
    return feed.getPublishedDate().toString();
  }

  @Override
  public String getLastBuildDate() throws Exception {
    return null;
  }

  @Override
  public String getTtl() throws Exception {
    return null;
  }

  @Override
  public String getGenerator() throws Exception {
    return null;
  }

  @Override
  public String getLanguage() throws Exception {
    return feed.getLanguage();
  }

  @Override
  public String getCopyright() throws Exception {
    return feed.getCopyright();
  }

  @Override
  public List<RssItem> getItems() throws Exception {
    List<RssItem> rssItems = new ArrayList<RssItem>();
    List<SyndEntry> entries = feed.getEntries();
    RssItem rssItem = null;
    for (SyndEntry syndEntry : entries) {
      rssItem = new RssItem();
      rssItem.setTitle(syndEntry.getTitle());
      rssItem.setLink(syndEntry.getLink());
      rssItem.setDescription(syndEntry.getDescription().getValue());
      rssItems.add(rssItem);
    }
    return rssItems;
  }

  public static void main(String[] args) {
    try {
      RssReader rssReader = new AmazonRssReader("http://www.amazon.com/rss/tag/invicta/popular/ref=tag_tdp_rss_pop_man");
      System.out.println(rssReader.getTitle());
      System.out.println(rssReader.getLink());
      System.out.println(rssReader.getDescription());
      System.out.println(rssReader.getPublishedDate());
      System.out.println(rssReader.getLanguage());
      System.out.println(rssReader.getCopyright());
      for (RssItem rssItem : rssReader.getItems()) {
        System.out.println("==========================================================================");
        System.out.println(rssItem.getTitle());
        System.out.println(rssItem.getLink());
        System.out.println(rssItem.getDescription());
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
