/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2012-02-03
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-02-03    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.ordering.system.core.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.quangpld.ordering.system.core.common.OrderConstants;

/**
 * OrderUtils class.
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
public class OrderUtils {

  /**
   * 
   * @return
   * @throws Exception
   */
  public static ApplicationContext getClassPathXmlApplicationContext() throws Exception {
    return new ClassPathXmlApplicationContext(new String[] {"application-context-ordering-core.xml"});
  }

  /**
   * 
   * @param title
   * @return
   * @throws Exception
   */
  public static String buildPrettyProductTitle(String title) throws Exception {
    return title.length() <= OrderConstants.PRODUCT_TITLE_MAX_LENGTH ? title : title.substring(0, OrderConstants.PRODUCT_TITLE_MAX_LENGTH) + OrderConstants.ETC;
  }

  /**
   * 
   * @param str
   * @return
   * @throws Exception
   */
  public static double getDoubleValueFromText(String str) throws Exception {
    double value = 0.00d;
    try {
      value = Double.parseDouble(str.replaceAll("[^\\d\\.]", ""));
    } catch (Exception e) {
      // TODO:
    }
    return value;
  }

  /**
   * 
   * @param weight
   * @param unit
   * @return
   */
  public static double convertWeightToKg(double weight, String unit) {
    try {
      if (OrderConstants.UNIT_KILOGRAMS.equals(unit)) {
        return weight;
      } else if (OrderConstants.UNIT_POUNDS.equals(unit)) {
        return weight * OrderConstants.UNIT_RATE_POUNDS_TO_KILOGRAMS;
      } else if (OrderConstants.UNIT_OUNCES.equals(unit)) {
        return weight * OrderConstants.UNIT_RATE_OUNCES_TO_KILOGRAMS;
      } else if (OrderConstants.UNIT_GRAMS.equals(unit)) {
        return weight * OrderConstants.UNIT_RATE_GRAMS_TO_KILOGRAMS;
      }
    } catch (Exception e) {
      // TODO:
    }
    return 0.0;
  }

  /**
   * 
   * @param weight
   * @return
   */
  public static double calculateWeight(double weight) {
    if (weight < 0.50) {
      return weight * 2.00;
    } else if (0.50 <= weight && weight < 1.00) {
      return weight * 1.75;
    } else if (1.00 <= weight && weight < 10.00) {
      return weight * 1.50;
    } else if (10.00 <= weight && weight < 20.00) {
      return weight * 1.35;
    } else {
      return weight * 1.15;
    }
  }

}
