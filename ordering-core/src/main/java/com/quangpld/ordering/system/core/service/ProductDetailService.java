package com.quangpld.ordering.system.core.service;

import com.quangpld.ordering.system.core.entity.ProductDetail;

public interface ProductDetailService {

  /**
   * 
   * @param productDetail
   * @throws Exception
   */
  void saveProductDetail(ProductDetail productDetail) throws Exception;

  /**
   * 
   * @param productDetail
   * @return
   * @throws Exception
   */
  ProductDetail updateProductDetail(ProductDetail productDetail) throws Exception;

  /**
   * 
   * @param productId
   * @return
   * @throws Exception
   */
  ProductDetail findProductDetailById(Integer productId) throws Exception;

  /**
   * 
   * @param productDetail
   * @throws Exception
   */
  void deleteProductDetail(ProductDetail productDetail) throws Exception;

}
