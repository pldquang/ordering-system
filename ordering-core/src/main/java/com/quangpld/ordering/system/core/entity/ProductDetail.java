package com.quangpld.ordering.system.core.entity;
// default package
// Generated Apr 18, 2013 10:47:26 PM by Hibernate Tools 4.0.0

/**
 * Productdetail generated by hbm2java
 */
public class ProductDetail implements java.io.Serializable {

  private int productid;
  private String productlink;
  private String productimagelink;

  public ProductDetail() {
  }

  public ProductDetail(int productid, String productlink,
      String productimagelink) {
    this.productid = productid;
    this.productlink = productlink;
    this.productimagelink = productimagelink;
  }

  public int getProductid() {
    return this.productid;
  }

  public void setProductid(int productid) {
    this.productid = productid;
  }

  public String getProductlink() {
    return this.productlink;
  }

  public void setProductlink(String productlink) {
    this.productlink = productlink;
  }

  public String getProductimagelink() {
    return this.productimagelink;
  }

  public void setProductimagelink(String productimagelink) {
    this.productimagelink = productimagelink;
  }

}
