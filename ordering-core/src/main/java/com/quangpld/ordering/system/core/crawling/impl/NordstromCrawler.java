package com.quangpld.ordering.system.core.crawling.impl;

import org.dom4j.Document;
import org.dom4j.Node;
import org.springframework.context.ApplicationContext;

import com.quangpld.core.parser.HtmlParser;
import com.quangpld.core.setting.management.entity.Setting;
import com.quangpld.core.setting.management.service.SettingService;
import com.quangpld.ordering.system.core.common.OrderConstants;
import com.quangpld.ordering.system.core.crawling.Crawler;
import com.quangpld.ordering.system.core.crawling.CrawlerFactory;
import com.quangpld.ordering.system.core.util.OrderUtils;

public class NordstromCrawler implements Crawler {

  /** SettingService object. */
  private SettingService settingService = null;

  private String productLink = null;

  private Document document = null;

  static {
    try {
      CrawlerFactory.registerProduct(OrderConstants.HOST_NAME_NORDSTROM, new NordstromCrawler());
    } catch (Exception e) {
      // TODO:
    }
  }

  public NordstromCrawler() throws Exception {
    ApplicationContext context = OrderUtils.getClassPathXmlApplicationContext();
    settingService = context.getBean("settingService", SettingService.class);
  }

  @Override
  public Crawler createCrawler() throws Exception {
    return new NordstromCrawler();
  }

  @Override
  public void setProductLink(String productLink) throws Exception {
    this.productLink = productLink;
    document = HtmlParser.parse(productLink);
  }

  @Override
  public String getProductTitle() throws Exception {
    //
    if (document != null) {
      Setting setting = settingService.findSettingById(OrderConstants.NORDSTROM_PRODUCT_TITLE_1_NODE);
      Node productTitle1 = document.selectSingleNode(setting != null ? setting.getSettingvalue()
                                                                     : OrderConstants.NORDSTROM_PRODUCT_TITLE_1_NODE_DEFAULT_VALUE);
      setting = settingService.findSettingById(OrderConstants.NORDSTROM_PRODUCT_TITLE_2_NODE);
      Node productTitle2 = document.selectSingleNode(setting != null ? setting.getSettingvalue()
                                                                     : OrderConstants.NORDSTROM_PRODUCT_TITLE_2_NODE_DEFAULT_VALUE);
      if (productTitle1 != null && productTitle2 != null) {
        return OrderUtils.buildPrettyProductTitle(productTitle1.getText() + " | " + productTitle2.getText());
      }
    }
    
    //
    return null;
  }

  @Override
  public double getProductCost() throws Exception {
    //
    if (document != null) {
      Setting setting = settingService.findSettingById(OrderConstants.NORDSTROM_PRODUCT_PRICE_SPECIAL_NODE);
      Node itemprice = document.selectSingleNode(setting != null ? setting.getSettingvalue()
                                                                 : OrderConstants.NORDSTROM_PRODUCT_PRICE_SPECIAL_NODE_DEFAULT_VALUE);
      //
      if (itemprice == null) {
        setting = settingService.findSettingById(OrderConstants.NORDSTROM_PRODUCT_PRICE_NODE);
        itemprice = document.selectSingleNode(setting != null ? setting.getSettingvalue()
                                                                   : OrderConstants.NORDSTROM_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
      }

      //
      if (itemprice != null) {
        return OrderUtils.getDoubleValueFromText(itemprice.getText());
      }
    }

    //
    return 0.00d;
  }

  @Override
  public double getProductTaxRate() throws Exception {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public double getProductImportTaxRate() throws Exception {
    return 0.00d;
  }

  @Override
  public double getProductWeight() throws Exception {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public double getProductShippingCost() throws Exception {
    return 0.00d;
  }

  @Override
  public String getCurrency() throws Exception {
    return OrderConstants.CURRENCY_SYMBOL_USD;
  }

}
