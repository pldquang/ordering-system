/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2012-02-03
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-02-03    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.ordering.system.core.service.impl;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import com.quangpld.core.dao.core.GenericDao;
import com.quangpld.ordering.system.core.entity.Order;
import com.quangpld.ordering.system.core.entity.OrderDetail;
import com.quangpld.ordering.system.core.entity.OrderDetailBill;
import com.quangpld.ordering.system.core.service.OrderException;
import com.quangpld.ordering.system.core.service.OrderService;
import com.quangpld.ordering.system.core.service.OrderException.Code;

/**
 * OrderServiceImpl class.
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
public class OrderServiceImpl implements OrderService {

  /** Generic DAO object for table ORDER. */
  private GenericDao<Order, Integer> orderDao = null;

  /** Generic DAO object for table ORDERDETAIL. */
  private GenericDao<OrderDetail, Integer> orderDetailDao = null;
  
  /** Generic DAO object for table ORDERDETAILBILL. */
  private GenericDao<OrderDetailBill, Integer> orderDetailBillDao = null;

  /**
   * Default constructor.
   */
  public OrderServiceImpl() {
  }

  /**
   * Setter for orderDao.
   * 
   * @param orderDao
   */
  public void setOrderDao(GenericDao<Order, Integer> orderDao) {
    this.orderDao = orderDao;
  }

  /**
   * Setter for orderDetailDao.
   * 
   * @param orderDetailDao
   */
  public void setOrderDetailDao(GenericDao<OrderDetail, Integer> orderDetailDao) {
    this.orderDetailDao = orderDetailDao;
  }

  /**
   * Setter for orderDetailBillDao.
   * 
   * @param orderDetailBillDao
   */
  public void setOrderDetailBillDao(GenericDao<OrderDetailBill, Integer> orderDetailBillDao) {
    this.orderDetailBillDao = orderDetailBillDao;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void saveOrder(Order order) throws Exception {
    orderDao.saveOrUpdate(order);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Order updateOrder(Order order) throws Exception {
    orderDao.saveOrUpdate(order);
    return order;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Order findOrderById(Integer orderId) throws Exception {
    return orderDao.findById(orderId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<Order> findOrderByExample(Order order) throws Exception {
    return orderDao.findByExample(order);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<Order> findOrderByExample(Order order, int offset, int limit) throws Exception {
    return orderDao.findByExample(order, offset, limit);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deleteOrder(Order order) throws Exception {
    orderDao.delete(order);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void hardDeleteOrder(Order order) throws Exception {
    // TODO:
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getOrdersCount() throws Exception {
    return (int) orderDao.count();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void addOrderDetail(OrderDetail orderDetail) throws Exception {
    Order order = orderDao.findById(orderDetail.getOrderid());
    if (order == null) {
      throw new OrderException(Code.ORDER_NOT_FOUND);
    }
    orderDetailDao.saveOrUpdate(orderDetail);
    order.setLastupdatetime(Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok")).getTime());
    orderDao.saveOrUpdate(order);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<OrderDetail> getOrderDetailByExample(OrderDetail exampleOrderDetail) throws Exception {
    return orderDetailDao.findByExample(exampleOrderDetail);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<OrderDetail> getOrderDetailByExample(OrderDetail exampleOrderDetail, int offset, int limit) throws Exception {
    return orderDetailDao.findByExample(exampleOrderDetail, offset, limit);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public OrderDetail updateOrderDetail(OrderDetail orderDetail) throws Exception {
    Order order = orderDao.findById(orderDetail.getOrderid());
    if (order == null) {
      throw new OrderException(Code.ORDER_NOT_FOUND);
    }
    orderDetailDao.saveOrUpdate(orderDetail);
    order.setLastupdatetime(Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok")).getTime());
    orderDao.saveOrUpdate(order);
    return orderDetail;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void removeOrderDetail(OrderDetail orderDetail) throws Exception {
    Order order = orderDao.findById(orderDetail.getOrderid());
    if (order == null) {
      throw new OrderException(Code.ORDER_NOT_FOUND);
    }
    orderDetailDao.delete(orderDetail);
    order.setLastupdatetime(Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok")).getTime());
    orderDao.saveOrUpdate(order);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void removeAllOrderDetails(int orderId) throws Exception {
    Order order = orderDao.findById(orderId);
    if (order == null) {
      throw new OrderException(Code.ORDER_NOT_FOUND);
    }
    OrderDetail example = new OrderDetail();
    example.setOrderid(orderId);
    List<OrderDetail> orderDetails = orderDetailDao.findByExample(example);
    for (OrderDetail orderDetail : orderDetails) {
      orderDetailDao.delete(orderDetail);
    }
    order.setLastupdatetime(Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok")).getTime());
    orderDao.saveOrUpdate(order);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getOrderDetailsCount(int orderId) throws Exception {
    Order order = orderDao.findById(orderId);
    if (order == null) {
      throw new OrderException(Code.ORDER_NOT_FOUND);
    }
    OrderDetail example = new OrderDetail();
    example.setOrderid(orderId);
    List<OrderDetail> orderDetails = orderDetailDao.findByExample(example);
    return orderDetails.size();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void addOrderDetailBill(OrderDetailBill orderDetailBill) throws Exception {
    //
    OrderDetail orderDetail = orderDetailDao.findById(orderDetailBill.getOrderdetailid());
    if (orderDetail == null) {
      throw new OrderException(Code.ORDER_DETAIL_NOT_FOUND);
    }

    //
    Order order = orderDao.findById(orderDetail.getOrderid());
    if (order == null) {
      throw new OrderException(Code.ORDER_NOT_FOUND);
    }

    //
    orderDetailBillDao.saveOrUpdate(orderDetailBill);
    order.setLastupdatetime(Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok")).getTime());
    orderDao.saveOrUpdate(order);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public OrderDetailBill getOrderDetailBillById(int orderDetailId) throws Exception {
    return orderDetailBillDao.findById(orderDetailId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public OrderDetailBill updateOrderDetailBill(OrderDetailBill orderDetailBill) throws Exception {
    //
    OrderDetail orderDetail = orderDetailDao.findById(orderDetailBill.getOrderdetailid());
    if (orderDetail == null) {
      throw new OrderException(Code.ORDER_DETAIL_NOT_FOUND);
    }

    //
    Order order = orderDao.findById(orderDetail.getOrderid());
    if (order == null) {
      throw new OrderException(Code.ORDER_NOT_FOUND);
    }

    //
    orderDetailBillDao.saveOrUpdate(orderDetailBill);
    order.setLastupdatetime(Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok")).getTime());
    orderDao.saveOrUpdate(order);

    //
    return orderDetailBill;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void removeOrderDetailBill(int orderDetailId) throws Exception {
    //
    OrderDetailBill orderDetailBill = orderDetailBillDao.findById(orderDetailId);
    if (orderDetailBill == null) {
      // TODO:
      return;
    }

    OrderDetail orderDetail = orderDetailDao.findById(orderDetailBill.getOrderdetailid());
    if (orderDetail == null) {
      throw new OrderException(Code.ORDER_DETAIL_NOT_FOUND);
    }

    //
    Order order = orderDao.findById(orderDetail.getOrderid());
    if (order == null) {
      throw new OrderException(Code.ORDER_NOT_FOUND);
    }

    //
    orderDetailBillDao.delete(orderDetailBill);
    order.setLastupdatetime(Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok")).getTime());
    orderDao.saveOrUpdate(order);
  }

}
