package com.quangpld.ordering.system.core.common;

public enum UserRole {
  MEMBER,
  ADMIN,
  SUPER_ADMIN;
}
