package com.quangpld.ordering.system.core.crawling.impl;

import java.util.List;
import java.util.StringTokenizer;

import org.dom4j.Document;
import org.dom4j.Node;
import org.springframework.context.ApplicationContext;

import com.quangpld.core.common.util.StringUtil;
import com.quangpld.core.parser.HtmlParser;
import com.quangpld.core.setting.management.entity.Setting;
import com.quangpld.core.setting.management.service.SettingService;
import com.quangpld.ordering.system.core.common.OrderConstants;
import com.quangpld.ordering.system.core.crawling.Crawler;
import com.quangpld.ordering.system.core.crawling.CrawlerFactory;
import com.quangpld.ordering.system.core.util.OrderUtils;

public class AmazonCrawler implements Crawler {

  /** SettingService object. */
  private SettingService settingService = null;

  private String productLink = null;

  private Document document = null;

  static {
    try {
      CrawlerFactory.registerProduct(OrderConstants.HOST_NAME_AMAZON, new AmazonCrawler());
    } catch (Exception e) {
      // TODO:
    }
  }

  public AmazonCrawler() throws Exception {
    ApplicationContext context = OrderUtils.getClassPathXmlApplicationContext();
    settingService = context.getBean("settingService", SettingService.class);
  }

  public void setProductLink(String productLink) throws Exception {
    this.productLink = productLink;
    document = HtmlParser.parse(productLink);
  }

  @Override
  public Crawler createCrawler() throws Exception {
    return new AmazonCrawler();
  }

  @Override
  public String getProductTitle() throws Exception {
    //
    if (document != null) {
      // Get Title node for product sold by sellers.
      Setting setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_TITLE_NODE);
      Node btAsinTitle = document.selectSingleNode(setting != null ? setting.getSettingvalue()
                                                                   : OrderConstants.AMAZON_PRODUCT_TITLE_NODE_DEFAULT_VALUE);

      // Get Title node for product fulfilled by Amazon.
      if (btAsinTitle == null) {
        setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_FULFILLED_BY_AMAZON_TITLE_NODE);
        btAsinTitle = document.selectSingleNode(setting != null ? setting.getSettingvalue()
                                                                : OrderConstants.AMAZON_PRODUCT_FULFILLED_BY_AMAZON_TITLE_NODE_DEFAULT_VALUE);
      }

      //
      if (btAsinTitle != null) {
        return OrderUtils.buildPrettyProductTitle(btAsinTitle.getText());
      }
    }

    //
    return null;
  }

  @Override
  public double getProductCost() throws Exception {
    if (document != null) {
      // Get Price node for product sold by sellers.
      Setting setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_PRICE_NODE_1);
      Node actualPrice = document.selectSingleNode(setting != null ? setting.getSettingvalue()
                                                                   : OrderConstants.AMAZON_PRODUCT_PRICE_NODE_1_DEFAULT_VALUE);
      if (actualPrice == null) {
        setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_PRICE_NODE_2);
        actualPrice = document.selectSingleNode(setting != null ? setting.getSettingvalue()
                                                                     : OrderConstants.AMAZON_PRODUCT_PRICE_NODE_2_DEFAULT_VALUE);
      }

      // Get Price node for product fulfilled by Amazon.
      if (actualPrice == null) {
        setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_FULFILLED_BY_AMAZON_PRICE_NODE);
        actualPrice = document.selectSingleNode(setting != null ? setting.getSettingvalue()
                                                                : OrderConstants.AMAZON_PRODUCT_FULFILLED_BY_AMAZON_PRICE_NODE_DEFAULT_VALUE);
      }

      // Get Price node for product sold by Amazon.
      if (actualPrice == null) {
        setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_SOLD_BY_AMAZON_PRICE_NODE);
        actualPrice = document.selectSingleNode(setting != null ? setting.getSettingvalue()
                                                                : OrderConstants.AMAZON_PRODUCT_SOLD_BY_AMAZON_PRICE_NODE_DEFAULT_VALUE);
      }

      // Get Price node for mobile.
      if (actualPrice == null && productLink.contains(OrderConstants.URL_AMAZON_MOBILE_PATTERN)) {
        setting = settingService.findSettingById(OrderConstants.AMAZON_MOBILE_PRODUCT_PRICE_NODE);
        List<Node> bodyText = document.selectNodes(setting != null ? setting.getSettingvalue()
                                                                   : OrderConstants.AMAZON_MOBILE_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
        for (Node node : bodyText) {
          double price = OrderUtils.getDoubleValueFromText(node.getText());
          if (price > 0.00d) {
            actualPrice = node;
            break;
          }
        }
      }

      //
      if (actualPrice != null) {
        StringTokenizer st = new StringTokenizer(actualPrice.getText(), "-");
        if (st.countTokens() > 0) {
          return OrderUtils.getDoubleValueFromText(st.nextToken());
        }
      }
    }

    //
    return 0.00d;
  }

  @Override
  public double getProductTaxRate() throws Exception {
    //
//    Setting setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_DEFAULT_TAX_RATE);
//    return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
//                           : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_DEFAULT_TAX_RATE_DEFAULT_VALUE);
    return 0.00d;
  }

  @Override
  public double getProductImportTaxRate() throws Exception {
    // Get default Product Title.
    String productTitle = null;
    Node title = document.selectSingleNode("//TITLE[1]");
    if (title != null) {
      productTitle = title.getText();
    }

    //
    if (StringUtil.isEmpty(productTitle)) {
      return 0.00d;
    }

    //
    Setting setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_KEY);
    String cameraPhoto = setting != null ? setting.getSettingvalue()
                                         : OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_KEY);
    String cellphonesAccessories = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_KEY);
    String videoGames = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_KEY);
    String mp3playersAccessories = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_KEY);
    String computersAccessories = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_KEY);
    String electronics = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_KEY);
    String clothing = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_KEY);
    String shoes = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOE_KEY);
    String shoe = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOE_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_KEY);
    String jewelry = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_KEY);
    String watches = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCH_KEY);
    String watch = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCH_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_KEY);
    String sportsOutdoors = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_KEY);
    String kindle = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_VALUE;

    //
    productTitle = productTitle.toUpperCase();
    if (productTitle.contains(cameraPhoto)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_IMPORT_TAX_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_IMPORT_TAX_VALUE);
    } else if (productTitle.contains(cellphonesAccessories)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_IMPORT_TAX_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_IMPORT_TAX_VALUE);
    } else if (productTitle.contains(videoGames)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_IMPORT_TAX_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_IMPORT_TAX_VALUE);
    } else if (productTitle.contains(mp3playersAccessories)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_IMPORT_TAX_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_IMPORT_TAX_VALUE);
    } else if (productTitle.contains(computersAccessories)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_IMPORT_TAX_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_IMPORT_TAX_VALUE);
    } else if (productTitle.contains(electronics)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_IMPORT_TAX_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_IMPORT_TAX_VALUE);
    } else if (productTitle.contains(clothing)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_IMPORT_TAX_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_IMPORT_TAX_VALUE);
    } else if (productTitle.contains(shoes) || productTitle.contains(shoe)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_IMPORT_TAX_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_IMPORT_TAX_VALUE);
    } else if (productTitle.contains(jewelry)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_IMPORT_TAX_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_IMPORT_TAX_VALUE);
    } else if (productTitle.contains(watches) || productTitle.contains(watch)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_IMPORT_TAX_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_IMPORT_TAX_VALUE);
    } else if (productTitle.contains(sportsOutdoors)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_IMPORT_TAX_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_IMPORT_TAX_VALUE);
    } else if (productTitle.contains(kindle)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_IMPORT_TAX_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_IMPORT_TAX_VALUE);
    }

    //
    return 0.00d;
  }

  @Override
  public double getProductWeight() throws Exception {
    // Get default Product Title.
    String productTitle = null;
    Node title = document.selectSingleNode("//TITLE[1]");
    if (title != null) {
      productTitle = title.getText().toUpperCase();
    }

    //
    Setting setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_KEY);
    String cameraPhoto = setting != null ? setting.getSettingvalue()
                                         : OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_KEY);
    String cellphonesAccessories = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_KEY);
    String videoGames = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_KEY);
    String mp3playersAccessories = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_KEY);
    String computersAccessories = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_KEY);
    String electronics = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_KEY);
    String clothing = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_KEY);
    String shoes = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOE_KEY);
    String shoe = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOE_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_KEY);
    String jewelry = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_KEY);
    String watches = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCH_KEY);
    String watch = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCH_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_KEY);
    String sportsOutdoors = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_KEY);
    String kindle = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_VALUE;

    boolean isPricePerItem = false;
    if (StringUtil.isNotEmpty(productTitle)
        && (productTitle.contains(shoes) || productTitle.contains(watches) || productTitle.contains(cameraPhoto))) {
      isPricePerItem = true;
    }

    //
    if (document != null) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_WEIGHT_NODE);
      Node shippingWeight = document.selectSingleNode(setting != null ? setting.getSettingvalue()
                                                                      : OrderConstants.AMAZON_PRODUCT_WEIGHT_NODE_DEFAULT_VALUE);
      if (shippingWeight != null && !isPricePerItem) {
        StringTokenizer st = new StringTokenizer(shippingWeight.getText().trim(), " ");
        if (st.countTokens() > 2) {
          double weight = 0.00d;
          try {
            weight = Double.parseDouble(st.nextToken());
          } catch (Exception e) {
            // TODO:
          }
          String unit = st.nextToken();
          return OrderUtils.calculateWeight(OrderUtils.convertWeightToKg(weight, unit));
        }
      } else {
        //
        if (StringUtil.isEmpty(productTitle)) {
          return 0.00d;
        }

        //
        if (productTitle.contains(cameraPhoto)) {
          setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_DEFAULT_WEIGHT_KEY);
          return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                                 : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_DEFAULT_WEIGHT_VALUE);
        } else if (productTitle.contains(cellphonesAccessories)) {
          setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_DEFAULT_WEIGHT_KEY);
          return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                                 : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_DEFAULT_WEIGHT_VALUE);
        } else if (productTitle.contains(videoGames)) {
          setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_DEFAULT_WEIGHT_KEY);
          return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                                 : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_DEFAULT_WEIGHT_VALUE);
        } else if (productTitle.contains(mp3playersAccessories)) {
          setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_DEFAULT_WEIGHT_KEY);
          return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                                 : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_DEFAULT_WEIGHT_VALUE);
        } else if (productTitle.contains(computersAccessories)) {
          setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_DEFAULT_WEIGHT_KEY);
          return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                                 : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_DEFAULT_WEIGHT_VALUE);
        } else if (productTitle.contains(electronics)) {
          setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_DEFAULT_WEIGHT_KEY);
          return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                                 : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_DEFAULT_WEIGHT_VALUE);
        } else if (productTitle.contains(clothing)) {
          setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_DEFAULT_WEIGHT_KEY);
          return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                                 : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_DEFAULT_WEIGHT_VALUE);
        } else if (productTitle.contains(shoes) || productTitle.contains(shoe)) {
          setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_DEFAULT_WEIGHT_KEY);
          return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                                 : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_DEFAULT_WEIGHT_VALUE);
        } else if (productTitle.contains(jewelry)) {
          setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_DEFAULT_WEIGHT_KEY);
          return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                                 : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_DEFAULT_WEIGHT_VALUE);
        } else if (productTitle.contains(watches) || productTitle.contains(watch)) {
          setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_DEFAULT_WEIGHT_KEY);
          return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                                 : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_DEFAULT_WEIGHT_VALUE);
        } else if (productTitle.contains(sportsOutdoors)) {
          setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_DEFAULT_WEIGHT_KEY);
          return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                                 : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_DEFAULT_WEIGHT_VALUE);
        } else if (productTitle.contains(kindle)) {
          setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_DEFAULT_WEIGHT_KEY);
          return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                                 : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_DEFAULT_WEIGHT_VALUE);
        }
      }
    }

    //
    return 0.00d;
  }

  @Override
  public double getProductShippingCost() throws Exception {
    // Get default Product Title.
    String productTitle = null;
    Node title = document.selectSingleNode("//TITLE[1]");
    if (title != null) {
      productTitle = title.getText();
    }

    //
    if (StringUtil.isEmpty(productTitle)) {
      return 0.00d;
    }

    //
    Setting setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_KEY);
    String cameraPhoto = setting != null ? setting.getSettingvalue()
                                         : OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_KEY);
    String cellphonesAccessories = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_KEY);
    String videoGames = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_KEY);
    String mp3playersAccessories = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_KEY);
    String computersAccessories = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_KEY);
    String electronics = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_KEY);
    String clothing = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_KEY);
    String shoes = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOE_KEY);
    String shoe = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOE_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_KEY);
    String jewelry = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_KEY);
    String watches = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCH_KEY);
    String watch = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCH_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_KEY);
    String sportsOutdoors = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_VALUE;
    setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_KEY);
    String kindle = setting != null ? setting.getSettingvalue()
                                                   : OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_VALUE;

    //
    productTitle = productTitle.toUpperCase();
    if (productTitle.contains(cameraPhoto)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_SHIPPING_COST_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_SHIPPING_COST_VALUE);
    } else if (productTitle.contains(cellphonesAccessories)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_SHIPPING_COST_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_SHIPPING_COST_VALUE);
    } else if (productTitle.contains(videoGames)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_SHIPPING_COST_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_SHIPPING_COST_VALUE);
    } else if (productTitle.contains(mp3playersAccessories)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_SHIPPING_COST_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_SHIPPING_COST_VALUE);
    } else if (productTitle.contains(computersAccessories)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_SHIPPING_COST_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_SHIPPING_COST_VALUE);
    } else if (productTitle.contains(electronics)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_SHIPPING_COST_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_SHIPPING_COST_VALUE);
    } else if (productTitle.contains(clothing)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_SHIPPING_COST_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_SHIPPING_COST_VALUE);
    } else if (productTitle.contains(shoes) || productTitle.contains(shoe)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_SHIPPING_COST_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_SHIPPING_COST_VALUE);
    } else if (productTitle.contains(jewelry)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_SHIPPING_COST_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_SHIPPING_COST_VALUE);
    } else if (productTitle.contains(watches) || productTitle.contains(watch)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_SHIPPING_COST_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_SHIPPING_COST_VALUE);
    } else if (productTitle.contains(sportsOutdoors)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_SHIPPING_COST_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_SHIPPING_COST_VALUE);
    } else if (productTitle.contains(kindle)) {
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_SHIPPING_COST_KEY);
      return setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                             : OrderUtils.getDoubleValueFromText(OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_SHIPPING_COST_VALUE);
    }

    //
    return 0.00d;
  }

  @Override
  public String getCurrency() throws Exception {
    return OrderConstants.CURRENCY_SYMBOL_USD;
  }

}
