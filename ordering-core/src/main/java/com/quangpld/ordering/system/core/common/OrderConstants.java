package com.quangpld.ordering.system.core.common;

public interface OrderConstants {

  /********************************************************************************************/
  /**                                                                                        **/
  /**                                    HOST_NAME                                           **/
  /**                                                                                        **/
  /********************************************************************************************/

  /** HOST_NAME_AMAZON. */
  public static final String HOST_NAME_AMAZON = "www.amazon.com";

  /** URL_AMAZON_MOBILE_PATTERN. */
  public static final String URL_AMAZON_MOBILE_PATTERN = "www.amazon.com/gp/aw/";

  /** HOST_NAME_EBAY. */
  public static final String HOST_NAME_EBAY = "www.ebay.com";

  /** HOST_NAME_EBAY_MOBILE. */
  public static final String HOST_NAME_EBAY_MOBILE = "item.mobileweb.ebay.com";

  /** HOST_NAME_WALMART. */
  public static final String HOST_NAME_WALMART = "www.walmart.com";

  /** HOST_NAME_WALMART_MOBILE. */
  public static final String HOST_NAME_WALMART_MOBILE = "mobile.walmart.com";

  /** HOST_NAME_RAFFAELLO_NETWORK. */
  public static final String HOST_NAME_RAFFAELLO_NETWORK = "www.raffaello-network.com";

  /** HOST_NAME_NORDSTROM. */
  public static final String HOST_NAME_NORDSTROM = "shop.nordstrom.com";

  /** HOST_NAME_NORDSTROM_MOBILE. */
  public static final String HOST_NAME_NORDSTROM_MOBILE = "m.nordstrom.com";

  /** HOST_NAME_FOREVER21. */
  public static final String HOST_NAME_FOREVER21 = "www.forever21.com";

  /** HOST_NAME_MACYS. */
  public static final String HOST_NAME_MACYS = "www1.macys.com";

  /** HOST_NAME_MACYS_MOBILE. */
  public static final String HOST_NAME_MACYS_MOBILE = "m.macys.com";

  /** HOST_NAME_SEPHORA. */
  public static final String HOST_NAME_SEPHORA = "www.sephora.com";

  /** HOST_NAME_SEPHORA_MOBILE. */
  public static final String HOST_NAME_SEPHORA_MOBILE = "m.sephora.com";

  /** HOST_NAME_BATH_AND_BODY_WORKS. */
  public static final String HOST_NAME_BATH_AND_BODY_WORKS = "www.bathandbodyworks.com";

  /** HOST_NAME_BATH_AND_BODY_WORKS_MOBILE. */
  public static final String HOST_NAME_BATH_AND_BODY_WORKS_MOBILE = "m.bathandbodyworks.com";

  /** HOST_NAME_HEAD_DIRECT. */
  public static final String HOST_NAME_HEAD_DIRECT = "www.head-direct.com";

  /** HOST_NAME_WOO_AUDIO. */
  public static final String HOST_NAME_WOO_AUDIO = "www.wooaudio.com";

  /** HOST_NAME_MOON_AUDIO. */
  public static final String HOST_NAME_MOON_AUDIO = "www.moon-audio.com";

  /********************************************************************************************/
  /**                                                                                        **/
  /**                                 COMMON_CONFIGURATION                                   **/
  /**                                                                                        **/
  /********************************************************************************************/
  public static final String HTTP_SCHEMA = "http://";
  public static final String HTTPS_SCHEMA = "https://";

  public static final String HTML_TAG_BREAK_LINE = "<br />";

  public static final String CURRENCY_SYMBOL_USD = "$";

  public static final String EXCHANGE_RATE_USD_VND = "EXCHANGE_RATE_USD_VND";
  public static final String EXCHANGE_RATE_USD_VND_DEFAULT_VALUE = "21,500.00";

  public static final String UNIT_KILOGRAMS = "kg";
  public static final String UNIT_GRAMS = "grams";
  public static final String UNIT_POUNDS = "pounds";
  public static final String UNIT_OUNCES = "ounces";
  public static final double UNIT_RATE_POUNDS_TO_KILOGRAMS = 0.453592d;
  public static final double UNIT_RATE_OUNCES_TO_KILOGRAMS = 0.0283495d;
  public static final double UNIT_RATE_GRAMS_TO_KILOGRAMS = 0.001d;

  public static final String ETC = "...";

  public static final int PRODUCT_TITLE_MAX_LENGTH = 100;

  public static final String CATEGORIES_TO_BE_DISPLAYED_IN_INDEX_PAGE_KEY = "CATEGORIES_TO_BE_DISPLAYED_IN_INDEX_PAGE_KEY";
  public static final String CATEGORIES_TO_BE_DISPLAYED_IN_INDEX_PAGE_VALUE = null;

  public static final String PRODUCT_DEFAULT_IMPORT_TAX_RATE_KEY = "PRODUCT_DEFAULT_IMPORT_TAX_RATE_KEY";
  public static final String PRODUCT_DEFAULT_IMPORT_TAX_RATE_VALUE = "10.00%";

  public static final String RSS_FEED_1_KEY = "RSS_FEED_1_KEY";
  public static final String RSS_FEED_1_VALUE = "http://www.amazon.com/rss/tag/invicta/popular/ref=tag_tdp_rss_pop_man";
  public static final String RSS_FEED_2_KEY = "RSS_FEED_2_KEY";
  public static final String RSS_FEED_2_VALUE = "http://www.amazon.com/rss/tag/invicta/popular/ref=tag_tdp_rss_pop_man";
  public static final String RSS_FEED_3_KEY = "RSS_FEED_3_KEY";
  public static final String RSS_FEED_3_VALUE = "http://www.amazon.com/rss/tag/invicta/popular/ref=tag_tdp_rss_pop_man";
  public static final String RSS_FEED_4_KEY = "RSS_FEED_4_KEY";
  public static final String RSS_FEED_4_VALUE = "http://www.amazon.com/rss/tag/invicta/popular/ref=tag_tdp_rss_pop_man";
  public static final String RSS_FEED_5_KEY = "RSS_FEED_5_KEY";
  public static final String RSS_FEED_5_VALUE = "http://www.amazon.com/rss/tag/invicta/popular/ref=tag_tdp_rss_pop_man";
  public static final String RSS_FEED_6_KEY = "RSS_FEED_6_KEY";
  public static final String RSS_FEED_6_VALUE = "http://www.amazon.com/rss/tag/invicta/popular/ref=tag_tdp_rss_pop_man";

  public static final String PRODUCT_DEFAULT_ADDITIONAL_RATE_1_KEY = "PRODUCT_DEFAULT_ADDITIONAL_RATE_1_KEY";
  public static final String PRODUCT_DEFAULT_ADDITIONAL_RATE_1_VALUE = "5.00%";
  public static final String PRODUCT_DEFAULT_ADDITIONAL_RATE_2_KEY = "PRODUCT_DEFAULT_ADDITIONAL_RATE_2_KEY";
  public static final String PRODUCT_DEFAULT_ADDITIONAL_RATE_2_VALUE = "6.50%";
  public static final String PRODUCT_DEFAULT_ADDITIONAL_RATE_3_KEY = "PRODUCT_DEFAULT_ADDITIONAL_RATE_3_KEY";
  public static final String PRODUCT_DEFAULT_ADDITIONAL_RATE_3_VALUE = "7.50%";

  public static final String PRODUCT_DEFAULT_WEIGHT_KEY = "PRODUCT_DEFAULT_WEIGHT_KEY";
  public static final String PRODUCT_DEFAULT_WEIGHT_VALUE = "1.00kg";
  public static final String PRODUCT_DEFAULT_SHIPPING_COST_KEY = "PRODUCT_DEFAULT_SHIPPING_COST_KEY";
  public static final String PRODUCT_DEFAULT_SHIPPING_COST_VALUE = "12.00$/kg";

  public static final String PRODUCT_MIN_SHIPPING_VN_COST_KEY = "PRODUCT_MIN_SHIPPING_VN_COST_KEY";
  public static final String PRODUCT_MIN_SHIPPING_VN_COST_VALUE = "6.00$";
  public static final String PRODUCT_MIN_ADDITIONAL_COST_KEY = "PRODUCT_MIN_ADDITIONAL_COST_KEY";
  public static final String PRODUCT_MIN_ADDITIONAL_COST_VALUE = "4.00$";

  public static final String PLACE_HOLDER_HOST_NAME = "\\{hostname\\}";
  public static final String PLACE_HOLDER_CURRENCY_SYMBOL = "\\{currency\\}";
  public static final String PLACE_HOLDER_EXCHANGE_RATE = "\\{exchangeRate\\}";
  public static final String PLACE_HOLDER_PRETTY_ORDER_ID = "\\{prettyOrderId\\}";
  public static final String PLACE_HOLDER_FULLNAME = "\\{fullname\\}";
  public static final String PLACE_HOLDER_ADDRESS = "\\{address\\}";
  public static final String PLACE_HOLDER_EMAIL = "\\{email\\}";
  public static final String PLACE_HOLDER_PHONE = "\\{phone\\}";
  public static final String PLACE_HOLDER_PRODUCT_TITLE = "\\{productTitle\\}";
  public static final String PLACE_HOLDER_PRODUCT_LINK = "\\{productLink\\}";
  public static final String PLACE_HOLDER_PRODUCT_COLOR = "\\{productColor\\}";
  public static final String PLACE_HOLDER_PRODUCT_SIZE = "\\{productSize\\}";
  public static final String PLACE_HOLDER_PRODUCT_QUANTITY = "\\{productQuantity\\}";
  public static final String PLACE_HOLDER_ORIGINAL_COST = "\\{originalCost\\}";
  public static final String PLACE_HOLDER_TOTAL_TAX = "\\{totalTax\\}";
  public static final String PLACE_HOLDER_TOTAL_SHIPPING_INTERNATIONAL_COST = "\\{totalShippingInternationalCost\\}";
  public static final String PLACE_HOLDER_TOTAL_SHIPPING_VIETNAM_COST = "\\{totalShippingVietnamCost\\}";
  public static final String PLACE_HOLDER_TOTAL_SHIPPING_INTERNAL_COST = "\\{totalShippingInternalCost\\}";
  public static final String PLACE_HOLDER_TOTAL_ADDITIONAL_COST = "\\{totalAdditionalCost\\}";
  public static final String PLACE_HOLDER_TOTAL_COST = "\\{totalCost\\}";
  public static final String PLACE_HOLDER_TOTAL_COST_VN = "\\{totalCostVN\\}";

  /********************************************************************************************/
  /**                                                                                        **/
  /**                                  MAIL_CONFIGURATION                                    **/
  /**                                                                                        **/
  /********************************************************************************************/
  public static final String EMAIL_DEFAULT_GMAIL_USERNAME_KEY = "EMAIL_DEFAULT_GMAIL_USERNAME_KEY";
  public static final String EMAIL_DEFAULT_GMAIL_USERNAME_VALUE = "dathang24h.net@gmail.com";
  public static final String EMAIL_DEFAULT_GMAIL_PASSWORD_KEY = "EMAIL_DEFAULT_GMAIL_PASSWORD_KEY";
  public static final String EMAIL_DEFAULT_GMAIL_PASSWORD_VALUE = "dathang24h.net@123";

  public static final String EMAIL_REGISTER_SUCCESSFUL_NOTIFICATION_SUBJECT_KEY = "EMAIL_REGISTER_SUCCESSFUL_NOTIFICATION_SUBJECT_KEY";
  public static final String EMAIL_REGISTER_SUCCESSFUL_NOTIFICATION_SUBJECT_VALUE = "ĐĂNG KÝ THÀNH CÔNG";
  public static final String EMAIL_REGISTER_SUCCESSFUL_NOTIFICATION_CONTENT_KEY = "EMAIL_REGISTER_SUCCESSFUL_NOTIFICATION_CONTENT_KEY";
  public static final String EMAIL_REGISTER_SUCCESSFUL_NOTIFICATION_CONTENT_VALUE = "Xin chào <b>{fullname}</b>," +
                                                                                    "<br />Chúc mừng bạn đã đăng ký thành viên tại website <a href=\"http://dathang24h.net\">dathang24h.net</a> thành công." +
                                                                                    "<br /><br />Thông tin tài khoản:" +
                                                                                    "<br />Tên đăng nhập: <b>{username}</b>" +
                                                                                    "<br />Mật khẩu: <b>{password}</b>" +
                                                                                    "<br /><br />Bạn có thể sử dụng tài khoản thành viên đã đăng ký để bắt đầu đặt hàng và sử dụng các dịch vụ cung cấp bởi website <a href=\"http://dathang24h.net\">dathang24h.net</a>" +
                                                                                    "<br />Xem hướng dẫn đặt hàng trực tuyến tại đây: <a href=\"http://dathang24h.net/ordering/order-instruction.action\">Hướng dẫn đặt hàng trực tuyến</a>" +
                                                                                    "<br />Để được trợ giúp, xin liên hệ tại địa chỉ email: <a href=\"mailto:dathang24h.net@gmail\">dathang24h.net@gmail</a> hoặc Hotline: <b>01.666.75.6666</b>" +
                                                                                    "<br />Xin chân thành cảm ơn.";

  public static final String EMAIL_ORDER_PLACED_ADMIN_NOTIFICATION_SUBJECT_KEY = "EMAIL_ORDER_PLACED_ADMIN_NOTIFICATION_SUBJECT_KEY";
  public static final String EMAIL_ORDER_PLACED_ADMIN_NOTIFICATION_SUBJECT_VALUE = "MÃ ĐẶT HÀNG No.{prettyOrderId} VỪA ĐƯỢC GỬI LÊN HỆ THỐNG";
  public static final String EMAIL_ORDER_PLACED_ADMIN_NOTIFICATION_CONTENT_KEY = "EMAIL_ORDER_PLACED_ADMIN_NOTIFICATION_CONTENT_KEY";
  public static final String EMAIL_ORDER_PLACED_ADMIN_NOTIFICATION_CONTENT_VALUE = "MÃ ĐẶT HÀNG <b>No.{prettyOrderId}</b> VỪA ĐƯỢC GỬI LÊN HỆ THỐNG" +
                                                                                   "<br />" +
                                                                                   "<br /><b>Thông tin người mua hàng:</b>" +
                                                                                   "<br />Họ và tên: {fullname}" +
                                                                                   "<br />Địa chỉ: {address}" +
                                                                                   "<br />E-mail: {email}" +
                                                                                   "<br />Điện thoại: {phone}" +
                                                                                   "<br />" +
                                                                                   "<br /><b>Thông tin đặt hàng:</b>" +
                                                                                   "<br />Tên sản phẩm: <a href=\"{productLink}\">{productTitle}</a>" +
                                                                                   "<br />Màu sắc: {productColor}" +
                                                                                   "<br />Kích cỡ: {productSize}" +
                                                                                   "<br />Số lượng: {productQuantity}" +
                                                                                   "<br />Giá web: <b>{currency}{originalCost}</b>";

  public static final String EMAIL_ORDER_PLACED_USER_NOTIFICATION_SUBJECT_KEY = "EMAIL_ORDER_PLACED_USER_NOTIFICATION_SUBJECT_KEY";
  public static final String EMAIL_ORDER_PLACED_USER_NOTIFICATION_SUBJECT_VALUE = "BẠN VỪA TẠO MÃ ĐẶT HÀNG No.{prettyOrderId} TRÊN HỆ THỐNG WEBSITE dathang24h.net";
  public static final String EMAIL_ORDER_PLACED_USER_NOTIFICATION_CONTENT_KEY = "EMAIL_ORDER_PLACED_USER_NOTIFICATION_CONTENT_KEY";
  public static final String EMAIL_ORDER_PLACED_USER_NOTIFICATION_CONTENT_VALUE = "BẠN VỪA TẠO MÃ ĐẶT HÀNG <b>No.{prettyOrderId}</b> TRÊN HỆ THỐNG WEBSITE dathang24h.net" +
                                                                                  "<br />" +
                                                                                  "<br /><b>Thông tin đặt hàng:</b>" +
                                                                                  "<br />Tên sản phẩm: <a href=\"{productLink}\">{productTitle}</a>" +
                                                                                  "<br />Màu sắc: {productColor}" +
                                                                                  "<br />Kích cỡ: {productSize}" +
                                                                                  "<br />Số lượng: {productQuantity}" +
                                                                                  "<br />Giá web: <b>{currency}{originalCost}</b>" +
                                                                                  "<br />" +
                                                                                  "<br /><b>Chi phí liên quan:</b>" +
                                                                                  "<br />Thuế nhập khẩu: {currency}{totalTax}" +
                                                                                  "<br />Thuế & Phí vận chuyển (Quốc tế): {currency}{totalShippingInternationalCost}" +
                                                                                  "<br />Phí vận chuyển Quốc tế - Việt Nam: {currency}{totalShippingVietnamCost}" +
                                                                                  "<br />Phí vận chuyển nội địa: {currency}{totalShippingInternalCost}" +
                                                                                  "<br />Phí xử lý giao dịch và chênh lệch: {currency}{totalAdditionalCost}" +
                                                                                  "<br />Tổng số tiền: <b>{currency}{totalCost}</b>" +
                                                                                  "<br />Tỷ giá: {exchangeRate}" +
                                                                                  "<br />Tổng số tiền (VNĐ): <b>{totalCostVN} VNĐ</b>" +
                                                                                  "<br />" +
                                                                                  "<br />Các chi phí trên sẽ được dathang24h.net xác nhận, cập nhật chính xác và thông báo đến Quý khách trong thời gian sớm nhất." +
                                                                                  "<br />Xin chân thành cảm ơn Quý khách đã quan tâm và sử dụng dịch vụ.";

  public static final String EMAIL_CHECK_LINK_NOTIFICATION_SUBJECT_KEY = "EMAIL_CHECK_LINK_NOTIFICATION_SUBJECT_KEY";
  public static final String EMAIL_CHECK_LINK_NOTIFICATION_SUBJECT_VALUE = "XỬ LÝ LINK SẢN PHẨM THÀNH CÔNG";
  public static final String EMAIL_CHECK_LINK_NOTIFICATION_CONTENT_KEY = "EMAIL_CHECK_LINK_NOTIFICATION_CONTENT_KEY";
  public static final String EMAIL_CHECK_LINK_NOTIFICATION_CONTENT_VALUE = "Xử lý link sản phẩm thành công: {productLink}";

  public static final String EMAIL_CHECK_LINK_FAIL_NOTIFICATION_SUBJECT_KEY = "EMAIL_CHECK_LINK_FAIL_NOTIFICATION_SUBJECT_KEY";
  public static final String EMAIL_CHECK_LINK_FAIL_NOTIFICATION_SUBJECT_VALUE = "XỬ LÝ LINK SẢN PHẨM KHÔNG THÀNH CÔNG";
  public static final String EMAIL_CHECK_LINK_FAIL_NOTIFICATION_CONTENT_KEY = "EMAIL_CHECK_LINK_FAIL_NOTIFICATION_CONTENT_KEY";
  public static final String EMAIL_CHECK_LINK_FAIL_NOTIFICATION_CONTENT_VALUE = "Xảy ra lỗi trong quá trình xử lý link sản phẩm: {productLink}";

  public static final String EMAIL_GET_CRAWLER_FAIL_NOTIFICATION_SUBJECT_KEY = "EMAIL_GET_CRAWLER_FAIL_NOTIFICATION_SUBJECT_KEY";
  public static final String EMAIL_GET_CRAWLER_FAIL_NOTIFICATION_SUBJECT_VALUE = "KHÔNG TÌM THẤY CRAWLER TƯƠNG ỨNG VỚI HOST {hostname}";
  public static final String EMAIL_GET_CRAWLER_FAIL_NOTIFICATION_CONTENT_KEY = "EMAIL_GET_CRAWLER_FAIL_NOTIFICATION_CONTENT_KEY";
  public static final String EMAIL_GET_CRAWLER_FAIL_NOTIFICATION_CONTENT_VALUE = "Xảy ra lỗi trong quá trình tìm kiếm crawler tương ứng với host: {hostname}" +
                                                                                 "<br />Link sản phẩm: {productLink}";

  public static final String EMAIL_EXCEPTION_NOTIFICATION_SUBJECT_KEY = "EMAIL_EXCEPTION_NOTIFICATION_SUBJECT_KEY";
  public static final String EMAIL_EXCEPTION_NOTIFICATION_SUBJECT_VALUE = "XUẤT HIỆN 1 EXCEPTION TRONG QUÁ TRÌNH XỬ LÝ";

  /********************************************************************************************/
  /**                                                                                        **/
  /**                                 AMAZON_CONFIGURATION                                   **/
  /**                                                                                        **/
  /********************************************************************************************/
  public static final String AMAZON_PRODUCT_TITLE_NODE = "AMAZON_PRODUCT_TITLE_NODE";
  public static final String AMAZON_PRODUCT_TITLE_NODE_DEFAULT_VALUE = "//SPAN[@id='btAsinTitle']";
  public static final String AMAZON_PRODUCT_FULFILLED_BY_AMAZON_TITLE_NODE = "AMAZON_PRODUCT_FULFILLED_BY_AMAZON_TITLE_NODE";
  public static final String AMAZON_PRODUCT_FULFILLED_BY_AMAZON_TITLE_NODE_DEFAULT_VALUE = "//DIV[@id='title_feature_div']/H1[@id='title']";
  public static final String AMAZON_MOBILE_PRODUCT_TITLE_NODE = "AMAZON_MOBILE_PRODUCT_TITLE_NODE";
  public static final String AMAZON_MOBILE_PRODUCT_TITLE_NODE_DEFAULT_VALUE = "//B[1]";

  public static final String AMAZON_PRODUCT_PRICE_NODE_1 = "AMAZON_PRODUCT_PRICE_NODE_1";
  public static final String AMAZON_PRODUCT_PRICE_NODE_1_DEFAULT_VALUE = "//DIV[@id='price_feature_div']/TABLE[@id='price']//SPAN[@class='a-color-price a-size-large']";
  public static final String AMAZON_PRODUCT_PRICE_NODE_2 = "AMAZON_PRODUCT_PRICE_NODE_2";
  public static final String AMAZON_PRODUCT_PRICE_NODE_2_DEFAULT_VALUE = "//SPAN[@id='actualPriceValue']/B[@class='priceLarge']";
  public static final String AMAZON_PRODUCT_FULFILLED_BY_AMAZON_PRICE_NODE = "AMAZON_PRODUCT_FULFILLED_BY_AMAZON_PRICE_NODE";
  public static final String AMAZON_PRODUCT_FULFILLED_BY_AMAZON_PRICE_NODE_DEFAULT_VALUE = "//DIV[@id='priceBlock']//SPAN[@class='priceLarge']";
  public static final String AMAZON_PRODUCT_SOLD_BY_AMAZON_PRICE_NODE = "AMAZON_PRODUCT_SOLD_BY_AMAZON_PRICE_NODE";
  public static final String AMAZON_PRODUCT_SOLD_BY_AMAZON_PRICE_NODE_DEFAULT_VALUE = "//DIV[@id='priceBlock']//B[@class='priceLarge kitsunePrice']";
  public static final String AMAZON_MOBILE_PRODUCT_PRICE_NODE = "AMAZON_MOBILE_PRODUCT_PRICE_NODE";
  public static final String AMAZON_MOBILE_PRODUCT_PRICE_NODE_DEFAULT_VALUE = "//BODY/text()";

  public static final String AMAZON_PRODUCT_WEIGHT_NODE = "AMAZON_PRODUCT_WEIGHT_NODE";
  public static final String AMAZON_PRODUCT_WEIGHT_NODE_DEFAULT_VALUE = "//B[text()='Shipping Weight:']/..";

  public static final String AMAZON_PRODUCT_DEFAULT_TAX_RATE = "AMAZON_PRODUCT_DEFAULT_TAX_RATE";
  public static final String AMAZON_PRODUCT_DEFAULT_TAX_RATE_DEFAULT_VALUE = "0.00%";

  public static final String AMAZON_DEFAULT_WATCH_WEIGHT_KEY = "AMAZON_DEFAULT_WATCH_WEIGHT_KEY";
  public static final String AMAZON_DEFAULT_WATCH_WEIGHT_VALUE = "1.00kg";
  public static final String AMAZON_DEFAULT_SHOE_WEIGHT_KEY = "AMAZON_DEFAULT_SHOE_WEIGHT_KEY";
  public static final String AMAZON_DEFAULT_SHOE_WEIGHT_VALUE = "1.50kg";

  public static final String AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_KEY = "AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_VALUE = "CAMERA & PHOTO";
  public static final String AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_KEY = "AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_VALUE = "CELL PHONES & ACCESSORIES";
  public static final String AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_KEY = "AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_VALUE = "VIDEO GAMES";
  public static final String AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_KEY = "AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_VALUE = "MP3 PLAYERS & ACCESSORIES";
  public static final String AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_KEY = "AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_VALUE = "COMPUTERS & ACCESSORIES";
  public static final String AMAZON_PRODUCT_CATEGORY_ELECTRONICS_KEY = "AMAZON_PRODUCT_CATEGORY_ELECTRONICS_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_ELECTRONICS_VALUE = "ELECTRONICS";
  public static final String AMAZON_PRODUCT_CATEGORY_CLOTHING_KEY = "AMAZON_PRODUCT_CATEGORY_CLOTHING_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_CLOTHING_VALUE = "CLOTHING";
  public static final String AMAZON_PRODUCT_CATEGORY_SHOES_KEY = "AMAZON_PRODUCT_CATEGORY_SHOES_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_SHOES_VALUE = "SHOES";
  public static final String AMAZON_PRODUCT_CATEGORY_SHOE_KEY = "AMAZON_PRODUCT_CATEGORY_SHOE_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_SHOE_VALUE = "SHOE";
  public static final String AMAZON_PRODUCT_CATEGORY_JEWELRY_KEY = "AMAZON_PRODUCT_CATEGORY_JEWELRY_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_JEWELRY_VALUE = "JEWELRY";
  public static final String AMAZON_PRODUCT_CATEGORY_WATCHES_KEY = "AMAZON_PRODUCT_CATEGORY_WATCHES_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_WATCHES_VALUE = "WATCHES";
  public static final String AMAZON_PRODUCT_CATEGORY_WATCH_KEY = "AMAZON_PRODUCT_CATEGORY_WATCH_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_WATCH_VALUE = "WATCH";
  public static final String AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_KEY = "AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_VALUE = "SPORTS & OUTDOORS";
  public static final String AMAZON_PRODUCT_CATEGORY_KINDLE_KEY = "AMAZON_PRODUCT_CATEGORY_KINDLE_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_KINDLE_VALUE = "KINDLE";

  public static final String AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_IMPORT_TAX_KEY = "AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_IMPORT_TAX_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_IMPORT_TAX_VALUE = "10.00%";
  public static final String AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_IMPORT_TAX_KEY = "AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_IMPORT_TAX_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_IMPORT_TAX_VALUE = "10.00%";
  public static final String AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_IMPORT_TAX_KEY = "AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_IMPORT_TAX_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_IMPORT_TAX_VALUE = "10.00%";
  public static final String AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_IMPORT_TAX_KEY = "AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_IMPORT_TAX_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_IMPORT_TAX_VALUE = "5.00%";
  public static final String AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_IMPORT_TAX_KEY = "AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_IMPORT_TAX_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_IMPORT_TAX_VALUE = "5.00%";
  public static final String AMAZON_PRODUCT_CATEGORY_ELECTRONICS_IMPORT_TAX_KEY = "AMAZON_PRODUCT_CATEGORY_ELECTRONICS_IMPORT_TAX_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_ELECTRONICS_IMPORT_TAX_VALUE = "10.00%";
  public static final String AMAZON_PRODUCT_CATEGORY_CLOTHING_IMPORT_TAX_KEY = "AMAZON_PRODUCT_CATEGORY_CLOTHING_IMPORT_TAX_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_CLOTHING_IMPORT_TAX_VALUE = "0.00%";
  public static final String AMAZON_PRODUCT_CATEGORY_SHOES_IMPORT_TAX_KEY = "AMAZON_PRODUCT_CATEGORY_SHOES_IMPORT_TAX_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_SHOES_IMPORT_TAX_VALUE = "5.00%";
  public static final String AMAZON_PRODUCT_CATEGORY_JEWELRY_IMPORT_TAX_KEY = "AMAZON_PRODUCT_CATEGORY_JEWELRY_IMPORT_TAX_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_JEWELRY_IMPORT_TAX_VALUE = "15.00%";
  public static final String AMAZON_PRODUCT_CATEGORY_WATCHES_IMPORT_TAX_KEY = "AMAZON_PRODUCT_CATEGORY_WATCHES_IMPORT_TAX_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_WATCHES_IMPORT_TAX_VALUE = "10.00%";
  public static final String AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_IMPORT_TAX_KEY = "AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_IMPORT_TAX_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_IMPORT_TAX_VALUE = "5.00%";
  public static final String AMAZON_PRODUCT_CATEGORY_KINDLE_IMPORT_TAX_KEY = "AMAZON_PRODUCT_CATEGORY_KINDLE_IMPORT_TAX_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_KINDLE_IMPORT_TAX_VALUE = "5.00%";

  public static final String AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_DEFAULT_WEIGHT_KEY = "AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_DEFAULT_WEIGHT_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_DEFAULT_WEIGHT_VALUE = "1.00item";
  public static final String AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_DEFAULT_WEIGHT_KEY = "AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_DEFAULT_WEIGHT_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_DEFAULT_WEIGHT_VALUE = "1.00kg";
  public static final String AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_DEFAULT_WEIGHT_KEY = "AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_DEFAULT_WEIGHT_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_DEFAULT_WEIGHT_VALUE = "1.00kg";
  public static final String AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_DEFAULT_WEIGHT_KEY = "AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_DEFAULT_WEIGHT_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_DEFAULT_WEIGHT_VALUE = "1.00kg";
  public static final String AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_DEFAULT_WEIGHT_KEY = "AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_DEFAULT_WEIGHT_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_DEFAULT_WEIGHT_VALUE = "1.00kg";
  public static final String AMAZON_PRODUCT_CATEGORY_ELECTRONICS_DEFAULT_WEIGHT_KEY = "AMAZON_PRODUCT_CATEGORY_ELECTRONICS_DEFAULT_WEIGHT_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_ELECTRONICS_DEFAULT_WEIGHT_VALUE = "1.00kg";
  public static final String AMAZON_PRODUCT_CATEGORY_CLOTHING_DEFAULT_WEIGHT_KEY = "AMAZON_PRODUCT_CATEGORY_CLOTHING_DEFAULT_WEIGHT_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_CLOTHING_DEFAULT_WEIGHT_VALUE = "1.00kg";
  public static final String AMAZON_PRODUCT_CATEGORY_SHOES_DEFAULT_WEIGHT_KEY = "AMAZON_PRODUCT_CATEGORY_SHOES_DEFAULT_WEIGHT_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_SHOES_DEFAULT_WEIGHT_VALUE = "1.00item";
  public static final String AMAZON_PRODUCT_CATEGORY_JEWELRY_DEFAULT_WEIGHT_KEY = "AMAZON_PRODUCT_CATEGORY_JEWELRY_DEFAULT_WEIGHT_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_JEWELRY_DEFAULT_WEIGHT_VALUE = "1.00kg";
  public static final String AMAZON_PRODUCT_CATEGORY_WATCHES_DEFAULT_WEIGHT_KEY = "AMAZON_PRODUCT_CATEGORY_WATCHES_DEFAULT_WEIGHT_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_WATCHES_DEFAULT_WEIGHT_VALUE = "1.00item";
  public static final String AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_DEFAULT_WEIGHT_KEY = "AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_DEFAULT_WEIGHT_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_DEFAULT_WEIGHT_VALUE = "1.00kg";
  public static final String AMAZON_PRODUCT_CATEGORY_KINDLE_DEFAULT_WEIGHT_KEY = "AMAZON_PRODUCT_CATEGORY_KINDLE_DEFAULT_WEIGHT_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_KINDLE_DEFAULT_WEIGHT_VALUE = "1.00kg";

  public static final String AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_SHIPPING_COST_KEY = "AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_SHIPPING_COST_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_SHIPPING_COST_VALUE = "30.00$/item";
  public static final String AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_SHIPPING_COST_KEY = "AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_SHIPPING_COST_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_SHIPPING_COST_VALUE = "50.00$/item";
  public static final String AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_SHIPPING_COST_KEY = "AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_SHIPPING_COST_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_SHIPPING_COST_VALUE = "10.00$/kg";
  public static final String AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_SHIPPING_COST_KEY = "AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_SHIPPING_COST_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_SHIPPING_COST_VALUE = "20.00$/item";
  public static final String AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_SHIPPING_COST_KEY = "AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_SHIPPING_COST_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_SHIPPING_COST_VALUE = "13.00$/kg";
  public static final String AMAZON_PRODUCT_CATEGORY_ELECTRONICS_SHIPPING_COST_KEY = "AMAZON_PRODUCT_CATEGORY_ELECTRONICS_SHIPPING_COST_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_ELECTRONICS_SHIPPING_COST_VALUE = "10.00$/kg";
  public static final String AMAZON_PRODUCT_CATEGORY_CLOTHING_SHIPPING_COST_KEY = "AMAZON_PRODUCT_CATEGORY_CLOTHING_SHIPPING_COST_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_CLOTHING_SHIPPING_COST_VALUE = "9.00$/kg";
  public static final String AMAZON_PRODUCT_CATEGORY_SHOES_SHIPPING_COST_KEY = "AMAZON_PRODUCT_CATEGORY_SHOES_SHIPPING_COST_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_SHOES_SHIPPING_COST_VALUE = "13.00$/item";
  public static final String AMAZON_PRODUCT_CATEGORY_JEWELRY_SHIPPING_COST_KEY = "AMAZON_PRODUCT_CATEGORY_JEWELRY_SHIPPING_COST_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_JEWELRY_SHIPPING_COST_VALUE = "16.00$/kg";
  public static final String AMAZON_PRODUCT_CATEGORY_WATCHES_SHIPPING_COST_KEY = "AMAZON_PRODUCT_CATEGORY_WATCHES_SHIPPING_COST_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_WATCHES_SHIPPING_COST_VALUE = "12.00$/item";
  public static final String AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_SHIPPING_COST_KEY = "AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_SHIPPING_COST_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_SHIPPING_COST_VALUE = "12.00$/kg";
  public static final String AMAZON_PRODUCT_CATEGORY_KINDLE_SHIPPING_COST_KEY = "AMAZON_PRODUCT_CATEGORY_KINDLE_SHIPPING_COST_KEY";
  public static final String AMAZON_PRODUCT_CATEGORY_KINDLE_SHIPPING_COST_VALUE = "10.00$/kg";

  /********************************************************************************************/
  /**                                                                                        **/
  /**                                  EBAY_CONFIGURATION                                    **/
  /**                                                                                        **/
  /********************************************************************************************/
  public static final String EBAY_PRODUCT_TITLE_NODE = "EBAY_PRODUCT_TITLE_NODE";
  public static final String EBAY_PRODUCT_TITLE_NODE_DEFAULT_VALUE = "//H1[@id='itemTitle']";

  public static final String EBAY_PRODUCT_PRICE_NODE = "EBAY_PRODUCT_PRICE_NODE";
  public static final String EBAY_PRODUCT_PRICE_NODE_DEFAULT_VALUE = "//SPAN[@id='prcIsum']";

  public static final String EBAY_PRODUCT_CATEGORY_SHOES_KEY = "EBAY_PRODUCT_CATEGORY_SHOES_KEY";
  public static final String EBAY_PRODUCT_CATEGORY_SHOES_VALUE = "SHOES";
  public static final String EBAY_PRODUCT_CATEGORY_WATCHES_KEY = "EBAY_PRODUCT_CATEGORY_WATCHES_KEY";
  public static final String EBAY_PRODUCT_CATEGORY_WATCHES_VALUE = "WATCHES";

  public static final String EBAY_PRODUCT_CATEGORY_SHOES_IMPORT_TAX_KEY = "EBAY_PRODUCT_CATEGORY_SHOES_IMPORT_TAX_KEY";
  public static final String EBAY_PRODUCT_CATEGORY_SHOES_IMPORT_TAX_VALUE = "5%";
  public static final String EBAY_PRODUCT_CATEGORY_WATCHES_IMPORT_TAX_KEY = "EBAY_PRODUCT_CATEGORY_WATCHES_IMPORT_TAX_KEY";
  public static final String EBAY_PRODUCT_CATEGORY_WATCHES_IMPORT_TAX_VALUE = "10%";

  /********************************************************************************************/
  /**                                                                                        **/
  /**                               WALMART_CONFIGURATION                                    **/
  /**                                                                                        **/
  /********************************************************************************************/
  public static final String WALMART_PRODUCT_TITLE_NODE = "WALMART_PRODUCT_TITLE_NODE";
  public static final String WALMART_PRODUCT_TITLE_NODE_DEFAULT_VALUE = "//DIV[@id='item']//DIV[@class='productTitle']//H2";

  public static final String WALMART_PRODUCT_PRICE_NODE = "WALMART_PRODUCT_PRICE_NODE";
  public static final String WALMART_PRODUCT_PRICE_NODE_DEFAULT_VALUE = "//SPAN[@id='buy.price.display']";

  /********************************************************************************************/
  /**                                                                                        **/
  /**                         RAFFAELLO_NETWORK_CONFIGURATION                                **/
  /**                                                                                        **/
  /********************************************************************************************/
  public static final String RAFFAELLO_NETWORK_PRODUCT_TITLE_1_NODE = "RAFFAELLO_NETWORK_PRODUCT_TITLE_1_NODE";
  public static final String RAFFAELLO_NETWORK_PRODUCT_TITLE_1_NODE_DEFAULT_VALUE = "//*[@id='product-range']/*[1]";
  public static final String RAFFAELLO_NETWORK_PRODUCT_TITLE_2_NODE = "RAFFAELLO_NETWORK_PRODUCT_TITLE_2_NODE";
  public static final String RAFFAELLO_NETWORK_PRODUCT_TITLE_2_NODE_DEFAULT_VALUE = "//*[@id='product-range']/*[2]";

  public static final String RAFFAELLO_NETWORK_PRODUCT_PRICE_NODE = "RAFFAELLO_NETWORK_PRODUCT_PRICE_NODE";
  public static final String RAFFAELLO_NETWORK_PRODUCT_PRICE_NODE_DEFAULT_VALUE = "//*[@id='itemprice']";
  public static final String RAFFAELLO_NETWORK_PRODUCT_PRICE_SPECIAL_NODE = "RAFFAELLO_NETWORK_PRODUCT_PRICE_SPECIAL_NODE";
  public static final String RAFFAELLO_NETWORK_PRODUCT_PRICE_SPECIAL_NODE_DEFAULT_VALUE = "//*[@id='specialoffer']";

  /********************************************************************************************/
  /**                                                                                        **/
  /**                                 NORDSTROM_CONFIGURATION                                **/
  /**                                                                                        **/
  /********************************************************************************************/
  public static final String NORDSTROM_PRODUCT_TITLE_1_NODE = "NORDSTROM_PRODUCT_TITLE_1_NODE";
  public static final String NORDSTROM_PRODUCT_TITLE_1_NODE_DEFAULT_VALUE = "//DIV[@class='brand-content']/UL/LI[1]/A";
  public static final String NORDSTROM_PRODUCT_TITLE_2_NODE = "NORDSTROM_PRODUCT_TITLE_2_NODE";
  public static final String NORDSTROM_PRODUCT_TITLE_2_NODE_DEFAULT_VALUE = "//DIV[@class='brand-content']/../H1";
  public static final String NORDSTROM_MOBILE_PRODUCT_TITLE_NODE = "NORDSTROM_MOBILE_PRODUCT_TITLE_NODE";
  public static final String NORDSTROM_MOBILE_PRODUCT_TITLE_NODE_DEFAULT_VALUE = "//DIV[@id='mainContent']//H2[@class='productName']";

  public static final String NORDSTROM_PRODUCT_PRICE_NODE = "NORDSTROM_PRODUCT_PRICE_NODE";
  public static final String NORDSTROM_PRODUCT_PRICE_NODE_DEFAULT_VALUE = "//DIV[@id='itemNumberPriceOuter']//SPAN[@class='price regular']";
  public static final String NORDSTROM_PRODUCT_PRICE_SPECIAL_NODE = "NORDSTROM_PRODUCT_PRICE_SPECIAL_NODE";
  public static final String NORDSTROM_PRODUCT_PRICE_SPECIAL_NODE_DEFAULT_VALUE = "//DIV[@id='itemNumberPriceOuter']//SPAN[@class='price sale']";
  public static final String NORDSTROM_MOBILE_PRODUCT_PRICE_NODE = "NORDSTROM_MOBILE_PRODUCT_PRICE_NODE";
  public static final String NORDSTROM_MOBILE_PRODUCT_PRICE_NODE_DEFAULT_VALUE = "//DIV[@class='price']/H2[@class='currentPrice']/SPAN[1]";

  /********************************************************************************************/
  /**                                                                                        **/
  /**                                FOREVER21_CONFIGURATION                                 **/
  /**                                                                                        **/
  /********************************************************************************************/
  public static final String FOREVER21_PRODUCT_TITLE_NODE = "FOREVER21_PRODUCT_TITLE_NODE";
  public static final String FOREVER21_PRODUCT_TITLE_NODE_DEFAULT_VALUE = "//DIV[@id='product-title-priceblock']//SPAN[@class='productpg-title']";

  public static final String FOREVER21_PRODUCT_PRICE_NODE = "FOREVER21_PRODUCT_PRICE_NODE";
  public static final String FOREVER21_PRODUCT_PRICE_NODE_DEFAULT_VALUE = "//DIV[@id='product-title-priceblock']//SPAN[@class='productpg-price']/FONT/B";

  /********************************************************************************************/
  /**                                                                                        **/
  /**                                 MACYS_CONFIGURATION                                    **/
  /**                                                                                        **/
  /********************************************************************************************/
  public static final String MACYS_PRODUCT_TITLE_NODE = "MACYS_PRODUCT_TITLE_NODE";
  public static final String MACYS_PRODUCT_TITLE_NODE_DEFAULT_VALUE = "//H1[@id='productTitle']";
  public static final String MACYS_MOBILE_PRODUCT_TITLE_NODE = "MACYS_MOBILE_PRODUCT_TITLE_NODE";
  public static final String MACYS_MOBILE_PRODUCT_TITLE_NODE_DEFAULT_VALUE = "//SPAN[@id='productTitle']";

  public static final String MACYS_PRODUCT_PRICE_NODE = "MACYS_PRODUCT_PRICE_NODE";
  public static final String MACYS_PRODUCT_PRICE_NODE_DEFAULT_VALUE = "//DIV[@id='priceInfo']/DIV[@class='standardProdPricingGroup']/SPAN";
  public static final String MACYS_PRODUCT_PRICE_SALE_NODE = "MACYS_PRODUCT_PRICE_SALE_NODE";
  public static final String MACYS_PRODUCT_PRICE_SALE_NODE_DEFAULT_VALUE = "//DIV[@id='priceInfo']/DIV[@class='standardProdPricingGroup']/SPAN[@class='priceSale']";
  public static final String MACYS_MOBILE_PRODUCT_PRICE_NODE = "MACYS_MOBILE_PRODUCT_PRICE_NODE";
  public static final String MACYS_MOBILE_PRODUCT_PRICE_NODE_DEFAULT_VALUE = "//DIV[@id='pdpreviews']//SPAN[@class='regularPrice']";
  public static final String MACYS_MOBILE_PRODUCT_PRICE_SALE_NODE = "MACYS_MOBILE_PRODUCT_PRICE_SALE_NODE";
  public static final String MACYS_MOBILE_PRODUCT_PRICE_SALE_NODE_DEFAULT_VALUE = "//DIV[@id='pdpreviews']//SPAN[@class='priceSale']";

  /********************************************************************************************/
  /**                                                                                        **/
  /**                                 SEPHORA_CONFIGURATION                                  **/
  /**                                                                                        **/
  /********************************************************************************************/
  public static final String SEPHORA_PRODUCT_TITLE_NODE = "SEPHORA_PRODUCT_TITLE_NODE";
  public static final String SEPHORA_PRODUCT_TITLE_NODE_DEFAULT_VALUE = "//DIV[@id='primarySkuInfoArea']//H1/SPAN[@class='OneLinkNoTx']";
  public static final String SEPHORA_MOBILE_PRODUCT_TITLE_NODE = "SEPHORA_MOBILE_PRODUCT_TITLE_NODE";
  public static final String SEPHORA_MOBILE_PRODUCT_TITLE_NODE_DEFAULT_VALUE = "//P[@class='prod-name']";

  public static final String SEPHORA_PRODUCT_PRICE_NODE = "SEPHORA_PRODUCT_PRICE_NODE";
  public static final String SEPHORA_PRODUCT_PRICE_NODE_DEFAULT_VALUE = "//DIV[@id='primarySkuInfo_price']//SPAN[@class='sku-price']/SPAN[@class='list-price']/SPAN[@class='price']";
  public static final String SEPHORA_PRODUCT_PRICE_SALE_NODE = "SEPHORA_PRODUCT_PRICE_SALE_NODE";
  public static final String SEPHORA_PRODUCT_PRICE_SALE_NODE_DEFAULT_VALUE = "//DIV[@id='primarySkuInfo_price']//SPAN[@class='sku-price sale']/SPAN[@class='sale-price']/SPAN[@class='price']";
  public static final String SEPHORA_MOBILE_PRODUCT_PRICE_NODE = "SEPHORA_MOBILE_PRODUCT_PRICE_NODE";
  public static final String SEPHORA_MOBILE_PRODUCT_PRICE_NODE_DEFAULT_VALUE = "//DIV[@id='primarySkuInfo_price']//SPAN[@class='sku-price']/SPAN[@class='list-price']/SPAN[@class='price']";
  public static final String SEPHORA_MOBILE_PRODUCT_PRICE_SALE_NODE = "SEPHORA_MOBILE_PRODUCT_PRICE_SALE_NODE";
  public static final String SEPHORA_MOBILE_PRODUCT_PRICE_SALE_NODE_DEFAULT_VALUE = "//DIV[@id='primarySkuInfo_price']//SPAN[@class='sku-price sale']/SPAN[@class='sale-price']/SPAN[@class='price']";

  /********************************************************************************************/
  /**                                                                                        **/
  /**                           BATH_AND_BODY_WORKS_CONFIGURATION                            **/
  /**                                                                                        **/
  /********************************************************************************************/
  public static final String BATH_AND_BODY_WORKS_PRODUCT_TITLE_BRAND_NODE = "BATH_AND_BODY_WORKS_PRODUCT_TITLE_BRAND_NODE";
  public static final String BATH_AND_BODY_WORKS_PRODUCT_TITLE_BRAND_NODE_DEFAULT_VALUE = "//DIV[@id='product-detail']/DIV[@class='brand']";
  public static final String BATH_AND_BODY_WORKS_PRODUCT_TITLE_FN_NODE = "BATH_AND_BODY_WORKS_PRODUCT_TITLE_FN_NODE";
  public static final String BATH_AND_BODY_WORKS_PRODUCT_TITLE_FN_NODE_DEFAULT_VALUE = "//DIV[@id='product-detail']/H1[@class='fn']";
  public static final String BATH_AND_BODY_WORKS_PRODUCT_TITLE_BRAND_COLOR_NODE = "BATH_AND_BODY_WORKS_PRODUCT_TITLE_BRAND_COLOR_NODE";
  public static final String BATH_AND_BODY_WORKS_PRODUCT_TITLE_BRAND_COLOR_NODE_DEFAULT_VALUE = "//DIV[@id='product-detail']/DIV[@class='brand-color']";
  public static final String BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_BRAND_NODE = "BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_BRAND_NODE";
  public static final String BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_BRAND_NODE_DEFAULT_VALUE = "//DIV[@class='product-details product-view']//P[@class='collection']";
  public static final String BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_FN_NODE = "BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_FN_NODE";
  public static final String BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_FN_NODE_DEFAULT_VALUE = "//DIV[@class='product-details product-view']//P[@class='type']";
  public static final String BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_BRAND_COLOR_NODE = "BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_BRAND_COLOR_NODE";
  public static final String BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_BRAND_COLOR_NODE_DEFAULT_VALUE = "//DIV[@class='product-details product-view']//P[@class='name']";

  public static final String BATH_AND_BODY_WORKS_PRODUCT_PRICE_NODE = "BATH_AND_BODY_WORKS_PRODUCT_PRICE_NODE";
  public static final String BATH_AND_BODY_WORKS_PRODUCT_PRICE_NODE_DEFAULT_VALUE = "//SPAN[@class='product-price base']/DIV[@class='price-ours price-ours-only p-v']/SPAN[@class='value price']";
  public static final String BATH_AND_BODY_WORKS_PRODUCT_PRICE_DISCOUNT_NODE = "BATH_AND_BODY_WORKS_PRODUCT_PRICE_DISCOUNT_NODE";
  public static final String BATH_AND_BODY_WORKS_PRODUCT_PRICE_DISCOUNT_NODE_DEFAULT_VALUE = "//SPAN[@class='product-price discount']/DIV[@class='price-ours price-ours-new p-v']/SPAN[@class='value price']";
  public static final String BATH_AND_BODY_WORKS_MOBILE_PRODUCT_PRICE_NODE = "BATH_AND_BODY_WORKS_MOBILE_PRODUCT_PRICE_NODE";
  public static final String BATH_AND_BODY_WORKS_MOBILE_PRODUCT_PRICE_NODE_DEFAULT_VALUE = "//DIV[@class='product-details product-view']//P[@class='price']";
  public static final String BATH_AND_BODY_WORKS_MOBILE_PRODUCT_PRICE_DISCOUNT_NODE = "BATH_AND_BODY_WORKS_MOBILE_PRODUCT_PRICE_DISCOUNT_NODE";
  public static final String BATH_AND_BODY_WORKS_MOBILE_PRODUCT_PRICE_DISCOUNT_NODE_DEFAULT_VALUE = "//DIV[@class='product-details product-view']//P[@class='price']/SPAN[@class='new-price']";

  /********************************************************************************************/
  /**                                                                                        **/
  /**                                HEAD_DIRECT_CONFIGURATION                               **/
  /**                                                                                        **/
  /********************************************************************************************/
  public static final String HEAD_DIRECT_PRODUCT_TITLE_BRAND_NODE = "HEAD_DIRECT_PRODUCT_TITLE_BRAND_NODE";
  public static final String HEAD_DIRECT_PRODUCT_TITLE_BRAND_NODE_DEFAULT_VALUE = "//*[@class='xxjs']/*[@class='xxjs_h']";
  public static final String HEAD_DIRECT_PRODUCT_TITLE_NODE = "HEAD_DIRECT_PRODUCT_TITLE_NODE";
  public static final String HEAD_DIRECT_PRODUCT_TITLE_NODE_DEFAULT_VALUE = "//*[@class='xxjs']/*[@class='mingche1']";

  public static final String HEAD_DIRECT_PRODUCT_PRICE_NODE = "HEAD_DIRECT_PRODUCT_PRICE_NODE";
  public static final String HEAD_DIRECT_PRODUCT_PRICE_NODE_DEFAULT_VALUE = "//*[@class='xxjs']//*[@size='-2']";
  public static final String HEAD_DIRECT_PRODUCT_PRICE_SALE_NODE = "HEAD_DIRECT_PRODUCT_PRICE_SALE_NODE";
  public static final String HEAD_DIRECT_PRODUCT_PRICE_SALE_NODE_DEFAULT_VALUE = "//*[@class='xxjs']//*[@color='red']";

  /********************************************************************************************/
  /**                                                                                        **/
  /**                                 WOO_AUDIO_CONFIGURATION                                **/
  /**                                                                                        **/
  /********************************************************************************************/
  public static final String WOO_AUDIO_PRODUCT_TITLE_NODE = "WOO_AUDIO_PRODUCT_TITLE_NODE";
  public static final String WOO_AUDIO_PRODUCT_TITLE_NODE_DEFAULT_VALUE = "//DIV[@id='header']";

  public static final String WOO_AUDIO_PRODUCT_PRICE_NODE = "WOO_AUDIO_PRODUCT_PRICE_NODE";
  public static final String WOO_AUDIO_PRODUCT_PRICE_NODE_DEFAULT_VALUE = "//DIV[@class='orderNow']//SPAN[@class='price']";

  /********************************************************************************************/
  /**                                                                                        **/
  /**                                MOON_AUDIO_CONFIGURATION                                **/
  /**                                                                                        **/
  /********************************************************************************************/
  public static final String MOON_AUDIO_PRODUCT_TITLE_NODE = "MOON_AUDIO_PRODUCT_TITLE_NODE";
  public static final String MOON_AUDIO_PRODUCT_TITLE_NODE_DEFAULT_VALUE = "//DIV[@class='product-name']/H1";

  public static final String MOON_AUDIO_PRODUCT_PRICE_NODE = "MOON_AUDIO_PRODUCT_PRICE_NODE";
  public static final String MOON_AUDIO_PRODUCT_PRICE_NODE_DEFAULT_VALUE = "//DIV[@class='product-shop']//DIV[@class='price-box']//SPAN[@class='price']";
  public static final String MOON_AUDIO_PRODUCT_PRICE_SALE_NODE = "MOON_AUDIO_PRODUCT_PRICE_SALE_NODE";
  public static final String MOON_AUDIO_PRODUCT_PRICE_SALE_NODE_DEFAULT_VALUE = "//DIV[@class='product-shop']//DIV[@class='price-box']/P[@class='special-price']/SPAN[@class='price']";
}
