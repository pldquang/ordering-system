package com.quangpld.ordering.system.core.rss;

import java.util.List;

public interface RssReader {

  String getTitle() throws Exception;

  String getLink() throws Exception;

  String getDescription() throws Exception;

  String getPublishedDate() throws Exception;

  String getLastBuildDate() throws Exception;

  String getTtl() throws Exception;

  String getGenerator() throws Exception;

  String getLanguage() throws Exception;

  String getCopyright() throws Exception;

  List<RssItem> getItems() throws Exception;

}
