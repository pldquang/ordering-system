/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2013-02-03
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-02-03    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.ordering.system.core.service;

/**
 * OrderException class.
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
public class OrderException extends Exception {

  /** serialVersionUID. */
  private static final long serialVersionUID = -7856411107875938359L;

  public enum Code {
    ORDER_NOT_FOUND,
    ORDER_DETAIL_NOT_FOUND;
  }

  /** Exception Code. */
  private final Code code;

  /**
   * 
   * @param code
   */
  public OrderException(Code code) {
    this.code = code;
  }

  /**
   * 
   * @param code
   * @param cause
   */
  public OrderException(Code code, Throwable cause) {
    super(cause);
    this.code = code;
  }

  /**
   * 
   * @param code
   * @param message
   * @param cause
   */
  public OrderException(Code code, String message, Throwable cause) {
    super(message, cause);
    this.code = code;
  }

  /**
   * 
   * @param code
   * @param message
   */
  public OrderException(Code code, String message) {
    super(message);
    this.code = code;
  }

  /**
   * Getter for code.
   * 
   * @return
   */
  public Code getCode() {
    return code;
  }

}
