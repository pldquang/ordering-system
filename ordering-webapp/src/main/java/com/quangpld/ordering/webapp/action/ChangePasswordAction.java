package com.quangpld.ordering.webapp.action;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.context.ApplicationContext;

import com.opensymphony.xwork2.ActionSupport;
import com.quangpld.core.common.util.StringUtil;
import com.quangpld.core.user.management.entity.User;
import com.quangpld.core.user.management.service.UserService;
import com.quangpld.ordering.webapp.util.OrderUtils;

public class ChangePasswordAction extends ActionSupport {

  /** serialVersionUID. */
  private static final long serialVersionUID = 2888332365392982579L;

  /** UserService object. */
  private UserService userService = null;

  private User currentUser = null;

  private String password;
  private String newPassword;
  private String newPasswordConfirm;

  private String message;

  public ChangePasswordAction() throws Exception {
    try {
      //
      ApplicationContext context = OrderUtils.getClassPathXmlApplicationContext();
      userService = context.getBean("userService", UserService.class);

      //
      currentUser = userService.findUserById(SecurityUtils.getSubject().getPrincipal().toString());
    } catch (Exception e) {
      OrderUtils.sendExceptionMailNotification(e);
    }
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getNewPassword() {
    return newPassword;
  }

  public void setNewPassword(String newPassword) {
    this.newPassword = newPassword;
  }

  public String getNewPasswordConfirm() {
    return newPasswordConfirm;
  }

  public void setNewPasswordConfirm(String newPasswordConfirm) {
    this.newPasswordConfirm = newPasswordConfirm;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public String execute() throws Exception {
    //
    try {
      if (isReadyForSubmit()) {
        currentUser.setPassword(new Md5Hash(newPassword).toBase64());
        userService.updateUser(currentUser);
        message = getText("dathang24h.change-password.message.successful");
      }
    } catch (Exception e) {
      OrderUtils.sendExceptionMailNotification(e);
    }

    //
    return SUCCESS;
  }

  private boolean isReadyForSubmit() {
    //
    if (StringUtil.isNotEmpty(password) && StringUtil.isNotEmpty(newPassword) && StringUtil.isNotEmpty(newPasswordConfirm)) {
      //
      if (!new Md5Hash(password).toBase64().equals(currentUser.getPassword())) {
        message = getText("dathang24h.change-password.message.wrong-password");
        return false;
      }

      //
      if (newPassword.length() < 6) {
        message = getText("dathang24h.change-password.message.min-length-password=");
        return false;
      }

      //
      if (!newPasswordConfirm.equals(newPassword)) {
        message = getText("dathang24h.change-password.message.wrong-password-confirm");
        return false;
      }

      //
      return true;
    }

    //
    return false;
  }

}
