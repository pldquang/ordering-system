package com.quangpld.ordering.webapp.util;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.quangpld.core.common.mail.GmailTLSSender;
import com.quangpld.core.common.mail.MailSender;
import com.quangpld.core.common.util.StringUtil;
import com.quangpld.core.setting.management.entity.Setting;
import com.quangpld.core.setting.management.service.SettingService;
import com.quangpld.core.user.management.service.UserService;
import com.quangpld.ordering.system.core.common.OrderConstants;
import com.quangpld.ordering.system.core.common.UserRole;

public class OrderUtils {

  /** USERNAME_REGEX. */
  public static final String USERNAME_REGEX = "^[a-zA-Z0-9\\_]+$";

  /** PHONE_REGEX. */
  public static final String PHONE_REGEX = "^[\\d\\s()\\.+\\-]{3,20}$";

  /** EMAIL_REGEX. */
  public static final String EMAIL_REGEX = "^([a-z0-9][-a-z0-9_\\+\\.]*[a-z0-9])@([a-z0-9][-a-z0-9\\.]*[a-z0-9]\\.(arpa|root|aero|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)|([0-9]{1,3}\\.{3}[0-9]{1,3}))$";

  /**
   * 
   * @return
   * @throws Exception
   */
  public static ApplicationContext getClassPathXmlApplicationContext() throws Exception {
    return new ClassPathXmlApplicationContext(new String[] {"application-context-ordering-webapp.xml"});
  }

  /**
   * 
   * @param username
   * @throws Exception
   */
  public static boolean isAdmin(String username) throws Exception {
    //
    if (StringUtil.isEmpty(username)) {
      return false;
    }

    //
    ApplicationContext applicationContext = getClassPathXmlApplicationContext();
    UserService userService = applicationContext.getBean("userService", UserService.class);
    List<String> userRoles = userService.getUserRoles(username);
    for (String role : userRoles) {
      if (UserRole.ADMIN.toString().equals(role)) {
        return true;
      }
    }
    return false;
  }

  /**
   * 
   * @param link
   * @return
   * @throws Exception
   */
  public static String makeValidLink(String link) throws Exception {
    if (link.startsWith(OrderConstants.HTTP_SCHEMA) || link.startsWith(OrderConstants.HTTPS_SCHEMA)) {
      return link;
    } else {
      return OrderConstants.HTTP_SCHEMA + link;
    }
  }

  /**
   * 
   * @param title
   * @return
   * @throws Exception
   */
  public static String buildPrettyProductTitle(String title) throws Exception {
    return title.length() <= OrderConstants.PRODUCT_TITLE_MAX_LENGTH ? title : title.substring(0, OrderConstants.PRODUCT_TITLE_MAX_LENGTH) + OrderConstants.ETC;
  }

  /**
   * 
   * @param str
   * @return
   * @throws Exception
   */
  public static double getDoubleValueFromText(String str) throws Exception {
    double value = 0.00d;
    try {
      value = Double.parseDouble(str.replaceAll("[^\\d\\.]", ""));
    } catch (Exception e) {
      // TODO:
    }
    return value;
  }

  /**
   * 
   * @param weight
   * @param unit
   * @return
   */
  public static double convertWeightToKg(double weight, String unit) {
    try {
      if (OrderConstants.UNIT_KILOGRAMS.equals(unit)) {
        return weight;
      } else if (OrderConstants.UNIT_POUNDS.equals(unit)) {
        return weight * OrderConstants.UNIT_RATE_POUNDS_TO_KILOGRAMS;
      } else if (OrderConstants.UNIT_OUNCES.equals(unit)) {
        return weight * OrderConstants.UNIT_RATE_OUNCES_TO_KILOGRAMS;
      } else if (OrderConstants.UNIT_GRAMS.equals(unit)) {
        return weight * OrderConstants.UNIT_RATE_GRAMS_TO_KILOGRAMS;
      }
    } catch (Exception e) {
      // TODO:
    }
    return 0.0;
  }

  /**
   * 
   * @param weight
   * @return
   */
  public static double calculateWeight(double weight) {
    if (weight < 0.50) {
      return weight * 2.00;
    } else if (0.50 <= weight && weight < 1.00) {
      return weight * 1.75;
    } else if (1.00 <= weight && weight < 10.00) {
      return weight * 1.50;
    } else if (10.00 <= weight && weight < 20.00) {
      return weight * 1.35;
    } else {
      return weight * 1.15;
    }
  }

  public static void sendAdminMailNotification(String subject, String content) throws Exception {
    //
    SettingService settingService = getClassPathXmlApplicationContext().getBean("settingService", SettingService.class);
    Setting setting = settingService.findSettingById(OrderConstants.EMAIL_DEFAULT_GMAIL_USERNAME_KEY);
    String emailUsername = setting != null ? setting.getSettingvalue() : OrderConstants.EMAIL_DEFAULT_GMAIL_USERNAME_VALUE;
    setting = settingService.findSettingById(OrderConstants.EMAIL_DEFAULT_GMAIL_PASSWORD_KEY);
    String emailPassword = setting != null ? setting.getSettingvalue() : OrderConstants.EMAIL_DEFAULT_GMAIL_PASSWORD_VALUE;

    //
    MailSender mailSender = new GmailTLSSender(emailUsername, emailPassword, emailUsername, emailUsername, subject, content);
    mailSender.sendMail();
  }

  public static void sendExceptionMailNotification(Exception e) throws Exception {
    //
    SettingService settingService = getClassPathXmlApplicationContext().getBean("settingService", SettingService.class);
    Setting setting = settingService.findSettingById(OrderConstants.EMAIL_EXCEPTION_NOTIFICATION_SUBJECT_KEY);
    String subject = setting != null ? setting.getSettingvalue()
                                     : OrderConstants.EMAIL_EXCEPTION_NOTIFICATION_SUBJECT_VALUE;
    StringBuilder sb = new StringBuilder();
    for (StackTraceElement stackTraceElement : e.getStackTrace()) {
      sb.append(stackTraceElement.toString());
      sb.append(OrderConstants.HTML_TAG_BREAK_LINE);
    }

    //
    sendAdminMailNotification(StringUtil.isNotEmpty(e.getMessage()) ? e.getMessage() : subject, sb.toString());
  }

}
