/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2013-01-27
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-01-27    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.ordering.webapp.action;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.dom4j.Document;
import org.dom4j.Node;
import org.springframework.context.ApplicationContext;

import com.opensymphony.xwork2.ActionSupport;
import com.quangpld.core.common.util.HttpUtils;
import com.quangpld.core.common.util.StringUtil;
import com.quangpld.core.parser.HtmlParser;
import com.quangpld.core.setting.management.entity.Setting;
import com.quangpld.core.setting.management.service.SettingService;
import com.quangpld.core.user.management.entity.User;
import com.quangpld.core.user.management.service.UserService;
import com.quangpld.ordering.system.core.common.OrderConstants;
import com.quangpld.ordering.system.core.crawling.Crawler;
import com.quangpld.ordering.system.core.crawling.CrawlerFactory;
import com.quangpld.ordering.webapp.util.OrderUtils;

/**
 * OrderAction class.
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
public class OrderAction extends ActionSupport {

  /** serialVersionUID. */
  private static final long serialVersionUID = 5806161610224093624L;

  /** SettingService object. */
  private SettingService settingService = null;

  /** UserService object. */
  private UserService userService = null;

  private String prettyOrderId;
  private String username;
  private String fullname;
  private String phone;
  private String email;
  private String address;
  private String itemName;
  private String itemLink;
  private String itemColor;
  private String itemSize;
  private String itemQuantity;
  private String itemUnit;
  private String itemNote;

  private String currency;
  private double exchangeRate;
  private double originalCost;
  private double totalOriginalCost;
  private double tax;
  private double totalTax;
  private double shippingInternationalCost;
  private double totalShippingInternationalCost;
  private double shippingVietnamCost;
  private double totalShippingVietnamCost;
  private double shippingInternalCost;
  private double totalShippingInternalCost;
  private double additionalCost;
  private double totalAdditionalCost;
  private double totalCost;
  private double totalCostVN;

  private String errorMessage;
  private DecimalFormat decimalFormat;

  /**
   * Default constructor.
   * @throws Exception 
   */
  public OrderAction() throws Exception {
    try {
      //
      ApplicationContext context = OrderUtils.getClassPathXmlApplicationContext();
      userService = context.getBean("userService", UserService.class);
      settingService = context.getBean("settingService", SettingService.class);

      //
      DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
      decimalFormatSymbols.setDecimalSeparator('.');
      decimalFormatSymbols.setGroupingSeparator(',');
      decimalFormat = new DecimalFormat("#,##0.00", decimalFormatSymbols);

      //
      Subject currentUser = SecurityUtils.getSubject();
      if (currentUser.getPrincipal() != null) {
        User user = userService.findUserById(currentUser.getPrincipal().toString());
        username = user.getUsername();
        fullname = user.getFirstname();
        phone = user.getMobilephone();
        email = user.getEmail();
        address = user.getAddress1();
      }
    } catch (Exception e) {
      errorMessage = getText("dathang24h.order-placed.message.error");
      OrderUtils.sendExceptionMailNotification(e);
    }
  }

  public String getPrettyOrderId() {
    return prettyOrderId;
  }

  public void setPrettyOrderId(String prettyOrderId) {
    this.prettyOrderId = prettyOrderId;
  }

  public double getExchangeRate() {
    return exchangeRate;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getFullname() {
    return fullname;
  }

  public void setFullname(String fullname) {
    this.fullname = fullname;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getItemName() {
    return itemName;
  }

  public void setItemName(String itemName) {
    this.itemName = itemName;
  }

  public String getItemLink() {
    return itemLink;
  }

  public void setItemLink(String itemLink) {
    this.itemLink = itemLink;
  }

  public String getItemColor() {
    return itemColor;
  }

  public void setItemColor(String itemColor) {
    this.itemColor = itemColor;
  }

  public String getItemSize() {
    return itemSize;
  }

  public void setItemSize(String itemSize) {
    this.itemSize = itemSize;
  }

  public String getItemQuantity() {
    return itemQuantity;
  }

  public void setItemQuantity(String itemQuantity) {
    this.itemQuantity = itemQuantity;
  }

  public String getItemUnit() {
    return itemUnit;
  }

  public void setItemUnit(String itemUnit) {
    this.itemUnit = itemUnit;
  }

  public String getItemNote() {
    return itemNote;
  }

  public void setItemNote(String itemNote) {
    this.itemNote = itemNote;
  }

  public String getCurrency() {
    return currency;
  }

  public double getOriginalCost() {
    return originalCost;
  }

  public double getTotalOriginalCost() {
    return totalOriginalCost;
  }

  public double getTax() {
    return tax;
  }

  public double getTotalTax() {
    return totalTax;
  }

  public double getShippingInternationalCost() {
    return shippingInternationalCost;
  }

  public double getTotalShippingInternationalCost() {
    return totalShippingInternationalCost;
  }

  public double getShippingVietnamCost() {
    return shippingVietnamCost;
  }

  public double getTotalShippingVietnamCost() {
    return totalShippingVietnamCost;
  }

  public double getShippingInternalCost() {
    return shippingInternalCost;
  }

  public double getTotalShippingInternalCost() {
    return totalShippingInternalCost;
  }

  public double getAdditionalCost() {
    return additionalCost;
  }

  public double getTotalAdditionalCost() {
    return totalAdditionalCost;
  }

  public double getTotalCost() {
    return totalCost;
  }

  public double getTotalCostVN() {
    return totalCostVN;
  }

  public String getPrettyOriginalCost() {
    return decimalFormat.format(originalCost);
  }

  public String getPrettyTotalOriginalCost() {
    return decimalFormat.format(totalOriginalCost);
  }

  public String getPrettyTax() {
    return decimalFormat.format(tax);
  }

  public String getPrettyTotalTax() {
    return decimalFormat.format(totalTax);
  }

  public String getPrettyShippingInternationalCost() {
    return decimalFormat.format(shippingInternationalCost);
  }

  public String getPrettyTotalShippingInternationalCost() {
    return decimalFormat.format(totalShippingInternationalCost);
  }

  public String getPrettyShippingVietnamCost() {
    return decimalFormat.format(shippingVietnamCost);
  }

  public String getPrettyTotalShippingVietnamCost() {
    return decimalFormat.format(totalShippingVietnamCost);
  }

  public String getPrettyShippingInternalCost() {
    return decimalFormat.format(shippingInternalCost);
  }

  public String getPrettyTotalShippingInternalCost() {
    return decimalFormat.format(totalShippingInternalCost);
  }

  public String getPrettyAdditionalCost() {
    return decimalFormat.format(additionalCost);
  }

  public String getPrettyTotalAdditionalCost() {
    return decimalFormat.format(totalAdditionalCost);
  }

  public String getPrettyTotalCost() {
    return decimalFormat.format(totalCost);
  }

  public String getPrettyTotalCostVN() {
    return decimalFormat.format(totalCostVN);
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  @Override
  public String execute() throws Exception {
    try {
      //
      if (!isReadyForSubmit()) {
        return INPUT;
      }

      //
      itemLink = OrderUtils.makeValidLink(itemLink);
      String hostName = HttpUtils.getHostName(itemLink);
      Setting setting = null;

      // Get appropriate crawler.
      Crawler crawler = null;
      try {
        crawler = CrawlerFactory.instance().createCrawler(hostName);
      } catch (Exception e) {
        //
        setting = settingService.findSettingById(OrderConstants.EMAIL_GET_CRAWLER_FAIL_NOTIFICATION_SUBJECT_KEY);
        String subject = setting != null ? setting.getSettingvalue()
                                              : OrderConstants.EMAIL_GET_CRAWLER_FAIL_NOTIFICATION_SUBJECT_VALUE;
        subject = subject.replaceAll(OrderConstants.PLACE_HOLDER_HOST_NAME, hostName);

        //
        setting = settingService.findSettingById(OrderConstants.EMAIL_GET_CRAWLER_FAIL_NOTIFICATION_CONTENT_KEY);
        String content = setting != null ? setting.getSettingvalue()
                                              : OrderConstants.EMAIL_GET_CRAWLER_FAIL_NOTIFICATION_CONTENT_VALUE;
        content = content.replaceAll(OrderConstants.PLACE_HOLDER_HOST_NAME, hostName);
        content = content.replaceAll(OrderConstants.PLACE_HOLDER_PRODUCT_LINK, itemLink);

        //
        OrderUtils.sendAdminMailNotification(subject, content);
      }

      if (crawler != null) {
        //
        crawler.setProductLink(itemLink);
        double quantity = Double.parseDouble(itemQuantity);

        // Get Product Currency info.
        currency = crawler.getCurrency();

        // Get Product Title info.
        if (StringUtil.isEmpty(itemName)) {
          itemName = crawler.getProductTitle();
        }

        // Get Product Original Cost info.
        originalCost = crawler.getProductCost();
        totalOriginalCost = originalCost * quantity;

        // Calculate Product Tax info.
        double importTaxRate = crawler.getProductImportTaxRate();
        tax = originalCost * importTaxRate / 100;
        totalTax = tax * quantity;

        // Calculate Product Tax and Shipping International Cost info.
        double itemTaxRate = crawler.getProductTaxRate();
        shippingInternationalCost = originalCost * itemTaxRate / 100;
        totalShippingInternationalCost = shippingInternationalCost * quantity;

        // Get Product Weight info.
        double itemWeight = crawler.getProductWeight();
        if (itemWeight <= 0) {
          setting = settingService.findSettingById(OrderConstants.PRODUCT_DEFAULT_WEIGHT_KEY);
          itemWeight =  setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                                        : OrderUtils.getDoubleValueFromText(OrderConstants.PRODUCT_DEFAULT_WEIGHT_VALUE);
        }

        // Get shipping cost info.
        double costPerKg = crawler.getProductShippingCost();
        if (costPerKg <= 0) {
          setting = settingService.findSettingById(OrderConstants.PRODUCT_DEFAULT_SHIPPING_COST_KEY);
          costPerKg =  setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                                       : OrderUtils.getDoubleValueFromText(OrderConstants.PRODUCT_DEFAULT_SHIPPING_COST_VALUE);
        }

        // Calculate Product Shipping to Vietnam Cost info.
        double minShippingVietnamCost = setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                                                        : OrderUtils.getDoubleValueFromText(OrderConstants.PRODUCT_MIN_SHIPPING_VN_COST_VALUE);
        shippingVietnamCost = itemWeight * costPerKg;
        if (shippingVietnamCost < minShippingVietnamCost) shippingVietnamCost = minShippingVietnamCost;
        totalShippingVietnamCost = shippingVietnamCost * quantity;

        // Calculate Product Additional Cost info.
        setting = settingService.findSettingById(OrderConstants.PRODUCT_MIN_ADDITIONAL_COST_KEY);
        double minAdditionalCost = setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                                                   : OrderUtils.getDoubleValueFromText(OrderConstants.PRODUCT_MIN_ADDITIONAL_COST_VALUE);
        double additionalRate = OrderUtils.getDoubleValueFromText(calculateAdditionalRate(originalCost));
        additionalCost = originalCost * additionalRate / 100;
        if (additionalCost < minAdditionalCost) additionalCost = minAdditionalCost;
        totalAdditionalCost = additionalCost * quantity;

        // Calculate Product Total Cost info.
        totalCost = totalOriginalCost + totalTax + totalShippingInternationalCost
                    + totalShippingVietnamCost + totalShippingInternalCost + totalAdditionalCost;

        // Calculate Product Total Cost in VND info.
        if (OrderConstants.CURRENCY_SYMBOL_USD.equals(currency)) {
          setting = settingService.findSettingById(OrderConstants.EXCHANGE_RATE_USD_VND);
          exchangeRate = setting != null ? OrderUtils.getDoubleValueFromText(setting.getSettingvalue())
                                         : OrderUtils.getDoubleValueFromText(OrderConstants.EXCHANGE_RATE_USD_VND_DEFAULT_VALUE);
        }
        totalCostVN = totalCost * exchangeRate;
      }

      // Get default Product Title.
      if (StringUtil.isEmpty(itemName)) {
        Document document = HtmlParser.parse(itemLink);
        Node title = document.selectSingleNode("//TITLE[1]");
        if (title != null) {
          itemName = OrderUtils.buildPrettyProductTitle(title.getText());
        } else {
          itemName = OrderUtils.buildPrettyProductTitle(itemLink);
        }
      }

      // Send mail notification in case getting Product Original Cost info fail.
      if (originalCost == 0) {
        setting = settingService.findSettingById(OrderConstants.EMAIL_CHECK_LINK_FAIL_NOTIFICATION_SUBJECT_KEY);
        String subject = setting != null ? setting.getSettingvalue()
                                              : OrderConstants.EMAIL_CHECK_LINK_FAIL_NOTIFICATION_SUBJECT_VALUE;
        setting = settingService.findSettingById(OrderConstants.EMAIL_CHECK_LINK_FAIL_NOTIFICATION_CONTENT_KEY);
        String content = setting != null ? setting.getSettingvalue()
                                              : OrderConstants.EMAIL_CHECK_LINK_FAIL_NOTIFICATION_CONTENT_VALUE;
        content = content.replaceAll(OrderConstants.PLACE_HOLDER_PRODUCT_LINK, itemLink);
        OrderUtils.sendAdminMailNotification(subject, content);

        //
        tax = 0.00d;
        totalTax = 0.00d;
        shippingInternationalCost = 0.00d;
        totalShippingInternationalCost = 0.00d;
        shippingVietnamCost = 0.00d;
        totalShippingVietnamCost = 0.00d;
        shippingInternalCost = 0.00d;
        totalShippingInternalCost = 0.00d;
        additionalCost = 0.00d;
        totalAdditionalCost = 0.00d;
        totalCost = 0.00d;
        totalCostVN = 0.00d;
      } else if (!OrderUtils.isAdmin(username)) {
        setting = settingService.findSettingById(OrderConstants.EMAIL_CHECK_LINK_NOTIFICATION_SUBJECT_KEY);
        String subject = setting != null ? setting.getSettingvalue()
                                              : OrderConstants.EMAIL_CHECK_LINK_NOTIFICATION_SUBJECT_VALUE;
        setting = settingService.findSettingById(OrderConstants.EMAIL_CHECK_LINK_NOTIFICATION_CONTENT_KEY);
        String content = setting != null ? setting.getSettingvalue()
                                              : OrderConstants.EMAIL_CHECK_LINK_NOTIFICATION_CONTENT_VALUE;
        content = content.replaceAll(OrderConstants.PLACE_HOLDER_PRODUCT_LINK, itemLink);
        OrderUtils.sendAdminMailNotification(subject, content);
      }

      //
      return SUCCESS;
    } catch (Exception e) {
      // Send mail notification.
      errorMessage = getText("dathang24h.order-placed.message.error");
      OrderUtils.sendExceptionMailNotification(e);
      return INPUT;
    }
  }

  private boolean isReadyForSubmit() {
    //
    if (StringUtil.isEmpty(fullname) || StringUtil.isEmpty(phone) || StringUtil.isEmpty(email)
        || StringUtil.isEmpty(address) || StringUtil.isEmpty(itemLink) || StringUtil.isEmpty(itemQuantity)) {
      return false;
    }

    //
    Pattern pattern = Pattern.compile(OrderUtils.PHONE_REGEX);
    Matcher matcher = pattern.matcher(phone);
    if (!matcher.matches()) {
      errorMessage = getText("dathang24h.order-placed.message.phone-error");
      return false;
    }

    //
    pattern = Pattern.compile(OrderUtils.EMAIL_REGEX);
    matcher = pattern.matcher(email);
    if (!matcher.matches()) {
      errorMessage = getText("dathang24h.order-placed.message.email-error");
      return false;
    }

    //
    try {
      Integer.parseInt(itemQuantity);
    } catch (Exception e) {
      itemQuantity = "1";
    }

    //
    return true;
  }

  private String calculateAdditionalRate(double originalCost) throws Exception {
    Setting setting = null;
    String additionalRate = null;
    if (originalCost < 100.00) {
      setting = settingService.findSettingById(OrderConstants.PRODUCT_DEFAULT_ADDITIONAL_RATE_3_KEY);
      additionalRate = setting != null ? setting.getSettingvalue() : OrderConstants.PRODUCT_DEFAULT_ADDITIONAL_RATE_3_VALUE;
    } else if (100.00 <= originalCost && originalCost < 250.00) {
      setting = settingService.findSettingById(OrderConstants.PRODUCT_DEFAULT_ADDITIONAL_RATE_2_KEY);
      additionalRate = setting != null ? setting.getSettingvalue() : OrderConstants.PRODUCT_DEFAULT_ADDITIONAL_RATE_2_VALUE;
    } else {
      setting = settingService.findSettingById(OrderConstants.PRODUCT_DEFAULT_ADDITIONAL_RATE_1_KEY);
      additionalRate = setting != null ? setting.getSettingvalue() : OrderConstants.PRODUCT_DEFAULT_ADDITIONAL_RATE_1_VALUE;
    }
    return additionalRate;
  }

}
