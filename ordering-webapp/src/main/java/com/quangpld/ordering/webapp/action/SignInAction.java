package com.quangpld.ordering.webapp.action;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import com.opensymphony.xwork2.ActionSupport;
import com.quangpld.core.common.util.StringUtil;
import com.quangpld.ordering.webapp.util.OrderUtils;

public class SignInAction extends ActionSupport {

  /** serialVersionUID. */
  private static final long serialVersionUID = 9012658638868637380L;

  private String username;
  private String password;
  private String errorMessage;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  @Override
  public String execute() throws Exception {
    try {
      //
      if (!isReadyForSubmit()) {
        return INPUT;
      }

      //
      UsernamePasswordToken token = new UsernamePasswordToken(username, password);
      Subject currentUser = SecurityUtils.getSubject();
      currentUser.login(token);
      return SUCCESS;
    } catch (Exception e) {
      // Send mail notification
      errorMessage = getText("dathang24h.signin.message.invalid-username-password");
      OrderUtils.sendExceptionMailNotification(e);
      return INPUT;
    }
  }

  private boolean isReadyForSubmit() {
    if (StringUtil.isNotEmpty(username) && StringUtil.isNotEmpty(password)) {
      return true;
    }
    return false;
  }

}
