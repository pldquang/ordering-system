/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2013-01-27
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-01-27    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.ordering.webapp.action;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
public class DefaultAction extends ActionSupport {

  /** serialVersionUID. */
  private static final long serialVersionUID = 3583614146568955502L;

  @Override
  public String execute() throws Exception {
    return SUCCESS;
  }

}
