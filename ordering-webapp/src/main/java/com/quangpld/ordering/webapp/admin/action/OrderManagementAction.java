package com.quangpld.ordering.webapp.admin.action;

import java.util.List;

import org.springframework.context.ApplicationContext;

import com.opensymphony.xwork2.ActionSupport;
import com.quangpld.ordering.system.core.entity.Order;
import com.quangpld.ordering.system.core.service.OrderService;
import com.quangpld.ordering.webapp.util.OrderUtils;

public class OrderManagementAction extends ActionSupport {

  /** serialVersionUID.*/
  private static final long serialVersionUID = -4620299422583522508L;

  /** UserService object. */
  private OrderService orderService = null;

  /** Order lists. */
  private List<Order> orders = null;

  public OrderManagementAction() throws Exception {
    ApplicationContext context = OrderUtils.getClassPathXmlApplicationContext();
    orderService = context.getBean("orderService", OrderService.class);
    orders = orderService.findOrderByExample(new Order());
  }

  public List<Order> getOrders() {
    return orders;
  }

  public void setOrders(List<Order> orders) {
    this.orders = orders;
  }

  @Override
  public String execute() throws Exception {
    return SUCCESS;
  }

}
