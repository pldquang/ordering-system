package com.quangpld.ordering.webapp.environment.initialization;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.context.ApplicationContext;

import com.quangpld.core.setting.management.entity.Setting;
import com.quangpld.core.setting.management.service.SettingService;
import com.quangpld.ordering.system.core.common.OrderConstants;
import com.quangpld.ordering.webapp.util.OrderUtils;

public class SettingInitializationListener implements ServletContextListener {

  /** SettingService object. */
  private SettingService settingService = null;

  public SettingInitializationListener() throws Exception {
    ApplicationContext context = OrderUtils.getClassPathXmlApplicationContext();
    settingService = context.getBean("settingService", SettingService.class);
  }

  @Override
  public void contextInitialized(ServletContextEvent arg0) {
    try {
      /********************************************************************************************/
      /**                                                                                        **/
      /**                                 COMMON_CONFIGURATION                                   **/
      /**                                                                                        **/
      /********************************************************************************************/
      Setting setting = settingService.findSettingById(OrderConstants.EXCHANGE_RATE_USD_VND);
      if (setting == null) {
        setting = new Setting(OrderConstants.EXCHANGE_RATE_USD_VND, OrderConstants.EXCHANGE_RATE_USD_VND_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      /********************************************************************************************/
      /**                                                                                        **/
      /**                                  MAIL_CONFIGURATION                                    **/
      /**                                                                                        **/
      /********************************************************************************************/
      setting = settingService.findSettingById(OrderConstants.EMAIL_DEFAULT_GMAIL_USERNAME_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.EMAIL_DEFAULT_GMAIL_USERNAME_KEY, OrderConstants.EMAIL_DEFAULT_GMAIL_USERNAME_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.EMAIL_DEFAULT_GMAIL_PASSWORD_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.EMAIL_DEFAULT_GMAIL_PASSWORD_KEY, OrderConstants.EMAIL_DEFAULT_GMAIL_PASSWORD_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.EMAIL_REGISTER_SUCCESSFUL_NOTIFICATION_SUBJECT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.EMAIL_REGISTER_SUCCESSFUL_NOTIFICATION_SUBJECT_KEY, OrderConstants.EMAIL_REGISTER_SUCCESSFUL_NOTIFICATION_SUBJECT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.EMAIL_REGISTER_SUCCESSFUL_NOTIFICATION_CONTENT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.EMAIL_REGISTER_SUCCESSFUL_NOTIFICATION_CONTENT_KEY, OrderConstants.EMAIL_REGISTER_SUCCESSFUL_NOTIFICATION_CONTENT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.EMAIL_ORDER_PLACED_ADMIN_NOTIFICATION_SUBJECT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.EMAIL_ORDER_PLACED_ADMIN_NOTIFICATION_SUBJECT_KEY, OrderConstants.EMAIL_ORDER_PLACED_ADMIN_NOTIFICATION_SUBJECT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.EMAIL_ORDER_PLACED_ADMIN_NOTIFICATION_CONTENT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.EMAIL_ORDER_PLACED_ADMIN_NOTIFICATION_CONTENT_KEY, OrderConstants.EMAIL_ORDER_PLACED_ADMIN_NOTIFICATION_CONTENT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.EMAIL_ORDER_PLACED_USER_NOTIFICATION_SUBJECT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.EMAIL_ORDER_PLACED_USER_NOTIFICATION_SUBJECT_KEY, OrderConstants.EMAIL_ORDER_PLACED_USER_NOTIFICATION_SUBJECT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.EMAIL_ORDER_PLACED_USER_NOTIFICATION_CONTENT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.EMAIL_ORDER_PLACED_USER_NOTIFICATION_CONTENT_KEY, OrderConstants.EMAIL_ORDER_PLACED_USER_NOTIFICATION_CONTENT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.EMAIL_CHECK_LINK_NOTIFICATION_SUBJECT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.EMAIL_CHECK_LINK_NOTIFICATION_SUBJECT_KEY, OrderConstants.EMAIL_CHECK_LINK_NOTIFICATION_SUBJECT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.EMAIL_CHECK_LINK_NOTIFICATION_CONTENT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.EMAIL_CHECK_LINK_NOTIFICATION_CONTENT_KEY, OrderConstants.EMAIL_CHECK_LINK_NOTIFICATION_CONTENT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.EMAIL_CHECK_LINK_FAIL_NOTIFICATION_SUBJECT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.EMAIL_CHECK_LINK_FAIL_NOTIFICATION_SUBJECT_KEY, OrderConstants.EMAIL_CHECK_LINK_FAIL_NOTIFICATION_SUBJECT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.EMAIL_CHECK_LINK_FAIL_NOTIFICATION_CONTENT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.EMAIL_CHECK_LINK_FAIL_NOTIFICATION_CONTENT_KEY, OrderConstants.EMAIL_CHECK_LINK_FAIL_NOTIFICATION_CONTENT_VALUE);
        settingService.saveSetting(setting);
      }

      /********************************************************************************************/
      /**                                                                                        **/
      /**                                 AMAZON_CONFIGURATION                                   **/
      /**                                                                                        **/
      /********************************************************************************************/
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_TITLE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_TITLE_NODE, OrderConstants.AMAZON_PRODUCT_TITLE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      setting = settingService.findSettingById(OrderConstants.AMAZON_MOBILE_PRODUCT_TITLE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_MOBILE_PRODUCT_TITLE_NODE, OrderConstants.AMAZON_MOBILE_PRODUCT_TITLE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_PRICE_NODE_1);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_PRICE_NODE_1, OrderConstants.AMAZON_PRODUCT_PRICE_NODE_1_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_FULFILLED_BY_AMAZON_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_FULFILLED_BY_AMAZON_PRICE_NODE, OrderConstants.AMAZON_PRODUCT_FULFILLED_BY_AMAZON_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_SOLD_BY_AMAZON_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_SOLD_BY_AMAZON_PRICE_NODE, OrderConstants.AMAZON_PRODUCT_SOLD_BY_AMAZON_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_MOBILE_PRODUCT_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_MOBILE_PRODUCT_PRICE_NODE, OrderConstants.AMAZON_MOBILE_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_WEIGHT_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_WEIGHT_NODE, OrderConstants.AMAZON_PRODUCT_WEIGHT_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.PRODUCT_DEFAULT_WEIGHT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.PRODUCT_DEFAULT_WEIGHT_KEY, OrderConstants.PRODUCT_DEFAULT_WEIGHT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.CATEGORIES_TO_BE_DISPLAYED_IN_INDEX_PAGE_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.CATEGORIES_TO_BE_DISPLAYED_IN_INDEX_PAGE_KEY, OrderConstants.CATEGORIES_TO_BE_DISPLAYED_IN_INDEX_PAGE_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.PRODUCT_DEFAULT_IMPORT_TAX_RATE_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.PRODUCT_DEFAULT_IMPORT_TAX_RATE_KEY, OrderConstants.PRODUCT_DEFAULT_IMPORT_TAX_RATE_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.RSS_FEED_1_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.RSS_FEED_1_KEY, OrderConstants.RSS_FEED_1_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.RSS_FEED_2_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.RSS_FEED_2_KEY, OrderConstants.RSS_FEED_2_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.RSS_FEED_3_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.RSS_FEED_3_KEY, OrderConstants.RSS_FEED_3_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.RSS_FEED_4_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.RSS_FEED_4_KEY, OrderConstants.RSS_FEED_4_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.RSS_FEED_5_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.RSS_FEED_5_KEY, OrderConstants.RSS_FEED_5_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.RSS_FEED_6_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.RSS_FEED_6_KEY, OrderConstants.RSS_FEED_6_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.PRODUCT_DEFAULT_ADDITIONAL_RATE_1_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.PRODUCT_DEFAULT_ADDITIONAL_RATE_1_KEY, OrderConstants.PRODUCT_DEFAULT_ADDITIONAL_RATE_1_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.PRODUCT_DEFAULT_ADDITIONAL_RATE_2_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.PRODUCT_DEFAULT_ADDITIONAL_RATE_2_KEY, OrderConstants.PRODUCT_DEFAULT_ADDITIONAL_RATE_2_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.PRODUCT_DEFAULT_ADDITIONAL_RATE_3_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.PRODUCT_DEFAULT_ADDITIONAL_RATE_3_KEY, OrderConstants.PRODUCT_DEFAULT_ADDITIONAL_RATE_3_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_DEFAULT_TAX_RATE);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_DEFAULT_TAX_RATE, OrderConstants.AMAZON_PRODUCT_DEFAULT_TAX_RATE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.PRODUCT_MIN_SHIPPING_VN_COST_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.PRODUCT_MIN_SHIPPING_VN_COST_KEY, OrderConstants.PRODUCT_MIN_SHIPPING_VN_COST_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.PRODUCT_MIN_ADDITIONAL_COST_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.PRODUCT_MIN_ADDITIONAL_COST_KEY, OrderConstants.PRODUCT_MIN_ADDITIONAL_COST_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.PRODUCT_DEFAULT_SHIPPING_COST_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.PRODUCT_DEFAULT_SHIPPING_COST_KEY, OrderConstants.PRODUCT_DEFAULT_SHIPPING_COST_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_DEFAULT_WATCH_WEIGHT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_DEFAULT_WATCH_WEIGHT_KEY, OrderConstants.AMAZON_DEFAULT_WATCH_WEIGHT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_DEFAULT_SHOE_WEIGHT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_DEFAULT_SHOE_WEIGHT_KEY, OrderConstants.AMAZON_DEFAULT_SHOE_WEIGHT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOE_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOE_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOE_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCH_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCH_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCH_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_IMPORT_TAX_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_IMPORT_TAX_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_IMPORT_TAX_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_IMPORT_TAX_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_IMPORT_TAX_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_IMPORT_TAX_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_IMPORT_TAX_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_IMPORT_TAX_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_IMPORT_TAX_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_IMPORT_TAX_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_IMPORT_TAX_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_IMPORT_TAX_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_IMPORT_TAX_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_IMPORT_TAX_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_IMPORT_TAX_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_IMPORT_TAX_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_IMPORT_TAX_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_IMPORT_TAX_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_IMPORT_TAX_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_IMPORT_TAX_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_IMPORT_TAX_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_IMPORT_TAX_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_IMPORT_TAX_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_IMPORT_TAX_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_IMPORT_TAX_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_IMPORT_TAX_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_IMPORT_TAX_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_IMPORT_TAX_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_IMPORT_TAX_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_IMPORT_TAX_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_IMPORT_TAX_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_IMPORT_TAX_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_IMPORT_TAX_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_IMPORT_TAX_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_IMPORT_TAX_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_IMPORT_TAX_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_DEFAULT_WEIGHT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_DEFAULT_WEIGHT_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_DEFAULT_WEIGHT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_DEFAULT_WEIGHT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_DEFAULT_WEIGHT_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_DEFAULT_WEIGHT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_DEFAULT_WEIGHT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_DEFAULT_WEIGHT_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_DEFAULT_WEIGHT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_DEFAULT_WEIGHT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_DEFAULT_WEIGHT_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_DEFAULT_WEIGHT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_DEFAULT_WEIGHT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_DEFAULT_WEIGHT_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_DEFAULT_WEIGHT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_DEFAULT_WEIGHT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_DEFAULT_WEIGHT_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_DEFAULT_WEIGHT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_DEFAULT_WEIGHT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_DEFAULT_WEIGHT_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_DEFAULT_WEIGHT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_DEFAULT_WEIGHT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_DEFAULT_WEIGHT_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_DEFAULT_WEIGHT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_DEFAULT_WEIGHT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_DEFAULT_WEIGHT_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_DEFAULT_WEIGHT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_DEFAULT_WEIGHT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_DEFAULT_WEIGHT_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_DEFAULT_WEIGHT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_DEFAULT_WEIGHT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_DEFAULT_WEIGHT_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_DEFAULT_WEIGHT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_DEFAULT_WEIGHT_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_DEFAULT_WEIGHT_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_DEFAULT_WEIGHT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_SHIPPING_COST_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_SHIPPING_COST_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_CAMERA_PHOTO_SHIPPING_COST_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_SHIPPING_COST_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_SHIPPING_COST_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_CELLPHONES_ACCESSORIES_SHIPPING_COST_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_SHIPPING_COST_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_SHIPPING_COST_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_VIDEO_GAMES_SHIPPING_COST_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_SHIPPING_COST_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_SHIPPING_COST_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_MP3_PLAYERS_ACCESSORIES_SHIPPING_COST_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_SHIPPING_COST_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_SHIPPING_COST_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_COMPUTERS_ACCESSORIES_SHIPPING_COST_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_SHIPPING_COST_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_SHIPPING_COST_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_ELECTRONICS_SHIPPING_COST_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_SHIPPING_COST_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_SHIPPING_COST_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_CLOTHING_SHIPPING_COST_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_SHIPPING_COST_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_SHIPPING_COST_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_SHOES_SHIPPING_COST_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_SHIPPING_COST_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_SHIPPING_COST_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_JEWELRY_SHIPPING_COST_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_SHIPPING_COST_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_SHIPPING_COST_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_WATCHES_SHIPPING_COST_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_SHIPPING_COST_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_SHIPPING_COST_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_SPORTS_OUTDOORS_SHIPPING_COST_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_SHIPPING_COST_KEY);
      if (setting == null) {
        setting = new Setting(OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_SHIPPING_COST_KEY, OrderConstants.AMAZON_PRODUCT_CATEGORY_KINDLE_SHIPPING_COST_VALUE);
        settingService.saveSetting(setting);
      }

      /********************************************************************************************/
      /**                                                                                        **/
      /**                                   EBAY_CONFIGURATION                                   **/
      /**                                                                                        **/
      /********************************************************************************************/
      setting = settingService.findSettingById(OrderConstants.EBAY_PRODUCT_TITLE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.EBAY_PRODUCT_TITLE_NODE, OrderConstants.EBAY_PRODUCT_TITLE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.EBAY_PRODUCT_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.EBAY_PRODUCT_PRICE_NODE, OrderConstants.EBAY_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.WALMART_PRODUCT_TITLE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.WALMART_PRODUCT_TITLE_NODE, OrderConstants.WALMART_PRODUCT_TITLE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.WALMART_PRODUCT_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.WALMART_PRODUCT_PRICE_NODE, OrderConstants.WALMART_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.RAFFAELLO_NETWORK_PRODUCT_TITLE_1_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.RAFFAELLO_NETWORK_PRODUCT_TITLE_1_NODE, OrderConstants.RAFFAELLO_NETWORK_PRODUCT_TITLE_1_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.RAFFAELLO_NETWORK_PRODUCT_TITLE_2_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.RAFFAELLO_NETWORK_PRODUCT_TITLE_2_NODE, OrderConstants.RAFFAELLO_NETWORK_PRODUCT_TITLE_2_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.RAFFAELLO_NETWORK_PRODUCT_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.RAFFAELLO_NETWORK_PRODUCT_PRICE_NODE, OrderConstants.RAFFAELLO_NETWORK_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.RAFFAELLO_NETWORK_PRODUCT_PRICE_SPECIAL_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.RAFFAELLO_NETWORK_PRODUCT_PRICE_SPECIAL_NODE, OrderConstants.RAFFAELLO_NETWORK_PRODUCT_PRICE_SPECIAL_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.NORDSTROM_PRODUCT_TITLE_1_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.NORDSTROM_PRODUCT_TITLE_1_NODE, OrderConstants.NORDSTROM_PRODUCT_TITLE_1_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.NORDSTROM_PRODUCT_TITLE_2_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.NORDSTROM_PRODUCT_TITLE_2_NODE, OrderConstants.NORDSTROM_PRODUCT_TITLE_2_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.NORDSTROM_MOBILE_PRODUCT_TITLE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.NORDSTROM_MOBILE_PRODUCT_TITLE_NODE, OrderConstants.NORDSTROM_MOBILE_PRODUCT_TITLE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.NORDSTROM_PRODUCT_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.NORDSTROM_PRODUCT_PRICE_NODE, OrderConstants.NORDSTROM_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.NORDSTROM_PRODUCT_PRICE_SPECIAL_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.NORDSTROM_PRODUCT_PRICE_SPECIAL_NODE, OrderConstants.NORDSTROM_PRODUCT_PRICE_SPECIAL_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.NORDSTROM_MOBILE_PRODUCT_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.NORDSTROM_MOBILE_PRODUCT_PRICE_NODE, OrderConstants.NORDSTROM_MOBILE_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.FOREVER21_PRODUCT_TITLE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.FOREVER21_PRODUCT_TITLE_NODE, OrderConstants.FOREVER21_PRODUCT_TITLE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.FOREVER21_PRODUCT_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.FOREVER21_PRODUCT_PRICE_NODE, OrderConstants.FOREVER21_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.MACYS_PRODUCT_TITLE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.MACYS_PRODUCT_TITLE_NODE, OrderConstants.MACYS_PRODUCT_TITLE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.MACYS_MOBILE_PRODUCT_TITLE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.MACYS_MOBILE_PRODUCT_TITLE_NODE, OrderConstants.MACYS_MOBILE_PRODUCT_TITLE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.MACYS_PRODUCT_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.MACYS_PRODUCT_PRICE_NODE, OrderConstants.MACYS_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.MACYS_PRODUCT_PRICE_SALE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.MACYS_PRODUCT_PRICE_SALE_NODE, OrderConstants.MACYS_PRODUCT_PRICE_SALE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.MACYS_MOBILE_PRODUCT_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.MACYS_MOBILE_PRODUCT_PRICE_NODE, OrderConstants.MACYS_MOBILE_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.MACYS_MOBILE_PRODUCT_PRICE_SALE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.MACYS_MOBILE_PRODUCT_PRICE_SALE_NODE, OrderConstants.MACYS_MOBILE_PRODUCT_PRICE_SALE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.SEPHORA_PRODUCT_TITLE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.SEPHORA_PRODUCT_TITLE_NODE, OrderConstants.SEPHORA_PRODUCT_TITLE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.SEPHORA_MOBILE_PRODUCT_TITLE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.SEPHORA_MOBILE_PRODUCT_TITLE_NODE, OrderConstants.SEPHORA_MOBILE_PRODUCT_TITLE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.SEPHORA_PRODUCT_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.SEPHORA_PRODUCT_PRICE_NODE, OrderConstants.SEPHORA_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.SEPHORA_PRODUCT_PRICE_SALE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.SEPHORA_PRODUCT_PRICE_SALE_NODE, OrderConstants.SEPHORA_PRODUCT_PRICE_SALE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.SEPHORA_MOBILE_PRODUCT_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.SEPHORA_MOBILE_PRODUCT_PRICE_NODE, OrderConstants.SEPHORA_MOBILE_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.SEPHORA_MOBILE_PRODUCT_PRICE_SALE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.SEPHORA_MOBILE_PRODUCT_PRICE_SALE_NODE, OrderConstants.SEPHORA_MOBILE_PRODUCT_PRICE_SALE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.BATH_AND_BODY_WORKS_PRODUCT_TITLE_BRAND_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.BATH_AND_BODY_WORKS_PRODUCT_TITLE_BRAND_NODE, OrderConstants.BATH_AND_BODY_WORKS_PRODUCT_TITLE_BRAND_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.BATH_AND_BODY_WORKS_PRODUCT_TITLE_FN_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.BATH_AND_BODY_WORKS_PRODUCT_TITLE_FN_NODE, OrderConstants.BATH_AND_BODY_WORKS_PRODUCT_TITLE_FN_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.BATH_AND_BODY_WORKS_PRODUCT_TITLE_BRAND_COLOR_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.BATH_AND_BODY_WORKS_PRODUCT_TITLE_BRAND_COLOR_NODE, OrderConstants.BATH_AND_BODY_WORKS_PRODUCT_TITLE_BRAND_COLOR_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_BRAND_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_BRAND_NODE, OrderConstants.BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_BRAND_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_FN_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_FN_NODE, OrderConstants.BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_FN_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_BRAND_COLOR_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_BRAND_COLOR_NODE, OrderConstants.BATH_AND_BODY_WORKS_MOBILE_PRODUCT_TITLE_BRAND_COLOR_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.BATH_AND_BODY_WORKS_PRODUCT_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.BATH_AND_BODY_WORKS_PRODUCT_PRICE_NODE, OrderConstants.BATH_AND_BODY_WORKS_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.BATH_AND_BODY_WORKS_PRODUCT_PRICE_DISCOUNT_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.BATH_AND_BODY_WORKS_PRODUCT_PRICE_DISCOUNT_NODE, OrderConstants.BATH_AND_BODY_WORKS_PRODUCT_PRICE_DISCOUNT_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.BATH_AND_BODY_WORKS_MOBILE_PRODUCT_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.BATH_AND_BODY_WORKS_MOBILE_PRODUCT_PRICE_NODE, OrderConstants.BATH_AND_BODY_WORKS_MOBILE_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.BATH_AND_BODY_WORKS_MOBILE_PRODUCT_PRICE_DISCOUNT_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.BATH_AND_BODY_WORKS_MOBILE_PRODUCT_PRICE_DISCOUNT_NODE, OrderConstants.BATH_AND_BODY_WORKS_MOBILE_PRODUCT_PRICE_DISCOUNT_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.HEAD_DIRECT_PRODUCT_TITLE_BRAND_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.HEAD_DIRECT_PRODUCT_TITLE_BRAND_NODE, OrderConstants.HEAD_DIRECT_PRODUCT_TITLE_BRAND_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.HEAD_DIRECT_PRODUCT_TITLE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.HEAD_DIRECT_PRODUCT_TITLE_NODE, OrderConstants.HEAD_DIRECT_PRODUCT_TITLE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.HEAD_DIRECT_PRODUCT_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.HEAD_DIRECT_PRODUCT_PRICE_NODE, OrderConstants.HEAD_DIRECT_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.HEAD_DIRECT_PRODUCT_PRICE_SALE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.HEAD_DIRECT_PRODUCT_PRICE_SALE_NODE, OrderConstants.HEAD_DIRECT_PRODUCT_PRICE_SALE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.WOO_AUDIO_PRODUCT_TITLE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.WOO_AUDIO_PRODUCT_TITLE_NODE, OrderConstants.WOO_AUDIO_PRODUCT_TITLE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.WOO_AUDIO_PRODUCT_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.WOO_AUDIO_PRODUCT_PRICE_NODE, OrderConstants.WOO_AUDIO_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.MOON_AUDIO_PRODUCT_TITLE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.MOON_AUDIO_PRODUCT_TITLE_NODE, OrderConstants.MOON_AUDIO_PRODUCT_TITLE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.MOON_AUDIO_PRODUCT_PRICE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.MOON_AUDIO_PRODUCT_PRICE_NODE, OrderConstants.MOON_AUDIO_PRODUCT_PRICE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }

      //
      setting = settingService.findSettingById(OrderConstants.MOON_AUDIO_PRODUCT_PRICE_SALE_NODE);
      if (setting == null) {
        setting = new Setting(OrderConstants.MOON_AUDIO_PRODUCT_PRICE_SALE_NODE, OrderConstants.MOON_AUDIO_PRODUCT_PRICE_SALE_NODE_DEFAULT_VALUE);
        settingService.saveSetting(setting);
      }
    } catch (Exception e) {
      // TODO:
    }
  }

  @Override
  public void contextDestroyed(ServletContextEvent arg0) {
    // TODO Auto-generated method stub
    
  }

}
