/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    |               |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.ordering.webapp.action;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.context.ApplicationContext;

import com.opensymphony.xwork2.ActionSupport;
import com.quangpld.core.common.mail.GmailTLSSender;
import com.quangpld.core.common.mail.MailSender;
import com.quangpld.core.common.util.StringUtil;
import com.quangpld.core.setting.management.entity.Setting;
import com.quangpld.core.setting.management.service.SettingService;
import com.quangpld.ordering.system.core.common.OrderConstants;
import com.quangpld.ordering.system.core.entity.Order;
import com.quangpld.ordering.system.core.entity.OrderDetail;
import com.quangpld.ordering.system.core.entity.OrderDetailBill;
import com.quangpld.ordering.system.core.service.OrderService;
import com.quangpld.ordering.system.core.service.OrderService.Status;
import com.quangpld.ordering.webapp.util.OrderUtils;

/**
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
public class OrderConfirmedAction extends ActionSupport {

  /** serialVersionUID. */
  private static final long serialVersionUID = 1363642927204852736L;

  private static final String SUCCESS_ANONYMOUS_RESULT = "successAnonymous";
  
  /** OrderService object. */
  private OrderService orderService = null;

  /** SettingService object. */
  private SettingService settingService = null;

  private List<Order> orders = null;

  private String prettyOrderId;
  private String fullname;
  private String phone;
  private String email;
  private String address;
  private String itemName;
  private String itemLink;
  private String itemColor;
  private String itemSize;
  private String itemQuantity;
  private String itemUnit;
  private String itemNote;

  private String currency;
  private double exchangeRate;
  private double originalCost;
  private double tax;
  private double shippingInternationalCost;
  private double shippingVietnamCost;
  private double shippingInternalCost;
  private double additionalCost;

  private String errorMessage;
  private DecimalFormat decimalFormat;

  /**
   * Default constructor.
   * @throws Exception 
   */
  public OrderConfirmedAction() throws Exception {
    try {
      //
      ApplicationContext context = OrderUtils.getClassPathXmlApplicationContext();
      orderService = (OrderService) context.getBean("orderService", OrderService.class);
      settingService = context.getBean("settingService", SettingService.class);

      //
      DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
      decimalFormatSymbols.setDecimalSeparator('.');
      decimalFormatSymbols.setGroupingSeparator(',');
      decimalFormat = new DecimalFormat("#,##0.00", decimalFormatSymbols);
    } catch (Exception e) {
      OrderUtils.sendExceptionMailNotification(e);
    }
  }

  public List<Order> getOrders() {
    return orders;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public void setPrettyOrderId(String prettyOrderId) {
    this.prettyOrderId = prettyOrderId;
  }

  public String getFullname() {
    return fullname;
  }

  public void setFullname(String fullname) {
    this.fullname = fullname;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getItemName() {
    return itemName;
  }

  public void setItemName(String itemName) {
    this.itemName = itemName;
  }

  public String getItemLink() {
    return itemLink;
  }

  public void setItemLink(String itemLink) {
    this.itemLink = itemLink;
  }

  public String getItemColor() {
    return itemColor;
  }

  public void setItemColor(String itemColor) {
    this.itemColor = itemColor;
  }

  public String getItemSize() {
    return itemSize;
  }

  public void setItemSize(String itemSize) {
    this.itemSize = itemSize;
  }

  public String getItemQuantity() {
    return itemQuantity;
  }

  public void setItemQuantity(String itemQuantity) {
    this.itemQuantity = itemQuantity;
  }

  public String getItemUnit() {
    return itemUnit;
  }

  public void setItemUnit(String itemUnit) {
    this.itemUnit = itemUnit;
  }

  public String getItemNote() {
    return itemNote;
  }

  public void setItemNote(String itemNote) {
    this.itemNote = itemNote;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public void setExchangeRate(double exchangeRate) {
    this.exchangeRate = exchangeRate;
  }

  public void setOriginalCost(double originalCost) {
    this.originalCost = originalCost;
  }

  public void setTax(double tax) {
    this.tax = tax;
  }

  public void setShippingInternationalCost(double shippingInternationalCost) {
    this.shippingInternationalCost = shippingInternationalCost;
  }

  public void setShippingVietnamCost(double shippingVietnamCost) {
    this.shippingVietnamCost = shippingVietnamCost;
  }

  public void setShippingInternalCost(double shippingInternalCost) {
    this.shippingInternalCost = shippingInternalCost;
  }

  public void setAdditionalCost(double additionalCost) {
    this.additionalCost = additionalCost;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  @Override
  public String execute() throws Exception {
    try {
      //
      if (StringUtil.isEmpty(fullname) || StringUtil.isEmpty(phone) || StringUtil.isEmpty(email)
          || StringUtil.isEmpty(address)) {
        return SUCCESS_ANONYMOUS_RESULT;
      }

      //
      Order order = new Order(fullname, phone, email, address, Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok")).getTime(), Status.NEW.toString());
      Subject currentUser = SecurityUtils.getSubject();
      if (currentUser.getPrincipal() != null) order.setUsername(currentUser.getPrincipal().toString());
      orderService.saveOrder(order);

      //
      OrderDetail orderDetail = new OrderDetail(order.getOrderid(), itemName != null ? itemName : "", itemLink,
                                                itemQuantity, itemUnit != null ? itemUnit : "");
      if (StringUtil.isNotEmpty(itemColor)) orderDetail.setProductcolor(itemColor);
      if (StringUtil.isNotEmpty(itemSize)) orderDetail.setProductsize(itemSize);
      if (StringUtil.isNotEmpty(itemNote)) orderDetail.setComment(itemNote);
      orderService.addOrderDetail(orderDetail);

      //
      OrderDetailBill orderDetailBill = new OrderDetailBill(orderDetail.getOrderdetailid(), originalCost, tax, shippingInternationalCost,
                                                            shippingVietnamCost, shippingInternalCost, additionalCost,
                                                            currency != null ? currency : OrderConstants.CURRENCY_SYMBOL_USD, exchangeRate);
      orderService.addOrderDetailBill(orderDetailBill);

      //
      prettyOrderId = order.getPrettyOrderId();

      try {
        double quantity = Double.parseDouble(itemQuantity);
        double totalOriginalCost = originalCost * quantity;
        double totalTax = tax * quantity;
        double totalShippingInternationalCost = shippingInternationalCost * quantity;
        double totalShippingVietnamCost = shippingVietnamCost * quantity;
        double totalShippingInternalCost = shippingInternalCost * quantity;
        double totalAdditionalCost = additionalCost * quantity;
        double totalCost = totalOriginalCost + totalTax + totalShippingInternationalCost
                           + totalShippingVietnamCost + totalShippingInternalCost + totalAdditionalCost;
        double totalCostVN = totalCost * exchangeRate;
        
        // Send admin e-mail notification
        Setting setting = settingService.findSettingById(OrderConstants.EMAIL_ORDER_PLACED_ADMIN_NOTIFICATION_SUBJECT_KEY);
        String subject = setting != null ? setting.getSettingvalue()
                                         : OrderConstants.EMAIL_ORDER_PLACED_ADMIN_NOTIFICATION_SUBJECT_VALUE;
        subject = subject.replaceAll(OrderConstants.PLACE_HOLDER_PRETTY_ORDER_ID, prettyOrderId);
        setting = settingService.findSettingById(OrderConstants.EMAIL_ORDER_PLACED_ADMIN_NOTIFICATION_CONTENT_KEY);
        String content = setting != null ? setting.getSettingvalue()
                                         : OrderConstants.EMAIL_ORDER_PLACED_ADMIN_NOTIFICATION_CONTENT_VALUE;
        content = content.replaceAll(OrderConstants.PLACE_HOLDER_PRETTY_ORDER_ID, prettyOrderId);
        content = content.replaceAll(OrderConstants.PLACE_HOLDER_FULLNAME, fullname);
        content = content.replaceAll(OrderConstants.PLACE_HOLDER_ADDRESS, address);
        content = content.replaceAll(OrderConstants.PLACE_HOLDER_EMAIL, email);
        content = content.replaceAll(OrderConstants.PLACE_HOLDER_PHONE, phone);
        content = content.replaceAll(OrderConstants.PLACE_HOLDER_PRODUCT_TITLE, itemName);
        content = content.replaceAll(OrderConstants.PLACE_HOLDER_PRODUCT_LINK, itemLink);
        content = content.replaceAll(OrderConstants.PLACE_HOLDER_PRODUCT_COLOR, itemColor);
        content = content.replaceAll(OrderConstants.PLACE_HOLDER_PRODUCT_SIZE, itemSize);
        content = content.replaceAll(OrderConstants.PLACE_HOLDER_PRODUCT_QUANTITY, itemQuantity);
        content = content.replaceAll(OrderConstants.PLACE_HOLDER_ORIGINAL_COST, decimalFormat.format(originalCost));
        content = content.replaceAll(OrderConstants.PLACE_HOLDER_CURRENCY_SYMBOL, currency.equals(OrderConstants.CURRENCY_SYMBOL_USD) ? "\\" + currency : currency);

        //
        OrderUtils.sendAdminMailNotification(subject, content);
        
        // Send user e-mail notification
        setting = settingService.findSettingById(OrderConstants.EMAIL_DEFAULT_GMAIL_USERNAME_KEY);
        String emailUsername = setting != null ? setting.getSettingvalue()
                                               : OrderConstants.EMAIL_DEFAULT_GMAIL_USERNAME_VALUE;
        setting = settingService.findSettingById(OrderConstants.EMAIL_DEFAULT_GMAIL_PASSWORD_KEY);
        String emailPassword = setting != null ? setting.getSettingvalue()
                                               : OrderConstants.EMAIL_DEFAULT_GMAIL_PASSWORD_VALUE;

        //
        setting = settingService.findSettingById(OrderConstants.EMAIL_ORDER_PLACED_USER_NOTIFICATION_SUBJECT_KEY);
        String emailSubject = setting != null ? setting.getSettingvalue()
                                              : OrderConstants.EMAIL_ORDER_PLACED_USER_NOTIFICATION_SUBJECT_VALUE;
        emailSubject = emailSubject.replaceAll(OrderConstants.PLACE_HOLDER_PRETTY_ORDER_ID, prettyOrderId);

        //
        setting = settingService.findSettingById(OrderConstants.EMAIL_ORDER_PLACED_USER_NOTIFICATION_CONTENT_KEY);
        String emailContent = setting != null ? setting.getSettingvalue()
                                              : OrderConstants.EMAIL_ORDER_PLACED_USER_NOTIFICATION_CONTENT_VALUE;
        emailContent = emailContent.replaceAll(OrderConstants.PLACE_HOLDER_PRETTY_ORDER_ID, prettyOrderId);
        emailContent = emailContent.replaceAll(OrderConstants.PLACE_HOLDER_PRODUCT_TITLE, itemName);
        emailContent = emailContent.replaceAll(OrderConstants.PLACE_HOLDER_PRODUCT_LINK, itemLink);
        emailContent = emailContent.replaceAll(OrderConstants.PLACE_HOLDER_PRODUCT_COLOR, itemColor);
        emailContent = emailContent.replaceAll(OrderConstants.PLACE_HOLDER_PRODUCT_SIZE, itemSize);
        emailContent = emailContent.replaceAll(OrderConstants.PLACE_HOLDER_PRODUCT_QUANTITY, itemQuantity);
        emailContent = emailContent.replaceAll(OrderConstants.PLACE_HOLDER_ORIGINAL_COST, decimalFormat.format(originalCost));
        emailContent = emailContent.replaceAll(OrderConstants.PLACE_HOLDER_TOTAL_TAX, decimalFormat.format(totalTax));
        emailContent = emailContent.replaceAll(OrderConstants.PLACE_HOLDER_TOTAL_SHIPPING_INTERNATIONAL_COST, decimalFormat.format(totalShippingInternationalCost));
        emailContent = emailContent.replaceAll(OrderConstants.PLACE_HOLDER_TOTAL_SHIPPING_VIETNAM_COST, decimalFormat.format(totalShippingVietnamCost));
        emailContent = emailContent.replaceAll(OrderConstants.PLACE_HOLDER_TOTAL_SHIPPING_INTERNAL_COST, decimalFormat.format(totalShippingInternalCost));
        emailContent = emailContent.replaceAll(OrderConstants.PLACE_HOLDER_TOTAL_ADDITIONAL_COST, decimalFormat.format(totalAdditionalCost));
        emailContent = emailContent.replaceAll(OrderConstants.PLACE_HOLDER_TOTAL_COST, decimalFormat.format(totalCost));
        emailContent = emailContent.replaceAll(OrderConstants.PLACE_HOLDER_EXCHANGE_RATE, decimalFormat.format(exchangeRate));
        emailContent = emailContent.replaceAll(OrderConstants.PLACE_HOLDER_TOTAL_COST_VN, decimalFormat.format(totalCostVN));
        emailContent = emailContent.replaceAll(OrderConstants.PLACE_HOLDER_CURRENCY_SYMBOL, currency.equals(OrderConstants.CURRENCY_SYMBOL_USD) ? "\\" + currency : currency);

        //
        MailSender mailSender = new GmailTLSSender(emailUsername, emailPassword, emailUsername, email, emailSubject, emailContent);
        mailSender.sendMail();
      } catch (Exception e) {
        OrderUtils.sendExceptionMailNotification(e);
      }

      //
      if (currentUser.getPrincipal() != null) {
        Order orderFilter = new Order();
        orderFilter.setUsername(currentUser.getPrincipal().toString());
        orders = orderService.findOrderByExample(orderFilter);

        //
        return SUCCESS;
      } else {
        errorMessage = getText("dathang24h.order.msg.order-success");
        return SUCCESS_ANONYMOUS_RESULT;
      }
    } catch (Exception e) {
      // Send mail notification.
      errorMessage = getText("dathang24h.order-placed.message.error");
      OrderUtils.sendExceptionMailNotification(e);
      return INPUT;
    }
  }

}
