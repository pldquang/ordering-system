package com.quangpld.ordering.webapp.admin.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;

import com.opensymphony.xwork2.ActionSupport;
import com.quangpld.core.user.management.entity.User;
import com.quangpld.core.user.management.service.UserService;
import com.quangpld.ordering.webapp.util.OrderUtils;

public class UserManagementAction extends ActionSupport {

  /** serialVersionUID.*/
  private static final long serialVersionUID = 2922860557831009214L;

  /** UserService object. */
  private UserService userService = null;

  /** User lists. */
  private List<User> users = null;

  /** User roles. */
  private Map<String, String> userRoles = null;

  public UserManagementAction() throws Exception {
    ApplicationContext context = OrderUtils.getClassPathXmlApplicationContext();
    userService = context.getBean("userService", UserService.class);
    users = userService.findUserByExample(new User());
    userRoles = new HashMap<String, String>();
    for (User user : users) {
      userRoles.put(user.getUsername(), userService.getUserRoles(user.getUsername()).toString());
    }
  }

  public List<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }

  public Map<String, String> getUserRoles() {
    return userRoles;
  }

  public void setUserRoles(Map<String, String> userRoles) {
    this.userRoles = userRoles;
  }

  @Override
  public String execute() throws Exception {
    return SUCCESS;
  }

}
