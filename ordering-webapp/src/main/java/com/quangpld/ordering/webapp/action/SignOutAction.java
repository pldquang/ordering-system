package com.quangpld.ordering.webapp.action;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import com.opensymphony.xwork2.ActionSupport;
import com.quangpld.ordering.webapp.util.OrderUtils;

public class SignOutAction extends ActionSupport {

  /** serialVersionUID. */
  private static final long serialVersionUID = 1609102432973626449L;

  @Override
  public String execute() throws Exception {
    try {
      Subject currentUser = SecurityUtils.getSubject();
      currentUser.logout();
      return SUCCESS;
    } catch (Exception e) {
      // Send mail notification
      OrderUtils.sendExceptionMailNotification(e);
      return ERROR;
    }
  }

}
