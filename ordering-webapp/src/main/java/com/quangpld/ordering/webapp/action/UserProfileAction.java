package com.quangpld.ordering.webapp.action;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.shiro.SecurityUtils;
import org.springframework.context.ApplicationContext;

import com.opensymphony.xwork2.ActionSupport;
import com.quangpld.core.common.util.StringUtil;
import com.quangpld.core.user.management.entity.User;
import com.quangpld.core.user.management.service.UserService;
import com.quangpld.ordering.webapp.util.OrderUtils;

public class UserProfileAction extends ActionSupport {

  /** serialVersionUID. */
  private static final long serialVersionUID = -6874177826283754552L;

  /** UserService object. */
  private UserService userService = null;

  private User currentUser = null;

  private String username;
  private String fullname;
  private String address;
  private String phone;
  private String email;

  private String message;

  public UserProfileAction() throws Exception {
    try {
      //
      ApplicationContext context = OrderUtils.getClassPathXmlApplicationContext();
      userService = context.getBean("userService", UserService.class);

      //
      currentUser = userService.findUserById(SecurityUtils.getSubject().getPrincipal().toString());
      username = currentUser.getUsername();
      fullname = currentUser.getFirstname();
      phone = currentUser.getMobilephone();
      email = currentUser.getEmail();
      address = currentUser.getAddress1();
    } catch (Exception e) {
      OrderUtils.sendExceptionMailNotification(e);
    }
  }

  public User getCurrentUser() {
    return currentUser;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getFullname() {
    return fullname;
  }

  public void setFullname(String fullname) {
    this.fullname = fullname;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public String execute() throws Exception {
    try {
      if (isReadyForSubmit()) {
        //
        currentUser.setFirstname(fullname);
        currentUser.setAddress1(address);
        currentUser.setMobilephone(phone);
        currentUser.setEmail(email);
        currentUser.setLastupdatetime(Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok")).getTime());
        userService.updateUser(currentUser);

        //
        message = getText("dathang24h.user-profile.message.successful");
      }

      //
      return SUCCESS;
    } catch (Exception e) {
      // Send mail notification
      OrderUtils.sendExceptionMailNotification(e);
      return ERROR;
    }
  }

  private boolean isReadyForSubmit() throws Exception {
    //
    if (!currentUser.getFirstname().equals(fullname) || !currentUser.getAddress1().equals(address)
        || !currentUser.getMobilephone().equals(phone) || !currentUser.getEmail().equals(email)) {
      //
      if (StringUtil.isEmpty(fullname)) {
        message = getText("dathang24h.user-profile.message.invalid-fullname");
        return false;
      }

      //
      if (StringUtil.isEmpty(address)) {
        message = getText("dathang24h.user-profile.message.invalid-address");
        return false;
      }

      //
      Pattern pattern = Pattern.compile(OrderUtils.PHONE_REGEX);
      Matcher matcher = pattern.matcher(phone);
      if (!matcher.matches()) {
        message = getText("dathang24h.user-profile.message.invalid-phone");
        return false;
      }

      //
      pattern = Pattern.compile(OrderUtils.EMAIL_REGEX);
      matcher = pattern.matcher(email);
      if (!matcher.matches()) {
        message = getText("dathang24h.user-profile.message.invalid-email");
        return false;
      }

      //
      User example = new User();
      example.setEmail(email);
      List<User> users = userService.findUserByExample(example);
      if (users.size() > 0 && !users.get(0).getUsername().equals(username)) {
        message = getText("dathang24h.user-profile.message.email-existed");
        return false;
      }

      //
      return true;
    }

    //
    return false;
  }

}
