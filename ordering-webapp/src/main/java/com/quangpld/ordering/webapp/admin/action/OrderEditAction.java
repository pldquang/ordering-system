package com.quangpld.ordering.webapp.admin.action;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.context.ApplicationContext;

import com.opensymphony.xwork2.ActionSupport;
import com.quangpld.ordering.system.core.entity.Order;
import com.quangpld.ordering.system.core.entity.OrderDetail;
import com.quangpld.ordering.system.core.service.OrderService;
import com.quangpld.ordering.webapp.util.OrderUtils;

public class OrderEditAction extends ActionSupport {

  /** serialVersionUID.*/
  private static final long serialVersionUID = 896982463557037217L;

  /** OrderService object. */
  private OrderService orderService = null;

  private String orderId;
  private String prettyOrderId;
  private String username;
  private String fullname;
  private String phone;
  private String email;
  private String address;
  private Date createdDate;
  private Date lastUpdateTime;
  private String orderStatus;
  private String itemName;
  private String itemLink;
  private String itemColor;
  private String itemSize;
  private String itemQuantity;
  private String itemUnit;
  private String itemNote;

  private boolean isUpdate;
  private String message;

  public OrderEditAction() throws Exception {
    //
    ApplicationContext context = OrderUtils.getClassPathXmlApplicationContext();
    orderService = context.getBean("orderService", OrderService.class);
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public String getPrettyOrderId() {
    return prettyOrderId;
  }

  public String getUsername() {
    return username;
  }

  public String getFullname() {
    return fullname;
  }

  public void setFullname(String fullname) {
    this.fullname = fullname;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public Date getLastUpdateTime() {
    return lastUpdateTime;
  }

  public String getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(String orderStatus) {
    this.orderStatus = orderStatus;
  }

  public String getItemName() {
    return itemName;
  }

  public void setItemName(String itemName) {
    this.itemName = itemName;
  }

  public String getItemLink() {
    return itemLink;
  }

  public void setItemLink(String itemLink) {
    this.itemLink = itemLink;
  }

  public String getItemColor() {
    return itemColor;
  }

  public void setItemColor(String itemColor) {
    this.itemColor = itemColor;
  }

  public String getItemSize() {
    return itemSize;
  }

  public void setItemSize(String itemSize) {
    this.itemSize = itemSize;
  }

  public String getItemQuantity() {
    return itemQuantity;
  }

  public void setItemQuantity(String itemQuantity) {
    this.itemQuantity = itemQuantity;
  }

  public String getItemUnit() {
    return itemUnit;
  }

  public void setItemUnit(String itemUnit) {
    this.itemUnit = itemUnit;
  }

  public String getItemNote() {
    return itemNote;
  }

  public void setItemNote(String itemNote) {
    this.itemNote = itemNote;
  }

  public boolean getIsUpdate() {
    return isUpdate;
  }

  public void setIsUpdate(boolean isUpdate) {
    this.isUpdate = isUpdate;
  }

  public String getMessage() {
    return message;
  }

  @Override
  public String execute() throws Exception {
    //
    Order order = orderService.findOrderById(Integer.parseInt(orderId));
    OrderDetail example = new OrderDetail();
    example.setOrderid(Integer.parseInt(orderId));
    List<OrderDetail> orderDetails = orderService.getOrderDetailByExample(example);

//    //
//    if (!fullname.equals(order.getFullname()) || !phone.equals(order.getPhone()) || !email.equals(order.getEmail())
//        || !address.equals(order.getAddress()) || !orderStatus.equals(order.getOrderstatus())
//        || !itemName.equals(orderDetails.get(0).getProductname()) || !itemLink.equals(orderDetails.get(0).getProductlink())
//        || !itemColor.equals(orderDetails.get(0).getProductcolor()) || !itemSize.equals(orderDetails.get(0).getProductsize())
//        || !itemQuantity.equals(orderDetails.get(0).getProductquantity()) || !itemUnit.equals(orderDetails.get(0).getProductunit())
//        || !itemNote.equals(orderDetails.get(0).getComment())) {
//      //
//      order.setFullname(fullname);
//      order.setPhone(phone);
//      order.setEmail(email);
//      order.setAddress(address);
//      order.setOrderstatus(orderStatus);
//      orderService.updateOrder(order);
//
//      //
//      orderDetails.get(0).setProductname(itemName);
//      orderDetails.get(0).setProductlink(itemLink);
//      orderDetails.get(0).setProductcolor(itemColor);
//      orderDetails.get(0).setProductsize(itemColor);
//      orderDetails.get(0).setProductquantity(itemQuantity);
//      orderDetails.get(0).setProductunit(itemUnit);
//      orderDetails.get(0).setComment(itemNote);
//
//      //
//      message = "Đã lưu thay đổi";
//    } else {
//      prettyOrderId = order.getPrettyOrderId();
//      username = order.getUsername();
//      fullname = order.getFullname();
//      phone = order.getPhone();
//      email = order.getEmail();
//      address = order.getAddress();
//      createdDate = order.getCreateddate();
//      lastUpdateTime = order.getLastupdatetime();
//      orderStatus = order.getOrderstatus();
//      itemName = orderDetails.get(0).getProductname();
//      itemLink = orderDetails.get(0).getProductlink();
//      itemColor = orderDetails.get(0).getProductcolor();
//      itemSize = orderDetails.get(0).getProductsize();
//      itemQuantity = orderDetails.get(0).getProductquantity();
//      itemUnit = orderDetails.get(0).getProductunit();
//      itemNote = orderDetails.get(0).getComment();
//    }

    //
    if (!isUpdate) {
      prettyOrderId = order.getPrettyOrderId();
      username = order.getUsername();
      fullname = order.getFullname();
      phone = order.getPhone();
      email = order.getEmail();
      address = order.getAddress();
      createdDate = order.getCreateddate();
      lastUpdateTime = order.getLastupdatetime();
      orderStatus = order.getOrderstatus();
      itemName = orderDetails.get(0).getProductname();
      itemLink = orderDetails.get(0).getProductlink();
      itemColor = orderDetails.get(0).getProductcolor();
      itemSize = orderDetails.get(0).getProductsize();
      itemQuantity = orderDetails.get(0).getProductquantity();
      itemUnit = orderDetails.get(0).getProductunit();
      itemNote = orderDetails.get(0).getComment();
    } else {
      if (!fullname.equals(order.getFullname()) || !phone.equals(order.getPhone()) || !email.equals(order.getEmail())
          || !address.equals(order.getAddress()) || !orderStatus.equals(order.getOrderstatus())
          || !itemName.equals(orderDetails.get(0).getProductname()) || !itemLink.equals(orderDetails.get(0).getProductlink())
          || !itemColor.equals(orderDetails.get(0).getProductcolor()) || !itemSize.equals(orderDetails.get(0).getProductsize())
          || !itemQuantity.equals(orderDetails.get(0).getProductquantity()) || !itemUnit.equals(orderDetails.get(0).getProductunit())
          || !itemNote.equals(orderDetails.get(0).getComment())) {
        //
        order.setFullname(fullname);
        order.setPhone(phone);
        order.setEmail(email);
        order.setAddress(address);
        order.setLastupdatetime(Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok")).getTime());
        order.setOrderstatus(orderStatus);
        orderService.updateOrder(order);

        //
        orderDetails.get(0).setProductname(itemName);
        orderDetails.get(0).setProductlink(itemLink);
        orderDetails.get(0).setProductcolor(itemColor);
        orderDetails.get(0).setProductsize(itemSize);
        orderDetails.get(0).setProductquantity(itemQuantity);
        orderDetails.get(0).setProductunit(itemUnit);
        orderDetails.get(0).setComment(itemNote);
        orderService.updateOrderDetail(orderDetails.get(0));

        //
        prettyOrderId = order.getPrettyOrderId();
        username = order.getUsername();
        fullname = order.getFullname();
        phone = order.getPhone();
        email = order.getEmail();
        address = order.getAddress();
        createdDate = order.getCreateddate();
        lastUpdateTime = order.getLastupdatetime();
        orderStatus = order.getOrderstatus();
        itemName = orderDetails.get(0).getProductname();
        itemLink = orderDetails.get(0).getProductlink();
        itemColor = orderDetails.get(0).getProductcolor();
        itemSize = orderDetails.get(0).getProductsize();
        itemQuantity = orderDetails.get(0).getProductquantity();
        itemUnit = orderDetails.get(0).getProductunit();
        itemNote = orderDetails.get(0).getComment();
        message = "Đã lưu thay đổi";
      } else {
        prettyOrderId = order.getPrettyOrderId();
        username = order.getUsername();
        fullname = order.getFullname();
        phone = order.getPhone();
        email = order.getEmail();
        address = order.getAddress();
        createdDate = order.getCreateddate();
        lastUpdateTime = order.getLastupdatetime();
        orderStatus = order.getOrderstatus();
        itemName = orderDetails.get(0).getProductname();
        itemLink = orderDetails.get(0).getProductlink();
        itemColor = orderDetails.get(0).getProductcolor();
        itemSize = orderDetails.get(0).getProductsize();
        itemQuantity = orderDetails.get(0).getProductquantity();
        itemUnit = orderDetails.get(0).getProductunit();
        itemNote = orderDetails.get(0).getComment();
        message = "Không có thay đổi nào được cập nhật!!!";
      }
    }

    //
    return SUCCESS;
  }

}
