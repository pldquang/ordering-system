package com.quangpld.ordering.webapp.shiro;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import com.quangpld.core.dao.core.GenericDao;
import com.quangpld.core.user.management.entity.RolePermission;
import com.quangpld.core.user.management.entity.User;
import com.quangpld.core.user.management.entity.UserRole;

public class HibernateRealm extends AuthorizingRealm {

  /** Generic DAO for table USER. */
  private GenericDao<User, String> userDao = null;

  /** Generic DAO for table USERROLE. */
  private GenericDao<UserRole, String> userRoleDao = null;

  /** Generic DAO for table ROLEPERMISSIONS. */
  private GenericDao<RolePermission, String> rolePermissionDao = null;

  /**
   * Default constructor.
   */
  public HibernateRealm() {
    //This name must match the name in the User class's getPrincipals() method
    setName("HibernateRealm");
    // TODO: Inject CredentialsMatcher here
    HashedCredentialsMatcher credentialsMatcher = new HashedCredentialsMatcher(Md5Hash.ALGORITHM_NAME);
    credentialsMatcher.setStoredCredentialsHexEncoded(false);
    setCredentialsMatcher(credentialsMatcher);
  }

  public void setUserDao(GenericDao<User, String> userDao) {
    this.userDao = userDao;
  }

  public void setUserRoleDao(GenericDao<UserRole, String> userRoleDao) {
    this.userRoleDao = userRoleDao;
  }

  public void setRolePermissionDao(GenericDao<RolePermission, String> rolePermissionDao) {
    this.rolePermissionDao = rolePermissionDao;
  }

  @Override
  protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
    try {
      String username = (String) principals.fromRealm(getName()).iterator().next();
      User user = userDao.findById(username);
      if (user != null) {
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        String roles = userRoleDao.findById(user.getUsername()).getRoles();
        info.addRole(roles);
//        info.addStringPermissions(rolePermissionDao.findById(arg0));
        return info;
      } else {
        return null;
      }
    } catch (Exception e) {
      // TODO:
      return null;
    }
  }

  @Override
  protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
    //
    UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
    User user = null;
    try {
      user = userDao.findById(token.getUsername());
    } catch (Exception e) {
      System.out.println(e.getMessage());
      e.printStackTrace();
    }

    //
    if (user != null) {
      return new SimpleAuthenticationInfo(user.getUsername(), user.getPassword(), null, getName());
    } else {
      return null;
    }
  }

}
