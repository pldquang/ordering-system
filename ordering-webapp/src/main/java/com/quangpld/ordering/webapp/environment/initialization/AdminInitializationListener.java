package com.quangpld.ordering.webapp.environment.initialization;

import java.util.Calendar;
import java.util.TimeZone;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.context.ApplicationContext;

import com.quangpld.core.user.management.entity.User;
import com.quangpld.core.user.management.service.UserService;
import com.quangpld.ordering.system.core.common.UserRole;
import com.quangpld.ordering.webapp.util.OrderUtils;

public class AdminInitializationListener implements ServletContextListener {

  /** UserService object. */
  private UserService userService = null;

  public AdminInitializationListener() throws Exception {
    ApplicationContext context = OrderUtils.getClassPathXmlApplicationContext();
    userService = context.getBean("userService", UserService.class);
  }

  @Override
  public void contextInitialized(ServletContextEvent servletContextEvent) {
    try {
      User admin = userService.findUserById("admin");
      if (admin == null) {
        //
        admin = new User("admin", new Md5Hash("admin@123").toBase64(), "dathang24h.net@gmail.com", Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok")).getTime());
        admin.setFirstname("Admin");
        admin.setAddress1("Vietnam");
        admin.setMobilephone("(+84)1666-75-6666");
        userService.saveUser(admin);
        
        //
        userService.grantUserRole("admin", UserRole.ADMIN.toString());
      }
    } catch (Exception e) {
      // TODO:
    }
  }

  @Override
  public void contextDestroyed(ServletContextEvent servletContextEvent) {
    // TODO Auto-generated method stub
    
  }

}
