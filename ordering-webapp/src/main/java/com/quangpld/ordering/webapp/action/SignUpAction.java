/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2013-02-05
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-02-05    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.ordering.webapp.action;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;

import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.springframework.context.ApplicationContext;

import com.opensymphony.xwork2.ActionSupport;
import com.quangpld.core.common.mail.GmailTLSSender;
import com.quangpld.core.common.mail.MailSender;
import com.quangpld.core.common.util.StringUtil;
import com.quangpld.core.setting.management.entity.Setting;
import com.quangpld.core.setting.management.service.SettingService;
import com.quangpld.core.user.management.entity.User;
import com.quangpld.core.user.management.service.UserException;
import com.quangpld.core.user.management.service.UserService;
import com.quangpld.ordering.system.core.common.OrderConstants;
import com.quangpld.ordering.system.core.common.UserRole;
import com.quangpld.ordering.webapp.util.OrderUtils;

/**
 * SignUpAction class.
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
public class SignUpAction extends ActionSupport implements ServletRequestAware {

  /** serialVersionUID. */
  private static final long serialVersionUID = 5067752533724791416L;

  /** UserService object. */
  private UserService userService = null;

  /** SettingService object. */
  private SettingService settingService = null;

  private String username;
  private String password;
  private String passwordConfirm;
  private String fullname;
  private String address;
  private String phone;
  private String email;
  private boolean isAgreed;

  private HttpServletRequest request;
  private String errorMessage;

  /**
   * Default constructor.
   * 
   * @throws Exception 
   */
  public SignUpAction() throws Exception {
    try {
      ApplicationContext context = OrderUtils.getClassPathXmlApplicationContext();
      userService = context.getBean("userService", UserService.class);
      settingService = context.getBean("settingService", SettingService.class);
    } catch (Exception e) {
      OrderUtils.sendExceptionMailNotification(e);
    }
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPasswordConfirm() {
    return passwordConfirm;
  }

  public void setPasswordConfirm(String passwordConfirm) {
    this.passwordConfirm = passwordConfirm;
  }

  public String getFullname() {
    return fullname;
  }

  public void setFullname(String fullname) {
    this.fullname = fullname;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public boolean getIsAgreed() {
    return isAgreed;
  }

  public void setIsAgreed(boolean isAgreed) {
    this.isAgreed = isAgreed;
  }

  @Override
  public void setServletRequest(HttpServletRequest request) {
    this.request = request;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  @Override
  public String execute() throws Exception {
    try {
      if (isReadyForSubmit()) {
        //
        User got = userService.findUserById(username);
        if (got != null) {
          errorMessage = "Tên đăng nhập đã tồn tại trong hệ thống!!!";
          return INPUT;
        }
        
        //
        User example = new User();
        example.setEmail(email);
        List<User> users = userService.findUserByExample(example);
        if (users.size() > 0) {
          errorMessage = "E-mail đã tồn tại trong hệ thống!!!";
          return INPUT;
        }

        //
        User user = new User(username, new Md5Hash(password).toBase64(), email, Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok")).getTime());
        user.setFirstname(fullname);
        user.setAddress1(address);
        user.setMobilephone(phone);
        userService.saveUser(user);

        //
        userService.grantUserRole(username, UserRole.MEMBER.toString());

        //
        Setting setting = settingService.findSettingById(OrderConstants.EMAIL_DEFAULT_GMAIL_USERNAME_KEY);
        String emailUsername = setting != null ? setting.getSettingvalue() : OrderConstants.EMAIL_DEFAULT_GMAIL_USERNAME_VALUE;
        setting = settingService.findSettingById(OrderConstants.EMAIL_DEFAULT_GMAIL_PASSWORD_KEY);
        String emailPassword = setting != null ? setting.getSettingvalue() : OrderConstants.EMAIL_DEFAULT_GMAIL_PASSWORD_VALUE;
        setting = settingService.findSettingById(OrderConstants.EMAIL_REGISTER_SUCCESSFUL_NOTIFICATION_SUBJECT_KEY);
        String emailSubject = setting != null ? setting.getSettingvalue() : OrderConstants.EMAIL_REGISTER_SUCCESSFUL_NOTIFICATION_SUBJECT_VALUE;
        setting = settingService.findSettingById(OrderConstants.EMAIL_REGISTER_SUCCESSFUL_NOTIFICATION_CONTENT_KEY);
        String emailContent = setting != null ? setting.getSettingvalue() : OrderConstants.EMAIL_REGISTER_SUCCESSFUL_NOTIFICATION_CONTENT_VALUE;
        emailContent = emailContent.replaceAll("\\{fullname\\}", fullname)
                                   .replaceAll("\\{username\\}", username)
                                   .replaceAll("\\{password\\}", password);
        MailSender mailSender = new GmailTLSSender(emailUsername, emailPassword, emailUsername, email, emailSubject, emailContent);
        mailSender.sendMail();

        //
        return SUCCESS;
      }
    } catch (UserException e) {
      errorMessage = "Có lỗi xảy ra trong quá trình xử lý - Vui lòng thử lại sau ít phút!!!";
    }

    //
    return INPUT;
  }

  private boolean isReadyForSubmit() {
    if (StringUtil.isNotEmpty(username) && StringUtil.isNotEmpty(password) && StringUtil.isNotEmpty(passwordConfirm)
        && StringUtil.isNotEmpty(fullname) && StringUtil.isNotEmpty(address) && StringUtil.isNotEmpty(phone)
        && StringUtil.isNotEmpty(email) && passwordConfirm.equals(password) && isAgreed) {
      //
      if (password.length() < 6) {
        return false;
      }

      //
      Pattern pattern = Pattern.compile(OrderUtils.USERNAME_REGEX);
      Matcher matcher = pattern.matcher(username);
      if (!matcher.matches()) {
        return false;
      }

      //
      pattern = Pattern.compile(OrderUtils.PHONE_REGEX);
      matcher = pattern.matcher(phone);
      if (!matcher.matches()) {
        return false;
      }

      //
      pattern = Pattern.compile(OrderUtils.EMAIL_REGEX);
      matcher = pattern.matcher(email);
      if (!matcher.matches()) {
        return false;
      }

      //
      String remoteAddr = request.getRemoteAddr();
      ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
      reCaptcha.setPrivateKey("6LcLBN4SAAAAAG1CkVadw1Ko7o-JSE5Jjq-0VtWJ");
      String challenge = request.getParameter("recaptcha_challenge_field");
      String uresponse = request.getParameter("recaptcha_response_field");
      ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(remoteAddr, challenge, uresponse);
      if (!reCaptchaResponse.isValid()) {
        errorMessage = "Mã xác nhận không chính xác!!!";
        return false;
      }

      return true;
    }

    //
    return false;
  }

}
