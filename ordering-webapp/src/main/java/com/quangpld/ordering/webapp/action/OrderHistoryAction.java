/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    |               |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.ordering.webapp.action;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.context.ApplicationContext;

import com.opensymphony.xwork2.ActionSupport;
import com.quangpld.ordering.system.core.entity.Order;
import com.quangpld.ordering.system.core.service.OrderService;
import com.quangpld.ordering.webapp.util.OrderUtils;

/**
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
public class OrderHistoryAction extends ActionSupport {

  /** serialVersionUID. */
  private static final long serialVersionUID = 7547688348982474315L;

  /** OrderService object. */
  private OrderService orderService = null;

  private List<Order> orders = null;

  public List<Order> getOrders() {
    return orders;
  }

  /**
   * Default constructor.
   * @throws Exception 
   */
  public OrderHistoryAction() throws Exception {
    try {
      ApplicationContext context = OrderUtils.getClassPathXmlApplicationContext();
      orderService = (OrderService) context.getBean("orderService", OrderService.class);
    } catch (Exception e) {
      OrderUtils.sendExceptionMailNotification(e);
    }
  }

  @Override
  public String execute() throws Exception {
    try {
      //
      Subject currentUser = SecurityUtils.getSubject();
      Order orderFilter = new Order();
      orderFilter.setUsername(currentUser.getPrincipal().toString());
      orders = orderService.findOrderByExample(orderFilter);

      //
      return SUCCESS;
    } catch (Exception e) {
      // Send mail notification
      OrderUtils.sendExceptionMailNotification(e);
      return ERROR;
    }
  }

}
