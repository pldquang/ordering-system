/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2013-01-27
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-01-27    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.ordering.webapp.action;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.context.ApplicationContext;

import com.opensymphony.xwork2.ActionSupport;
import com.quangpld.ordering.system.core.entity.Order;
import com.quangpld.ordering.system.core.entity.OrderDetail;
import com.quangpld.ordering.system.core.entity.OrderDetailBill;
import com.quangpld.ordering.system.core.service.OrderService;
import com.quangpld.ordering.webapp.util.OrderUtils;

/**
 * OrderAction class.
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
public class OrderReceiptAction extends ActionSupport {

  /** serialVersionUID. */
  private static final long serialVersionUID = 5806161610224093624L;

  /** OrderService object. */
  private OrderService orderService = null;

  private int orderId;
  private String prettyOrderId;
  private String username;
  private String fullname;
  private String phone;
  private String email;
  private String address;
  private String itemName;
  private String itemLink;
  private String itemColor;
  private String itemSize;
  private String itemQuantity;
  private String itemUnit;
  private String itemNote;

  private String currency;
  private double exchangeRate;
  private double originalCost;
  private double totalOriginalCost;
  private double tax;
  private double totalTax;
  private double shippingInternationalCost;
  private double totalShippingInternationalCost;
  private double shippingVietnamCost;
  private double totalShippingVietnamCost;
  private double shippingInternalCost;
  private double totalShippingInternalCost;
  private double additionalCost;
  private double totalAdditionalCost;
  private double totalCost;
  private double totalCostVN;

  private DecimalFormat decimalFormat;

  /**
   * Default constructor.
   * @throws Exception 
   */
  public OrderReceiptAction() throws Exception {
    try {
      //
      ApplicationContext context = OrderUtils.getClassPathXmlApplicationContext();
      orderService = context.getBean("orderService", OrderService.class);

      //
      DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
      decimalFormatSymbols.setDecimalSeparator('.');
      decimalFormatSymbols.setGroupingSeparator(',');
      decimalFormat = new DecimalFormat("#,##0.00", decimalFormatSymbols);
    } catch (Exception e) {
      OrderUtils.sendExceptionMailNotification(e);
    }
  }

  public int getOrderId() {
    return orderId;
  }

  public void setOrderId(int orderId) {
    this.orderId = orderId;
  }

  public String getPrettyOrderId() {
    return prettyOrderId;
  }

  public void setPrettyOrderId(String prettyOrderId) {
    this.prettyOrderId = prettyOrderId;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getFullname() {
    return fullname;
  }

  public void setFullname(String fullname) {
    this.fullname = fullname;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getItemName() {
    return itemName;
  }

  public void setItemName(String itemName) {
    this.itemName = itemName;
  }

  public String getItemLink() {
    return itemLink;
  }

  public void setItemLink(String itemLink) {
    this.itemLink = itemLink;
  }

  public String getItemColor() {
    return itemColor;
  }

  public void setItemColor(String itemColor) {
    this.itemColor = itemColor;
  }

  public String getItemSize() {
    return itemSize;
  }

  public void setItemSize(String itemSize) {
    this.itemSize = itemSize;
  }

  public String getItemQuantity() {
    return itemQuantity;
  }

  public void setItemQuantity(String itemQuantity) {
    this.itemQuantity = itemQuantity;
  }

  public String getItemUnit() {
    return itemUnit;
  }

  public void setItemUnit(String itemUnit) {
    this.itemUnit = itemUnit;
  }

  public String getItemNote() {
    return itemNote;
  }

  public void setItemNote(String itemNote) {
    this.itemNote = itemNote;
  }

  public String getCurrency() {
    return currency;
  }

  public double getExchangeRate() {
    return exchangeRate;
  }

  public double getOriginalCost() {
    return originalCost;
  }

  public double getTotalOriginalCost() {
    return totalOriginalCost;
  }

  public double getTax() {
    return tax;
  }

  public double getTotalTax() {
    return totalTax;
  }

  public double getShippingInternationalCost() {
    return shippingInternationalCost;
  }

  public double getTotalShippingInternationalCost() {
    return totalShippingInternationalCost;
  }

  public double getShippingVietnamCost() {
    return shippingVietnamCost;
  }

  public double getTotalShippingVietnamCost() {
    return totalShippingVietnamCost;
  }

  public double getShippingInternalCost() {
    return shippingInternalCost;
  }

  public double getTotalShippingInternalCost() {
    return totalShippingInternalCost;
  }

  public double getAdditionalCost() {
    return additionalCost;
  }

  public double getTotalAdditionalCost() {
    return totalAdditionalCost;
  }

  public double getTotalCost() {
    return totalCost;
  }

  public double getTotalCostVN() {
    return totalCostVN;
  }

  public String getPrettyOriginalCost() {
    return decimalFormat.format(originalCost);
  }

  public String getPrettyTotalOriginalCost() {
    return decimalFormat.format(totalOriginalCost);
  }

  public String getPrettyTax() {
    return decimalFormat.format(tax);
  }

  public String getPrettyTotalTax() {
    return decimalFormat.format(totalTax);
  }

  public String getPrettyShippingInternationalCost() {
    return decimalFormat.format(shippingInternationalCost);
  }

  public String getPrettyTotalShippingInternationalCost() {
    return decimalFormat.format(totalShippingInternationalCost);
  }

  public String getPrettyShippingVietnamCost() {
    return decimalFormat.format(shippingVietnamCost);
  }

  public String getPrettyTotalShippingVietnamCost() {
    return decimalFormat.format(totalShippingVietnamCost);
  }

  public String getPrettyShippingInternalCost() {
    return decimalFormat.format(shippingInternalCost);
  }

  public String getPrettyTotalShippingInternalCost() {
    return decimalFormat.format(totalShippingInternalCost);
  }

  public String getPrettyAdditionalCost() {
    return decimalFormat.format(additionalCost);
  }

  public String getPrettyTotalAdditionalCost() {
    return decimalFormat.format(totalAdditionalCost);
  }

  public String getPrettyTotalCost() {
    return decimalFormat.format(totalCost);
  }

  public String getPrettyTotalCostVN() {
    return decimalFormat.format(totalCostVN);
  }

  @Override
  public String execute() throws Exception {
    try {
      //
      Order order = orderService.findOrderById(orderId);
      if (order == null) {
        return ERROR;
      }

      //
      Subject currentUser = SecurityUtils.getSubject();
      if (!order.getUsername().equals(currentUser.getPrincipal().toString())) {
        return ERROR;
      }

      //
      OrderDetail example = new OrderDetail();
      example.setOrderid(orderId);
      List<OrderDetail> orderDetails = orderService.getOrderDetailByExample(example);
      if (orderDetails.size() == 0) {
        return ERROR;
      }

      //
      prettyOrderId = order.getPrettyOrderId();
      username = order.getUsername();
      fullname = order.getFullname();
      phone = order.getPhone();
      email = order.getEmail();
      address = order.getAddress();
      itemName = orderDetails.get(0).getProductname();
      itemLink = orderDetails.get(0).getProductlink();
      itemColor = orderDetails.get(0).getProductcolor();
      itemSize = orderDetails.get(0).getProductsize();
      itemQuantity = orderDetails.get(0).getProductquantity();
      itemUnit = orderDetails.get(0).getProductunit();

      //
      OrderDetailBill orderDetailBill = orderService.getOrderDetailBillById(orderDetails.get(0).getOrderdetailid());
      if (orderDetailBill == null) {
        return ERROR;
      }

      //
      currency = orderDetailBill.getCurrency();
      exchangeRate = orderDetailBill.getExchangerate();
      originalCost = orderDetailBill.getOriginalcost();
      totalOriginalCost = originalCost * Double.parseDouble(itemQuantity);
      tax = orderDetailBill.getTax();
      totalTax = tax * Double.parseDouble(itemQuantity);
      shippingInternationalCost = orderDetailBill.getShippinginternationalcost();
      totalShippingInternationalCost = shippingInternationalCost * Double.parseDouble(itemQuantity);
      shippingVietnamCost = orderDetailBill.getShippingvietnamcost();
      totalShippingVietnamCost = shippingVietnamCost * Double.parseDouble(itemQuantity);
      shippingInternalCost = orderDetailBill.getShippinginternalcost();
      totalShippingInternalCost = shippingInternalCost * Double.parseDouble(itemQuantity);
      additionalCost = orderDetailBill.getAdditionalcost();
      totalAdditionalCost = additionalCost * Double.parseDouble(itemQuantity);
      totalCost = totalOriginalCost + totalTax + totalShippingInternationalCost + totalShippingVietnamCost
                  + totalShippingInternalCost + totalAdditionalCost;
      totalCostVN = totalCost * exchangeRate;

      //
      return SUCCESS;
    } catch (Exception e) {
      // Send mail notification
      OrderUtils.sendExceptionMailNotification(e);
      return ERROR;
    }
  }

}
