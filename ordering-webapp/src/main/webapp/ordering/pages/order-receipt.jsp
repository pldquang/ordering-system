<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="description" content="<s:text name="dathang24h.order-receipt.description" />" />
  <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="" />
  <meta name="copyright" content="" />
  <meta name="robots" content="all" />
  <meta name="geo.region" content="US-OH" />
  <meta name="geo.placename" content="Cincinnati" />
  <meta name="geo.position" content="39.108269;-84.497414" />
  <meta name="ICBM" content="39.108269, -84.497414" />
  <title><s:text name="dathang24h.title" /> - <s:text name="dathang24h.order-receipt.title" /></title>
  <link rel="canonical" href="#"/>
  <link rel="shortcut icon" href="<s:url value="/" />img/logo.ico" /> 
  <link rel="apple-touch-icon" href="#"/>

  <!-- CSS: implied media=all -->
  <!-- CSS build script-->
  <link href="<s:url value="/" encode="false" />css/framework.css" rel="stylesheet" type="text/css"  media="all"  />
  <link href="<s:url value="/" encode="false" />css/style_desktop.css" rel="stylesheet" type="text/css"  media="all"  />
  <link href="<s:url value="/" encode="false" />css/bootstrap.min.css" rel="stylesheet" type="text/css"  media="all"  />
  <link href="<s:url value="/" encode="false" />css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"  media="all"  />
  <link href="<s:url value="/" encode="false" />css/dropmenu.css" rel="stylesheet" type="text/css"  media="all"  />
  <link href="<s:url value="/" encode="false" />css/accordion-a.css" rel="stylesheet" />
  <link href="<s:url value="/" encode="false" />css/accordion-a.minimal.css" rel="stylesheet" />
  <link href="<s:url value="/" encode="false" />css/jquery.lightbox.css" rel="stylesheet" type="text/css" />
  <link href="<s:url value="/" encode="false" />css/bannerscollection_kenburns.css" rel="stylesheet" type="text/css">
  <link href="<s:url value="/" encode="false" />css/theme-controls.css" type="text/css" rel="stylesheet" />
  <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600' rel='stylesheet' type='text/css'>
  <!-- end CSS-->

  <!-- Modernizr load -->
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

</head>

<body class="body-bg-line" style="background-color:#FFFFFF; background-image:url('<s:url value="/" />img/background.png')">

  <!--  -->
  <div class="container">
    <hr />
    <div class="row">
      <div class="span2 pagination-centered">
        <img class="logo-fix" src="<s:url value="/" />img/logo.png" />
      </div>
      <div class="span10 pagination-centered">
        <h1><s:text name="dathang24h.order.label.heading" /></h1>
        <h4><s:text name="dathang24h.order.label.order-id" />&nbsp;<span class="label" style="font-size:20px;"><s:text name="dathang24h.order.label.no" /><s:property value="prettyOrderId" /></span></h4>
        <div><em>(<s:text name="dathang24h.order.label.date" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,&nbsp;<s:text name="dathang24h.order.label.month" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,&nbsp;<s:text name="dathang24h.order.label.year" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</em></div>
      </div>
    </div>
    <div class="row">
      <div class="span12">
        <table class="table table-striped table-bordered">
          <tbody>
            <tr>
              <td width="30%"  rowspan="4"  style="vertical-align: middle;"><s:text name="dathang24h.order.label.user-info" /></td>
              <td width="20%" ><s:text name="dathang24h.order.label.fullname" /></td>
              <td width="50%" ><s:property value="fullname" /></td>
            </tr>
            <tr>
              <td><s:text name="dathang24h.order.label.address" /></td>
              <td><s:property value="address" /></td>
            </tr>
            <tr>
              <td><s:text name="dathang24h.order.label.e-mail" /></td>
              <td><s:property value="email" /></td>
            </tr>
            <tr>
              <td><s:text name="dathang24h.order.label.phone" /></td>
              <td><s:property value="phone" /></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="span12">
        <h4><s:text name="dathang24h.order.label.order-info" /></h4>
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th width="40%"><div class="pagination-centered"><s:text name="dathang24h.order.label.product-name" /></div></th>
              <th width="15%"><div class="pagination-centered"><s:text name="dathang24h.order.label.product-color" /></div></th>
              <th width="15%"><div class="pagination-centered"><s:text name="dathang24h.order.label.product-size" /></div></th>
              <th width="15%"><div class="pagination-centered"><s:text name="dathang24h.order.label.product-quantity" /></div></th>
              <th width="15%"><div class="pagination-centered"><s:text name="dathang24h.order.label.product-cost" /></div></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><a target="_blank" href="<s:property value="itemLink" />"><s:property value="itemName" /></a></td>
              <td><div class="pagination-centered"><s:property value="itemColor" /></div></td>
              <td><div class="pagination-centered"><s:property value="itemSize" /></div></td>
              <td><div class="pagination-centered"><s:property value="itemQuantity" /></div></td>
              <td>
                <div class="pagination-centered">
                  <s:if test="originalCost > 0">
                    <s:property value="currency" /><s:property value="prettyOriginalCost" />
                  </s:if>
                  <s:else>
                    <s:text name="dathang24h.order-status.new" />
                  </s:else>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="span12">
        <h4><s:text name="dathang24h.order.label.cost" /></h4>
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th width="40%"><s:text name="dathang24h.order.label.cost-type" /></th>
              <th width="20%"><div class="pagination-centered"><s:text name="dathang24h.order.label.single-cost" /></div></th>
              <th width="20%"><div class="pagination-centered"><s:text name="dathang24h.order.label.cost-quantity" /></div></th>
              <th width="20%"><div class="pagination-centered"><s:text name="dathang24h.order.label.total-cost" /></div></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><s:text name="dathang24h.order.label.product-cost" /></td>
              <td><div class="pagination-centered"><s:property value="currency" /><s:property value="prettyOriginalCost" /></div></td>
              <td><div class="pagination-centered"><s:property value="itemQuantity" /></div></td>
              <td>
                <div class="pagination-centered">
                  <s:if test="originalCost > 0">
                    <s:property value="currency" /><s:property value="prettyTotalOriginalCost" />
                  </s:if>
                  <s:else>
                    <s:text name="dathang24h.order-status.new" />
                  </s:else>
                </div>
              </td>
            </tr>
            <tr>
              <td><s:text name="dathang24h.order.label.import-tax" /></td>
              <td><div class="pagination-centered"><s:property value="currency" /><s:property value="prettyTax" /></div></td>
              <td><div class="pagination-centered"><s:property value="itemQuantity" /></div></td>
              <td>
                <div class="pagination-centered">
                  <s:if test="originalCost > 0">
                    <s:property value="currency" /><s:property value="prettyTotalTax" />
                  </s:if>
                  <s:else>
                    <s:text name="dathang24h.order-status.new" />
                  </s:else>
                </div>
              </td>
            </tr>
            <tr>
              <td><s:text name="dathang24h.order.label.international-cost" /></td>
              <td><div class="pagination-centered"><s:property value="currency" /><s:property value="prettyShippingInternationalCost" /></div></td>
              <td><div class="pagination-centered"><s:property value="itemQuantity" /></div></td>
              <td>
                <div class="pagination-centered">
                  <s:if test="originalCost > 0">
                    <s:property value="currency" /><s:property value="prettyTotalShippingInternationalCost" />
                  </s:if>
                  <s:else>
                    <s:text name="dathang24h.order-status.new" />
                  </s:else>
                </div>
              </td>
            </tr>
            <tr>
              <td><s:text name="dathang24h.order.label.shipping-cost" /></td>
              <td><div class="pagination-centered"><s:property value="currency" /><s:property value="prettyShippingVietnamCost" /></div></td>
              <td><div class="pagination-centered"><s:property value="itemQuantity" /></div></td>
              <td>
                <div class="pagination-centered">
                  <s:if test="originalCost > 0 && shippingVietnamCost > 0">
                    <s:property value="currency" /><s:property value="prettyTotalShippingVietnamCost" />
                  </s:if>
                  <s:else>
                    <s:text name="dathang24h.order-status.new" />
                  </s:else>
                </div>
              </td>
            </tr>
            <tr>
              <td><s:text name="dathang24h.order.label.internal-shipping-cost" /></td>
              <td><div class="pagination-centered"><s:property value="currency" /><s:property value="prettyShippingInternalCost" /></div></td>
              <td><div class="pagination-centered"><s:property value="itemQuantity" /></div></td>
              <td>
                <div class="pagination-centered">
                  <s:if test="originalCost > 0">
                    <s:property value="currency" /><s:property value="prettyTotalShippingInternalCost" />
                  </s:if>
                  <s:else>
                    <s:text name="dathang24h.order-status.new" />
                  </s:else>
                </div>
              </td>
            </tr>
            <tr>
              <td><s:text name="dathang24h.order.label.additional-cost" /></td>
              <td><div class="pagination-centered"><s:property value="currency" /><s:property value="prettyAdditionalCost" /></div></td>
              <td><div class="pagination-centered"><s:property value="itemQuantity" /></div></td>
              <td>
                <div class="pagination-centered">
                  <s:if test="originalCost > 0">
                    <s:property value="currency" /><s:property value="prettyTotalAdditionalCost" />
                  </s:if>
                  <s:else>
                    <s:text name="dathang24h.order-status.new" />
                  </s:else>
                </div>
              </td>
            </tr>
            <tr>
              <td colspan="3"><div class="pagination-right"><strong><s:text name="dathang24h.order.label.final-cost" /></strong></div></td>
              <td>
                <div class="pagination-centered">
                  <s:if test="originalCost > 0">
                    <p><s:property value="currency" /><s:property value="prettyTotalCost" /></p>
                    <p><em>(<s:text name="dathang24h.order.label.exchange-rate" />: 1&nbsp;<s:property value="currency" /> = <s:property value="exchangeRate" />&nbsp;<s:text name="dathang24h.order.label.vnd" />)</em></p>
                    <p><strong><s:property value="prettyTotalCostVN" />&nbsp;<s:text name="dathang24h.order.label.vnd" /></strong></p>
                  </s:if>
                  <s:else>
                    <s:text name="dathang24h.order-status.new" />
                  </s:else>
                </div>
              </td>
            </tr>
            <tr>
              <td colspan="3"><div class="pagination-right"><strong><s:text name="dathang24h.order.label.deposited" /></strong></div></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="3"><div class="pagination-right"><strong><s:text name="dathang24h.order.label.remaining" /></strong></div></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="span12">
        <p><i class="icon-chevron-right"></i><s:text name="dathang24h.order.label.rule1" /></p>
        <p><i class="icon-chevron-right"></i><s:text name="dathang24h.order.label.rule2" /></p>
        <p><i class="icon-chevron-right"></i><s:text name="dathang24h.order.label.rule3" /></p>
        <p><i class="icon-chevron-right"></i><s:text name="dathang24h.order.label.rule4" /></p>
        <p><i class="icon-chevron-right"></i><s:text name="dathang24h.order.label.rule5" /></p>
        <p><i class="icon-chevron-right"></i><s:text name="dathang24h.order.label.rule6" /></p>
        <p><input type="checkbox" checked="checked" disabled /><s:text name="dathang24h.order.label.customer-agreement" /></p>
      </div>
    </div>
    <div class="row">
      <div class="span12">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th width="50%"><div class="pagination-centered"><s:text name="dathang24h.order.label.dathang24h-signature" /></div></th>
              <th width="50%"><div class="pagination-centered"><s:text name="dathang24h.order.label.customer-signature" /></div></th>
            </tr>
          </thead>
          <tbody>
            <tr style="height:150px;">
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <!-- JavaScript libraries -->
  <script src="<s:url value="/" encode="false" />js/jquery.min.js" type="text/javascript"></script>
  <script src="<s:url value="/" encode="false" />js/bootstrap.min.js" type="text/javascript"></script>    
  <script src="<s:url value="/" encode="false" />js/bootstrap-collapse.js" type="text/javascript"></script>
  <script src="<s:url value="/" encode="false" />js/ordering/order.js" type="text/javascript"></script>

  <!-- Histats.com  START (hidden counter)-->
  <script type="text/javascript">document.write(unescape("%3Cscript src=%27http://s10.histats.com/js15.js%27 type=%27text/javascript%27%3E%3C/script%3E"));</script>
  <a href="http://www.histats.com" target="_blank" title="free stats" ><script  type="text/javascript" >
  try {Histats.start(1,2257283,4,0,0,0,"");
  Histats.track_hits();} catch(err){};
  </script></a>
  <noscript><a href="http://www.histats.com" target="_blank"><img  src="http://sstatic1.histats.com/0.gif?2257283&101" alt="free stats" border="0"></a></noscript>
  <!-- Histats.com  END  -->
</body>
</html>
