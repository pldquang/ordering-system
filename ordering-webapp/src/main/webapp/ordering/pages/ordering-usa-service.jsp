<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="description" content="<s:text name="dathang24h.ordering-usa-service.description" />" />
  <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="" />
  <meta name="copyright" content="" />
  <meta name="robots" content="all" />
  <meta name="geo.region" content="US-OH" />
  <meta name="geo.placename" content="Cincinnati" />
  <meta name="geo.position" content="39.108269;-84.497414" />
  <meta name="ICBM" content="39.108269, -84.497414" />
  <title><s:text name="dathang24h.title" /> - <s:text name="dathang24h.ordering-usa-service.title" /></title>
  <link rel="canonical" href="#"/>
  <link rel="shortcut icon" href="<s:url value="/" />img/logo.ico" /> 
  <link rel="apple-touch-icon" href="#"/>

  <!-- CSS: implied media=all -->
  <!-- CSS build script-->
  <link href="<s:url value="/" encode="false" />css/framework.css" rel="stylesheet" type="text/css"  media="all"  />
  <link href="<s:url value="/" encode="false" />css/style_desktop.css" rel="stylesheet" type="text/css"  media="all"  />
  <link href="<s:url value="/" encode="false" />css/bootstrap.min.css" rel="stylesheet" type="text/css"  media="all"  />
  <link href="<s:url value="/" encode="false" />css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"  media="all"  />
  <link href="<s:url value="/" encode="false" />css/dropmenu.css" rel="stylesheet" type="text/css"  media="all"  />
  <link href="<s:url value="/" encode="false" />css/accordion-a.css" rel="stylesheet" />
  <link href="<s:url value="/" encode="false" />css/accordion-a.minimal.css" rel="stylesheet" />
  <link href="<s:url value="/" encode="false" />css/jquery.lightbox.css" rel="stylesheet" type="text/css" />
  <link href="<s:url value="/" encode="false" />css/bannerscollection_kenburns.css" rel="stylesheet" type="text/css">
  <link href="<s:url value="/" encode="false" />css/theme-controls.css" type="text/css" rel="stylesheet" />
  <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600' rel='stylesheet' type='text/css'>
  <!-- end CSS-->

  <!-- Modernizr load -->
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

</head>

<body class="body-bg-line">

  <!-- / Navigation -->
  <div class="navbar navbar-inverse navbar-fixed-top" id="topstaticnav">
    <div class="navbar-inner visible-desktop">
      <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <div class="nav-collapse collapse">
          <ul class="nav">
            <li class="dropdown">
              <a href="#" data-toggle="dropdown" class="dropdown-toggle"><s:text name="dathang24h.top-navigation.service.label" /><b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="ordering-usa-service.action"><s:text name="dathang24h.top-navigation.service.ordering-usa-service.label" /></a></li>
                <li><a href="#"><s:text name="dathang24h.top-navigation.service.shipping-usa-service.label" /></a></li>
              </ul>
            </li>
            <li><a href="#"><s:text name="dathang24h.top-navigation.news.label" /></a></li>
            <li><a href="online-stores.action"><s:text name="dathang24h.top-navigation.online-stores.label" /></a></li>
            <li><a href="contact-us.action"><s:text name="dathang24h.top-navigation.contact-us.label" /></a></li>
            <li class="saletag"><a href="#"><s:text name="dathang24h.top-navigation.sale-off" /></a></li>
          </ul>                      
        </div><!--/.nav-collapse -->
      </div>
    </div>
  </div>

  <!-- / TOP MENU -->
  <div class="container" id="container-fix">
    <div class="row">       
      <div class="span12">
        <div class="row-fluid">
          <div class="span4 pull-right header-fix" id="SignUp">
            <div class="header-points"  style="margin:0 0 10px;">
              <shiro:notAuthenticated>
                <a href="signup.action" class="btn btn-primary"><i class="icon-white icon-ok"></i>&nbsp;<s:text name="dathang24h.user-menu.button.register.label" /></a>
                <a data-toggle="modal" href="#signinModal" class="btn btn-success"><i class="icon-white icon-user"></i>&nbsp;<s:text name="dathang24h.user-menu.button.signin.label" /></a>
              </shiro:notAuthenticated>
              <shiro:authenticated>
                <div class="btn-group">
                  <a class="btn btn-success dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="icon-white icon-wrench"></i>&nbsp;<shiro:principal/>
                    <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu pull-right">
                    <shiro:hasRole name="ADMIN">
                      <li><a href="../admin/index.action"><s:text name="dathang24h.user-menu.button.admin-page.label" /></a></li>
                    </shiro:hasRole>
                    <li><a href="user-profile.action"><s:text name="dathang24h.user-menu.button.user-profile.label" /></a></li>
                    <li><a href="order-history.action"><s:text name="dathang24h.user-menu.button.order-history.label" /></a></li>
                    <li><a href="signout.action"><s:text name="dathang24h.user-menu.button.signout.label" /></a></li>
                  </ul>
                </div>
              </shiro:authenticated>
            </div>
          </div>
        </div>
        <div class="row-fluid">
          <div class="span4 header-left">
            <span class="pop-text"><s:text name="dathang24h.advertising.express.label" /></span><br />
            <span class="regular-text"><span class="badge">1</span>&nbsp;<s:text name="dathang24h.advertising.step1.label" /></span><br />
            <span class="regular-text"><span class="badge badge-warning">2</span>&nbsp;<s:text name="dathang24h.advertising.step2.label" /></span><br />
            <span class="regular-text"><span class="badge badge-important">3</span>&nbsp;<s:text name="dathang24h.advertising.step3.label" /></span><br />
            <span class="regular-text"><span class="badge badge-success">4</span>&nbsp;<s:text name="dathang24h.advertising.step4.label" /></span><br />
            <span class="phone-header"><img src="<s:url value="/" />img/phone_telephone_call.png" /><strong><s:text name="dathang24h.hotline" /></strong></span>
          </div>
          <div class="span4 logo-div">
            <a href="<s:url value="index.action" />" title=""><img class="logo-fix" src="<s:url value="/" />img/logo.png" alt=""></a>
    	  </div>                 
          <div class="span4 pull-right header-fix">
            <p class="header-points"><s:text name="dathang24h.user-menu.follow-us.label" /></p>
            <p class="header-points social-links-header"><a href="#" target="_blank">Twitter</a> - <a href="#" target="_blank">Facebook</a></p>
            <p><img src="<s:url value="/" />img/arrow-right.png" alt="" /><a class="btn btn-large btn-danger" href="<s:url value="order.action" />"><i class="icon-white icon-list-alt"></i>&nbsp;<s:text name="dathang24h.user-menu.button.order-receipt.label" /></a></p>
          </div>
        </div>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span12">
        <div style="position: static;" class="navbar">
          <div class="navbar-inner">
            <div class="container-fluid">
              <div class="">
                <ul class="nav">
                  <li><a href="index.action"><i class="icon-home"></i></a></li>
				  <li class="visible-desktop"><a href="order-instruction.action"><s:text name="dathang24h.middle-navigation.order-instruction.label" /></a></li>
				  <li class="visible-desktop"><a href="price-rate-instruction.action"><s:text name="dathang24h.middle-navigation.price-rate-instruction.label" /></a></li>
                  <li class="visible-desktop"><a href="payment-instruction.action"><s:text name="dathang24h.middle-navigation.payment-instruction.label" /></a></li>
                  <li class="visible-desktop"><a href="faq.action"><s:text name="dathang24h.middle-navigation.faq.label" /></a></li>
                  <li class="visible-desktop"><a href="online-support.action"><s:text name="dathang24h.middle-navigation.online-support.label" /></a></li>
                </ul>
			  </div><!-- /.nav-collapse -->
              <form id="search_form" name="search" action="/search">
                <button type="submit" class="btn btn-cta pull-right"  name="s" disabled><i class="icon-search"></i></button>                		                        
                <input type="text" placeholder="Tìm kiếm" class="search-query typeahead pull-right" data-provide="typeahead" name="q" autocomplete="off">
              </form>                                
            </div>
          </div><!-- /navbar-inner -->
        </div>
      </div>                     
    </div>        
  </div>

  <!-- / CONTAINER -->
  <div class="container">
    <div class="well">
      <div class="alert alert-success pagination-centered"><h2><img alt="" src="<s:url value="/" />img/Network-Service.png" />&nbsp;<s:text name="dathang24h.ordering-usa-service.label.heading" /></h2></div>
      <hr />
      <div class="well">
        <p><img alt="" src="<s:url value="/" />img/1361800772_Problem.png" />Bạn đang có nhu cầu mua hàng hóa trên các website cung cấp dịch vụ mua hàng trực tuyến uy tín hàng đầu ở Mỹ như <a target="_blank" href="http://www.amazon.com/">Amazon</a>, <a target="_blank" href="http://www.ebay.com/">e-Bay</a>, <a target="_blank" href="http://www.walmart.com/">Walmart</a>, <a target="_blank" href="http://www.newegg.com/">NewEgg</a>... Bạn gặp khó khăn với các hình thức thanh toán trực tuyến, hay khó khăn khi hầu hết các mặt hàng không nhận ship về Việt Nam?</p>
        <div class="pagination-centered"><img alt="" src="<s:url value="/" />img/Online-payment.gif" /></div><br />
        <p><img alt="" src="<s:url value="/" />img/1361800846_question-balloon_blue.png" width="32px" />Vậy làm cách nào để bạn sở hữu những món hàng ưng ý từ các nhà cung cấp dịch vụ uy tín kể trên một cách nhanh nhất, đơn giản nhất, thuận tiện nhất, an toàn nhất và đảm bảo nhất?</p>
        <p><img alt="" src="<s:url value="/" />img/idea.png" width="32px" />Thật đơn giản, bạn chỉ cần lựa chọn dịch vụ <strong>Mua hàng US</strong> được cung cấp tại website <a href="index.action">dathang24h.net</a>, và chỉ cần 1 cú click chuột, bạn sẽ nhận được hàng hóa từ hơn 50 bang của Mỹ vận chuyển về tận cửa nhà bạn.</p>
        <p><img src="<s:url value="/" />img/old-edit-redo.png">Xem chi tiết <a href="order-instruction.action"><strong>Hướng dẫn mua hàng</strong></a> để biết phương thức đặt hàng tại website <a href="index.action">dathang24h.net</a> và nhận báo giá tốt nhất.</p>
        <div class="pagination-centered"><img alt="" src="<s:url value="/" />img/Online-Shopping03.jpg"></div>
      </div>
    </div>

    <!-- signin modal-->
    <div id="signinModal" class="modal hide">
      <div class="modal-header">
        <a class="close" data-dismiss="modal" >&times;</a>
        <h3><s:text name="dathang24h.modal.signin.label.heading" /></h3>
      </div>
      <div class="modal-body">
        <s:form method="post" id="formSignUp" cssClass="well form-horizontal" action="signin.action" theme="simple">
          <div class="control-group">
            <label class="control-label" for="username"><s:text name="dathang24h.modal.signin.label.username" /></label>
            <div class="controls">
              <s:textfield id="username" name="username" />
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="password"><s:text name="dathang24h.modal.signin.label.password" /></label>
            <div class="controls">
              <s:password id="password" name="password" />
            </div>
          </div>
          <div class="control-group">
            <div class="controls">
              <label class="checkbox">
                <input type="checkbox">&nbsp;<s:text name="dathang24h.modal.signin.label.remember-me" />
              </label>
              <s:submit key="dathang24h.modal.signin.button.submit" cssClass="btn" />
            </div>
          </div>
        </s:form>
      </div>
      <div class="modal-footer">
       <a class="btn" data-dismiss="modal" href="#"><s:text name="dathang24h.modal.signin.button.close" /></a>
      </div>
    </div>
  </div> <!-- /container -->

  <!-- / FOOTER -->
  <div id="footer">
    <div class="container">
      <div class="row-fluid">
        <div class="span12">
          <p class="copyrighttext"><s:text name="dathang24h.footer.label.copyright" /></p>
          <span class="copyright-links-float"><p class="copyrighttext"><a href="#" rel="nofollow" ><s:text name="dathang24h.footer.label.customer-benefit" /></a> - <a href="#" rel="nofollow" ><s:text name="dathang24h.footer.label.general-rule" /></a></p></span>
        </div>
      </div>
      <div class="row-fluid">
        <div class="span3"  id="footernav">
          <h4><s:text name="dathang24h.footer.label.customer-care" /></h4>
          <ul>
            <li><a href="online-support.action"><s:text name="dathang24h.footer.label.online-support" /></a></li>
            <li><a href="contact-us.action"><s:text name="dathang24h.top-navigation.contact-us.label" /></a></li>
            <li><a href="faq.action"><s:text name="dathang24h.footer.label.faq" /></a></li>
          </ul>
        </div>
        <div class="span3"  id="footernav">
          <h4><s:text name="dathang24h.footer.label.information.heading" /></h4>
          <p><s:text name="dathang24h.footer.label.address.no.street" /></p>
          <p><s:text name="dathang24h.footer.label.address.dist.city" /></p>
          <p><a href="https://maps.google.com/maps/ms?msid=212194521636831158173.0004d69c33e5e6a872bc6&msa=0&ll=21.012597,105.81851&spn=0.004322,0.004823&iwloc=0004d69c33e72c2b3bcb5" rel="nofollow" title="Bản đồ dathang24h.net" target="_blank"><s:text name="dathang24h.footer.label.map" /></a></p>
          <p><strong><s:text name="dathang24h.footer.label.hotline" /></strong></p>
        </div>
        <div class="span3" id="footernav">
          <h4><s:text name="dathang24h.footer.label.online-stores" /></h4>
          <ul>
            <li><a href="online-stores.action"><s:text name="dathang24h.footer.label.online-stores.general" /></a></li>
            <li><a href="online-stores-headphone-dac.action"><s:text name="dathang24h.footer.label.online-stores.audio" /></a></li>
            <li><a href="online-stores-computer.action"><s:text name="dathang24h.footer.label.online-stores.computer" /></a></li>
            <li><a href="online-stores-fashion.action"><s:text name="dathang24h.footer.label.online-stores.fashion" /></a></li>
            <li><a href="online-stores-cosmetic.action"><s:text name="dathang24h.footer.label.online-stores.comestic-accessory" /></a></li>
          </ul>
        </div>
        <div class="span3"  id="footernav">
          <h4><s:text name="dathang24h.footer.label.follow-us" /></h4>
          <ul>
            <li><a href="#" target="_blank" title="dathang24h.net on Twitter">Twitter</a></li>
            <li><a href="#" target="_blank" title="dathang24h.net on Facebook">Facebook</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <!-- JavaScript libraries -->
  <script src="<s:url value="/" encode="false" />js/jquery.min.js" type="text/javascript"></script>
  <script src="<s:url value="/" encode="false" />js/bootstrap.min.js" type="text/javascript"></script>    
  <script src="<s:url value="/" encode="false" />js/bootstrap-collapse.js" type="text/javascript"></script>

  <!-- Histats.com  START (hidden counter)-->
  <script type="text/javascript">document.write(unescape("%3Cscript src=%27http://s10.histats.com/js15.js%27 type=%27text/javascript%27%3E%3C/script%3E"));</script>
  <a href="http://www.histats.com" target="_blank" title="free stats" ><script  type="text/javascript" >
  try {Histats.start(1,2257283,4,0,0,0,"");
  Histats.track_hits();} catch(err){};
  </script></a>
  <noscript><a href="http://www.histats.com" target="_blank"><img  src="http://sstatic1.histats.com/0.gif?2257283&101" alt="free stats" border="0"></a></noscript>
  <!-- Histats.com  END  -->
</body>
</html>
