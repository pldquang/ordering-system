<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="description" content="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." />
  <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="" />
  <meta name="copyright" content="" />
  <meta name="robots" content="all" />
  <meta name="geo.region" content="US-OH" />
  <meta name="geo.placename" content="Cincinnati" />
  <meta name="geo.position" content="39.108269;-84.497414" />
  <meta name="ICBM" content="39.108269, -84.497414" />
  <title><s:text name="dathang24h.title" /></title>
  <link rel="stylesheet" href="<s:url value="/" encode="false" />css/reset.css" type="text/css" />
  <link rel="stylesheet" href="<s:url value="/" encode="false" />css/sketcher.css" type="text/css" />
  <link rel="stylesheet" href="<s:url value="/" encode="false" />css/colorbox.css" type="text/css" />
  <link href='http://fonts.googleapis.com/css?family=Lato:regular,regularitalic,900' rel='stylesheet' type='text/css' />
  <!-- CHROME -->
  <link href="css/chrome.css" rel="stylesheet" type="text/css" />
  <!-- SAFARI -->
  <link href="css/safari.css" rel="stylesheet" type="text/css" />
  <!--[if IE 7]>
  <link href="css/ie7.css" rel="stylesheet" type="text/css" />
  <![endif]-->
  <!--[if IE 8]>
  <link href="css/ie8.css" rel="stylesheet" type="text/css" />
  <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
  <![endif]-->
  <script type="text/javascript" src="<s:url value="/" encode="false" />js/jquery-1.5.1.min.js"></script>
  <script type="text/javascript" src="<s:url value="/" encode="false" />js/jquery.countdown.js"></script>
  <script type="text/javascript" src="<s:url value="/" encode="false" />js/jquery.colorbox-min.js"></script>
  <script type="text/javascript" src="<s:url value="/" encode="false" />js/sketcher.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      //Setting trigger, content and dimensions of contact box overlay
      $(".contactusicon").colorbox({width:"600px", height:"580px", initialWidth:"600px", initialHeight:"580px", speed:"2000", inline:true, href:"#contactus"});
    });
  </script>
  <script type="text/javascript">
  //<![CDATA[
      var secondsStr = "31348891";
  //]]>
  </script>
  <script type="text/javascript">
  
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-2473517-16']);
    _gaq.push(['_trackPageview']);
  
    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  
  </script>
</head>

<body class="default">

  <div id="page">

    <h1><a href="<s:url value="index.action" />" title=""><img class="logo-fix" src="<s:url value="/" />img/under-construction.png" alt=""></a></h1>   <div id="countdown" class="false"></div>

    <div class="content">
      <h2>Trang web đang trong quá trình hoàn thiện, quý khách vui lòng quay lại sau. Xin chân thành cảm ơn. Quay về <a href="<s:url value="index.action" />">Trang chủ</a>.</h2>
      <ul id="social">
        <li class="facebook"><a href="#"></a></li>
        <li class="twitter"><a href="#"></a></li>
        <li class="youtube"><a href="#"></a></li>
        <li class="blogger"><a href="#"></a></li>
        <li class="skype"><a href="#"></a></li>
        <li class="tumblr"><a href="#"></a></li>
        <li class="lastfm"><a href="#"></a></li>
        <li class="flickr"><a href="#"></a></li>
        <li class="digg"><a href="#"></a></li>
        <li class="linkedin"><a href="#"></a></li>
        <li class="myspace"><a href="#"></a></li>
        <li class="vimeo"><a href="#"></a></li>
        <li class="rss"><a href="#"></a></li>
        <li class="email contactusicon"><a href="#"></a></li>
      </ul><!-- end #social -->
    </div><!-- end #content -->
  </div><!-- end #page -->

  <!-- Histats.com  START (hidden counter)-->
  <script type="text/javascript">document.write(unescape("%3Cscript src=%27http://s10.histats.com/js15.js%27 type=%27text/javascript%27%3E%3C/script%3E"));</script>
  <a href="http://www.histats.com" target="_blank" title="free stats" ><script  type="text/javascript" >
  try {Histats.start(1,2257283,4,0,0,0,"");
  Histats.track_hits();} catch(err){};
  </script></a>
  <noscript><a href="http://www.histats.com" target="_blank"><img  src="http://sstatic1.histats.com/0.gif?2257283&101" alt="free stats" border="0"></a></noscript>
  <!-- Histats.com  END  -->
</body>
</html>
