<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="description" content="Đặt Hàng, Mua Hàng, Order Hàng, Ship Hàng từ các website bán hàng trực tuyến uy tín: Amazon, ebay, Walmart, Raffaello-Network, Nordstrom, Forever 21, Macy's, Sephora, Bath&Body Works, Accessorize..." />
  <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="" />
  <meta name="copyright" content="" />
  <meta name="robots" content="all" />
  <meta name="geo.region" content="US-OH" />
  <meta name="geo.placename" content="Cincinnati" />
  <meta name="geo.position" content="39.108269;-84.497414" />
  <meta name="ICBM" content="39.108269, -84.497414" />
  <title>dathang24h - Đặt Hàng, Mua Hàng, Order Hàng, Ship Hàng Amazon, ebay, Walmart, Raffaello-Network, Nordstrom, Forever 21, Macy's, Sephora, Bath&Body Works, Accessorize...</title>
  <link rel="canonical" href="#"/>
  <link rel="apple-touch-icon" href="#"/>
</head>

<body class="body-bg-line">
  <c:redirect url="ordering/index.action" />
</body>
</html>
