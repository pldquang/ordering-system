;(function($){
  //
  $('#formSignIn').bind('submit', function(event) {
    //
    if ($('#username').val().trim() == '') {
      alert('Vui lòng điền tên đăng nhập!!!');
      $('#username').focus();
      return false;
    }

    //
    if ($('#password').val().trim() == '') {
      alert('Vui lòng cung cấp mật khẩu đăng nhập!!!');
      $('#password').focus();
      return false;
    }

    //
    return true;
  });

})(jQuery);
