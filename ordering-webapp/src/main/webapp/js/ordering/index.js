;(function($){
  $('#spinner').hide();
  $('#itemLink').attr('placeholder', 'Nhập link sản phẩm bạn muốn xem thông tin');
  $('#formOrder').bind('submit', function(event) {
    //
    if ($('#itemLink').val().trim() == '') {
      alert('Vui lòng nhập link sản phẩm!!!');
      $('#itemLink').focus();
      return false;
    }

    //
    var leftPosition = $('#btnSubmit').offset().left + 55;
    var opts = {
      lines: 13, // The number of lines to draw
      length: 0, // The length of each line
      width: 5, // The line thickness
      radius: 10, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      direction: 1, // 1: clockwise, -1: counterclockwise
      color: '#000', // #rgb or #rrggbb
      speed: 1, // Rounds per second
      trail: 60, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: '-30px', // Top position relative to parent in px
      left: leftPosition // Left position relative to parent in px
    };

    //
    var target = document.getElementById('spinner');
    var spinner = new Spinner(opts).spin(target);
    $('#spinner').show();
  });
})(jQuery);