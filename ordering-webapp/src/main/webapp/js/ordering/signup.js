;(function($){
  //
  var USERNAME_REGEX = /^[a-zA-Z0-9\_]+$/;
  var PHONE_REGEX = /^[\d\s()\.+\-]{3,20}$/;
  //var EMAIL_REGEX = /^[A-z]+([A-z0-9]+[\.\_]?[A-z0-9]+)*@[A-z0-9]{2,}(\.[A-z0-9]{2,}){1,3}[A-z]$/;
  //var EMAIL_REGEX = /^[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
  var EMAIL_REGEX = /^([a-z0-9][-a-z0-9_\+\.]*[a-z0-9])@([a-z0-9][-a-z0-9\.]*[a-z0-9]\.(arpa|root|aero|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)|([0-9]{1,3}\.{3}[0-9]{1,3}))$/;

  //
  $('#formSignUp').bind('submit', function(event) {
    //
    if ($('#username').val().trim() == '') {
      alert('Vui lòng điền đầy đủ thông tin vào các ô ✓');
      $('#username').focus();
      return false;
    }

    //
    if (!$('#username').val().match(USERNAME_REGEX)) {
      alert('Tên đăng nhập không hợp lệ!!!\nTên đăng nhập chỉ bao gồm các ký tự từ a-z hoặc A-Z, dấu _ và ký tự số');
      $('#username').focus();
      return false;
    }

    //
    if ($('#password').val().trim() == '') {
      alert('Vui lòng điền đầy đủ thông tin vào các ô ✓');
      $('#password').focus();
      return false;
    }

    //
    if ($('#password').val().length < 6) {
      alert('Mật khẩu bao gồm ít nhất 6 ký tự!!!');
      $('#password').focus();
      return false;
    }

    //
    if ($('#passwordConfirm').val().trim() == '') {
      alert('Vui lòng điền đầy đủ thông tin vào các ô ✓');
      $('#passwordConfirm').focus();
      return false;
    }

    //
    if ($('#password').val() != $('#passwordConfirm').val()) {
      alert('Xác nhận mật khẩu không hợp lệ!!!');
      $('#password').val('');
      $('#passwordConfirm').val('');
      $('#password').focus();
      return false;
    }

    //
    if ($('#fullname').val().trim() == '') {
      alert('Vui lòng điền đầy đủ thông tin vào các ô ✓');
      $('#fullname').focus();
      return false;
    }

    //
    if ($('#address').val().trim() == '') {
      alert('Vui lòng điền đầy đủ thông tin vào các ô ✓');
      $('#address').focus();
      return false;
    }

    //
    if ($('#phone').val().trim() == '') {
      alert('Vui lòng điền đầy đủ thông tin vào các ô ✓');
      $('#phone').focus();
      return false;
    }

    //
    if (!$('#phone').val().match(PHONE_REGEX)) {
      alert('Số điện thoại không hợp lệ!!!');
      $('#phone').focus();
      return false;
    }

    //
    if ($('#email').val().trim() == '') {
      alert('Vui lòng điền đầy đủ thông tin vào các ô ✓');
      $('#email').focus();
      return false;
    }

    //
    if (!$('#email').val().match(EMAIL_REGEX)) {
      alert('Email không hợp lệ!!!');
      $('#email').focus();
      return false;
    }

    //
    if ($('#recaptcha_response_field').val().trim() == '') {
      alert('Vui lòng điền Mã xác nhận!!!');
      $('#recaptcha_response_field').focus();
      return false;
    }

    //
    if (!$('#isAgreed').is(':checked')) {
      alert('Vui lòng đọc kỹ quy định dành cho thành viên và đánh dấu ✓ xác nhận');
      $('#isAgreed').focus();
      return false;
    }

    return true;
  });

})(jQuery);
