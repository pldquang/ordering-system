;(function($){
  //
  $('#formChangePassword').bind('submit', function(event) {
    //
    if ($('#password').val().trim() == '') {
      alert('Mật khẩu hiện tại không được để trống!!!');
      $('#password').focus();
      return false;
    }

    //
    if ($('#newPassword').val().trim() == '') {
      alert('Mật khẩu mới không được để trống!!!');
      $('#newPassword').focus();
      return false;
    }

    //
    if ($('#newPassword').val().length < 6) {
      alert('Mật khẩu mới bao gồm ít nhất 6 ký tự!!!');
      $('#newPassword').focus();
      return false;
    }

    //
    if ($('#newPasswordConfirm').val().trim() == '') {
      alert('Xác nhận Mật khẩu mới không được để trống!!!');
      $('#newPasswordConfirm').focus();
      return false;
    }

    //
    if ($('#newPasswordConfirm').val() != $('#newPassword').val()) {
      alert('Xác nhận Mật khẩu mới không chính xác!!!');
      $('#newPasswordConfirm').focus();
      return false;
    }

    return true;
  });

})(jQuery);
