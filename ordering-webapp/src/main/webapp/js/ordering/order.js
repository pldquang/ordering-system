;(function($){
  //
  var PHONE_REGEX = /^[\d\s()\.+\-]{3,20}$/;
  var EMAIL_REGEX = /^([a-z0-9][-a-z0-9_\+\.]*[a-z0-9])@([a-z0-9][-a-z0-9\.]*[a-z0-9]\.(arpa|root|aero|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)|([0-9]{1,3}\.{3}[0-9]{1,3}))$/;

  //
  $('#spinner').hide();

  //
  if ($('#itemQuantity').length > 0) {
    $('#itemQuantity').spinedit();
  }

  //
  $('#formOrder').bind('submit', function(event) {
    //
    if ($('#fullname').val().trim() == '') {
      alert('Vui lòng cung cấp thông tin Họ và tên!!!');
      $('#fullname').focus();
      return false;
    }

    //
    if ($('#phone').val().trim() == '') {
      alert('Vui lòng cung cấp thông tin Số điện thoại liên hệ!!!');
      $('#phone').focus();
      return false;
    }

    //
    if (!$('#phone').val().match(PHONE_REGEX)) {
      alert('Số điện thoại không hợp lệ!!!');
      $('#phone').focus();
      return false;
    }

    //
    if ($('#email').val().trim() == '') {
      alert('Vui lòng cung cấp thông tin địa chỉ e-mail!!!');
      $('#email').focus();
      return false;
    }

    //
    if (!$('#email').val().match(EMAIL_REGEX)) {
      alert('Email không hợp lệ!!!');
      $('#email').focus();
      return false;
    }

    //
    if ($('#address').val().trim() == '') {
      alert('Vui lòng cung cấp thông tin địa chỉ liên hệ!!!');
      $('#address').focus();
      return false;
    }

    //
    if ($('#itemLink').val().trim() == '') {
      alert('Vui lòng cung cấp thông tin Link sản phẩm!!!');
      $('#itemLink').focus();
      return false;
    }

    //
    if ($('#itemQuantity').val().trim() == '') {
      alert('Vui lòng cung cấp thông tin Số lượng đặt hàng sản phẩm!!!');
      $('#itemQuantity').focus();
      return false;
    }
    
    //
    var leftPosition = $('#btnSubmit').offset().left + 55;
    var opts = {
      lines: 13, // The number of lines to draw
      length: 0, // The length of each line
      width: 5, // The line thickness
      radius: 10, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      direction: 1, // 1: clockwise, -1: counterclockwise
      color: '#000', // #rgb or #rrggbb
      speed: 1, // Rounds per second
      trail: 60, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: '-30px', // Top position relative to parent in px
      left: leftPosition // Left position relative to parent in px
    };

    //
    var target = document.getElementById('spinner');
    var spinner = new Spinner(opts).spin(target);
    $('#spinner').show();
    return true;
  });

})(jQuery);
