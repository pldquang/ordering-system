<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Unicorn Admin</title>
    <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<s:url value="/" />css/admin/bootstrap.min.css" />
    <link rel="stylesheet" href="<s:url value="/" />css/admin/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<s:url value="/" />css/admin/uniform.css" />
    <link rel="stylesheet" href="<s:url value="/" />css/admin/select2.css" />    
    <link rel="stylesheet" href="<s:url value="/" />css/admin/unicorn.main.css" />
    <link rel="stylesheet" href="<s:url value="/" />css/admin/unicorn.grey.css" class="skin-color" />  
  </head>
  <body>
    
    
    <div id="header">
      <h1><a href="./dashboard.html">Unicorn Admin</a></h1>   
    </div>
    
    <div id="search">
      <input type="text" placeholder="Search here..."/><button type="submit" class="tip-right" title="Search"><i class="icon-search icon-white"></i></button>
    </div>
    <div id="user-nav" class="navbar navbar-inverse">
            <ul class="nav btn-group">
                <li class="btn btn-inverse" ><a title="" href="#"><i class="icon icon-user"></i> <span class="text">Profile</span></a></li>
                <li class="btn btn-inverse dropdown" id="menu-messages"><a href="#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-envelope"></i> <span class="text">Messages</span> <span class="label label-important">5</span> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a class="sAdd" title="" href="#">new message</a></li>
                        <li><a class="sInbox" title="" href="#">inbox</a></li>
                        <li><a class="sOutbox" title="" href="#">outbox</a></li>
                        <li><a class="sTrash" title="" href="#">trash</a></li>
                    </ul>
                </li>
                <li class="btn btn-inverse"><a title="" href="#"><i class="icon icon-cog"></i> <span class="text">Settings</span></a></li>
                <li class="btn btn-inverse"><a title="" href="login.html"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
            </ul>
        </div>
            
    <div id="sidebar">
      <a href="#" class="visible-phone"><i class="icon icon-th-list"></i> Tables</a>
      <ul>
        <li><a href="index.html"><i class="icon icon-home"></i> <span>Dashboard</span></a></li>
        <li class="submenu">
          <a href="#"><i class="icon icon-th-list"></i> <span>Form elements</span> <span class="label">3</span></a>
          <ul>
            <li><a href="form-common.html">Common elements</a></li>
            <li><a href="form-validation.html">Validation</a></li>
            <li><a href="form-wizard.html">Wizard</a></li>
          </ul>
        </li>
        <li><a href="buttons.html"><i class="icon icon-tint"></i> <span>Buttons &amp; icons</span></a></li>
        <li><a href="interface.html"><i class="icon icon-pencil"></i> <span>Interface elements</span></a></li>
        <li class="active"><a href="tables.html"><i class="icon icon-th"></i> <span>Tables</span></a></li>
        <li><a href="grid.html"><i class="icon icon-th-list"></i> <span>Grid Layout</span></a></li>
        <li class="submenu">
          <a href="#"><i class="icon icon-file"></i> <span>Sample pages</span> <span class="label">4</span></a>
          <ul>
            <li><a href="invoice.html">Invioce</a></li>
            <li><a href="chat.html">Support chat</a></li>
            <li><a href="calendar.html">Calendar</a></li>
            <li><a href="gallery.html">Gallery</a></li>
          </ul>
        </li>
        <li>
          <a href="charts.html"><i class="icon icon-signal"></i> <span>Charts &amp; graphs</span></a>
        </li>
        <li>
          <a href="widgets.html"><i class="icon icon-inbox"></i> <span>Widgets</span></a>
        </li>
        <li>
          <a href="user-management.action"><i class="icon icon-user"></i> <span>Quản lý người dùng</span></a>
        </li>
        <li>
          <a href="order-management.action"><i class="icon icon-list-alt"></i> <span>Quản lý đặt hàng</span></a>
        </li>
      </ul>
    
    </div>
    
    <div id="style-switcher">
      <i class="icon-arrow-left icon-white"></i>
      <span>Style:</span>
      <a href="#grey" style="background-color: #555555;border-color: #aaaaaa;"></a>
      <a href="#blue" style="background-color: #2D2F57;"></a>
      <a href="#red" style="background-color: #673232;"></a>
    </div>
    
    <div id="content">
      <div id="content-header">
        <h1>Tables</h1>
        <div class="btn-group">
          <a class="btn btn-large tip-bottom" title="Manage Files"><i class="icon-file"></i></a>
          <a class="btn btn-large tip-bottom" title="Manage Users"><i class="icon-user"></i></a>
          <a class="btn btn-large tip-bottom" title="Manage Comments"><i class="icon-comment"></i><span class="label label-important">5</span></a>
          <a class="btn btn-large tip-bottom" title="Manage Orders"><i class="icon-shopping-cart"></i></a>
        </div>
      </div>
      <div id="breadcrumb">
        <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
        <a href="#" class="current">Tables</a>
      </div>
      <div class="container-fluid">
        <div class="row-fluid">
          <div class="span12">
            <s:if test="message != null && message.trim() != ''">
              <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <em><s:property value="message" escapeHtml="true" /></em>
              </div>
            </s:if>
            <div class="widget-box">
              <s:form method="post" id="formOrderEdit" cssClass="well form-horizontal" action="order-edit.action" theme="simple">
                <legend>Phiếu Đặt Hàng (No.<s:property value="prettyOrderId" />)</legend>
                <input type="hidden" name="orderId" value="<s:property value="orderId" />" />
                <input type="hidden" name="isUpdate" value="true" />
                <div class="control-group">
                  <label class="control-label" for="username"><s:text name="dathang24h.modal.signin.label.username" /></label>
                  <div class="controls">
                    <div class="input-prepend" style="width: 50%">
                      <span class="add-on"><i class="icon-user"></i></span>
                      <s:textfield id="username" name="username" maxlength="100" disabled="true" />
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="fullname">Họ và tên</label>
                  <div class="controls">
                    <div class="input-prepend" style="width: 100%">
                      <span class="add-on"><i class="icon-user"></i></span>
                      <s:textfield id="fullname" name="fullname" maxlength="100" />
                      <span class="help-inline"><i class="icon-check"></i></span>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="phone">Số điện thoại</label>
                  <div class="controls">
                    <div class="input-prepend" style="width: 50%">
                      <span class="add-on"><i class="icon-signal"></i></span>
                      <s:textfield id="phone" name="phone" maxlength="50" />
                      <span class="help-inline"><i class="icon-check"></i></span>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="email">E-mail</label>
                  <div class="controls">
                    <div class="input-prepend" style="width: 50%">
                      <span class="add-on"><i class="icon-envelope"></i></span>
                      <s:textfield id="email" name="email" maxlength="100" />
                      <span class="help-inline"><i class="icon-check"></i></span>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="address">Địa chỉ</label>
                  <div class="controls">
                    <div class="input-prepend" style="width: 100%">
                      <span class="add-on"><i class="icon-home"></i></span>
                      <s:textfield id="address" name="address" maxlength="100" />
                      <span class="help-inline"><i class="icon-check"></i></span>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="createdDate">Ngày đặt hàng</label>
                  <div class="controls">
                    <div class="input-prepend" style="width: 50%">
                      <span class="add-on"><i class="icon-time"></i></span>
                      <s:textfield id="createdDate" name="createdDate" maxlength="100" disabled="true" />
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="lastUpdateTime">Ngày cập nhật cuối</label>
                  <div class="controls">
                    <div class="input-prepend" style="width: 50%">
                      <span class="add-on"><i class="icon-time"></i></span>
                      <s:textfield id="lastUpdateTime" name="lastUpdateTime" maxlength="100" disabled="true" />
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="orderStatus">Trạng thái</label>
                  <div class="controls">
                    <div class="input-prepend" style="width: 100%">
                      <span class="add-on"><i class="icon-tasks"></i></span>
                      <s:select list="#{'NEW': 'Chờ xử lý', 'CUSTOMER_DEPOSITED':'Đã đặt cọc', 'FINISHED':'Kết thúc'}" id="orderStatus" name="orderStatus" value="orderStatus" />
                    </div>
                  </div>
                </div>
                <legend></legend>
                <div class="control-group">
                  <label class="control-label" for="itemName">Tên sản phẩm</label>
                  <div class="controls">
                    <div class="input-prepend" style="width: 100%">
                      <span class="add-on"><i class="icon-shopping-cart"></i></span>
                      <s:textfield id="itemName" name="itemName" maxlength="200" />
                      <span class="help-inline"><i class="icon-check"></i></span>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="itemLink">Link sản phẩm</label>
                  <div class="controls">
                    <div class="input-prepend" style="width: 100%">
                      <span class="add-on"><i class="icon-download-alt"></i></span>
                      <s:textfield id="itemLink" name="itemLink" maxlength="2000" />
                      <span class="help-inline"><i class="icon-check"></i></span>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="itemColor">Màu sắc</label>
                  <div class="controls">
                    <div class="input-prepend" style="width: 50%">
                      <span class="add-on"><i class="icon-star"></i></span>
                      <s:textfield id="itemColor" name="itemColor" maxlength="100" />
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="itemSize">Kích cỡ</label>
                  <div class="controls">
                    <div class="input-prepend" style="width: 50%">
                      <span class="add-on"><i class="icon-resize-full"></i></span>
                      <s:textfield id="itemSize" name="itemSize" maxlength="50" />
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="itemQuantity">Số lượng</label>
                  <div class="controls">
                    <div class="input-prepend" style="width: 50%">
                      <span class="add-on"><i class="icon-list-alt"></i></span>
                      <s:textfield id="itemQuantity" name="itemQuantity" maxlength="50" />
                      <span class="help-inline"><i class="icon-check"></i></span>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="itemUnit">Đơn vị tính</label>
                  <div class="controls">
                    <div class="input-prepend" style="width: 50%">
                      <span class="add-on"><i class="icon-tag"></i></span>
                      <s:textfield id="itemUnit" name="itemUnit" maxlength="50" />
                      <span class="help-inline"><i class="icon-check"></i></span>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="itemNote">Ghi chú</label>
                  <div class="controls" style="width: 86%">
                    <s:textarea id="itemNote" name="itemNote" rows="6" maxlength="2000" cssClass="input-xxlarge"></s:textarea>
                  </div>
                </div>
                <div class="form-actions">
                  <s:submit value="Lưu thay đổi" cssClass="btn btn-primary" />
                  <s:reset value="Nhập lại" cssClass="btn" />
                </div>
              </s:form>
            </div>
          </div>
        </div>
        
        <div class="row-fluid">
          <div id="footer" class="span12">
            2012 &copy; Unicorn Admin. Brought to you by <a href="https://wrapbootstrap.com/user/diablo9983">diablo9983</a>
          </div>
        </div>
      </div>
    </div>
    
    
            
            <script src="<s:url value="/" />js/admin/jquery.min.js"></script>
            <script src="<s:url value="/" />js/admin/jquery.ui.custom.js"></script>
            <script src="<s:url value="/" />js/admin/bootstrap.min.js"></script>
            <script src="<s:url value="/" />js/admin/jquery.uniform.js"></script>
            <script src="<s:url value="/" />js/admin/select2.min.js"></script>
            <script src="<s:url value="/" />js/admin/jquery.dataTables.min.js"></script>
            <script src="<s:url value="/" />js/admin/unicorn.js"></script>
            <script src="<s:url value="/" />js/admin/unicorn.tables.js"></script>
  </body>
</html>
